<?php

namespace Service;

use \PDO;

class DatabaseService
{

    private static $connection = null;

    private static function initConnection()
    {
        $dbhost = '127.0.0.1';
        $dbuser = 'root';
        $dbpass = 'user';
        $dbname = 'buscom';
        $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$connection = $dbh;
    }

    private static function initBusTicketsConnection()
    {
        $dbhost = 'busng.cogdtvsel9xd.us-west-1.rds.amazonaws.com';
        $dbuser = 'busrootadmin';
        $dbpass = 'BR624XGghpdFP7rk';
        $dbname = 'bustickets';
        $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$connection = $dbh;
    }

    public static function getConnection( $type = null)
    {
        if($type == null){
            if (self::$connection == null) {
                self::initConnection();
            }
        }else{
            if (self::$connection == null) {
                self::initBusTicketsConnection();
            }
        }
        return self::$connection;
    }

    public function executeQuery($sql, $type = null) {
        try {
            $db = self::getConnection( $type );
            $stmt = $db->query($sql);
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            return $results;
        } catch (\PDOException $e) {
            return json_encode(['error'=>['text'=> $e->getMessage()]]);
        }
    }

    public function executeSaveQuery($query, $type = null){
        try {
            $db = self::getConnection($type);
            if($db->exec($query)){
                return true;
            }else{
                return false;
            }
        }
        catch(\PDOException $e)
        {
            return json_encode(['error'=>['text'=> $e->getMessage()]]);
        }
    }

    public function getUserDetails($user_id = null)
    {
        $query = "SELECT * FROM users WHERE id='$user_id'";
        $user = $this->executeQuery($query);
        return $user;
    }

}
