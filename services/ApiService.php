<?php

namespace Service;
require 'Libraries/Encrypt.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Libraries\Encrypt;
use Libraries\Utilities;
use Slim\Slim;

class ApiService
{

    private $api_url = 'http://localhost:8000/api/auth';
    public $api_salt = '83596322330e4de083ab50ba18546c87';
    public $life_span = 20160;
    public $api_call;
    public $encrypt;

    public function __construct()
    {
        $this->utility = new Utilities();
        $this->dbService = new \Service\DatabaseService();
        $this->api_call = new Client([
            'base_url' => $this->api_url,
        ]);
    }

    public function sendPost_Request($jsonData)
    {

        try {
            $res = $this->api_call->post($this->api_url, [
                'json' => json_decode($jsonData),
                'headers' => [
                    'api_key' => $this->api_key
                ]
            ]);
            http_response_code($res->getStatusCode());
            $result_ = $res->getBody();
        } catch (RequestException $e) {
            http_response_code($e->getResponse()->getStatusCode());
            $result_ = $e->getResponse()->getBody();
            $token = null;
        }
        $result = self::returnAuthJson($jsonData, $result_);
        return $result;
    }

    public function sendPostRequest($jsonData)
    {
        $url = $this->api_url;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData),
                'api_key: ' . $this->api_key)
        );

        $result_ = curl_exec($ch);
        $result = self::returnAuthJson($jsonData, $result_);
        return $result;
    }

    public function generateRandomAlphabets($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generate_token(Slim $slim)
    {
        $date = date('Y-m-d H:i:s');
        $expiry_date_ = strtotime("+12 hours", strtotime($date));
        $expiry_date = date("Y-m-d h:i:s", $expiry_date_);
        $api_key = $this->api_salt;
        $token = base64_encode($api_key . '|' . $id . '|' . $date . '|' . $expiry_date);
        return $token;
    }

    public function generateRandomNumber($length)
    {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(1, 9);
        }
        return $result;
    }

    public function UnsuccessfulError($message)
    {
        $message = [
            'code' => 403,
            'status' => 'error',
            'message' => $message
        ];
        return $message;
    }

    public function saveApiActivity($token, $ip, $request, $response)
    {
        $query = "INSERT INTO api_logs (token, ip_address,request,response) VALUES('$token', '$ip','$request','$response')";
        $result = $this->dbService->executeSaveQuery($query);
        return $result;
    }

    /*    public function authenticate_api_call($request = null, $tokeen = false)
        {
            $key = isset($request) ? $request: '';
            $api_key = $this->api_key;
            if (!$tokeen) {
                if ($key !== $api_key) {
                    $result = self::UnsuccessfulError('Invalid API Key!');
                } else {
                    $result = true;
                }
            } else {
                $token = isset($request) ? $request: '';
                if (!empty($token)) {
                    $validated_token = self::validate_token($token);
                    $now = new \DateTime('now', new \DateTimeZone('Africa/Lagos'));
                    $expiry_date__ = strtotime("+12 hours", strtotime(date('Y-m-d H:i:s')));
                    $expiry_date = date("Y-m-d h:i:s", $expiry_date__);
                    if ($validated_token[0] == 400) {
                        $result = self::UnsuccessfulError('Corrupt Token!');
                    } elseif ($validated_token[0] !== $api_key) {
                        $result = self::UnsuccessfulError('Corrupt Token!');
                    } elseif ($now < $expiry_date) {
                        $result = self::UnsuccessfulError('Token Has Expired!');
                    } else {
                        $result = self::generate_token($validated_token[1]);
                    }
                } elseif (!empty($key)) {
                    if ($key !== $api_key) {
                        $result = self::UnsuccessfulError('Invalid API Key!');
                    } else {
                        $result = true;
                    }
                } else {
                    $result = self::UnsuccessfulError('Token Absent!');
                }
            }

            return $result;
        }*/
    public function authenticate_api_call($token = null)
    {
        $payload = self::validate_token($token);
        $api_key = $payload[0];
        $agent_code = $payload[1];
        $get_agent = "SELECT api_key, agent_code FROM agents WHERE api_key = '$api_key' AND agent_code = '$agent_code'";
        $result = $this->dbService->executeQuery($get_agent);
        if(is_null($token)){
            $response = self::UnsuccessfulError('Token Absent!');
            return $response;
        }
        if (!empty($result)) {
            return true;
        } else {
            $response = self::UnsuccessfulError('Invalid Token!');
            return $response;
        }
    }

    public function authenticate_agent_api_call(Slim $slim)
    {
        $token = self::getApiHeaders($slim);
        print_r($token);
        exit;
    }

    public function validate_token($token)
    {
        $encryption_class = new Encrypt();
        $decode_token = $encryption_class->decode($token);
        if (strpos($decode_token, '|') !== false) {
            $bare_token = explode('|', $decode_token);
        } else {
            $bare_token = [0 => 400, 1 => 'Token is corrupt!', 3 => date('Y-m-d H:i:s')];
        }
        return $bare_token;
    }

    public function returnAuthJson($request, $response)
    {
        $api_response = is_array($response) ? $response : json_decode($response, true);
        $request_result = [
            'code' => 200,
            'message' => 'Request completed successfully.',
            'status' => 'SUCCESS',
            'yourRequest' => json_decode($request, true),
            'yourResponse' => $api_response
        ];
        $token = isset($api_response['token']) ? $api_response['token'] : null;
        self::saveApiActivity($token, '127.0.1', $request, json_encode($request_result));
        return json_encode($request_result);
    }

    public function getApiHeaders(Slim $slim)
    {
        $headers = $slim->request->headers;
        return $headers;
    }

    public function encodeAgentDetails(Slim $slim){
        $form_data = isset($slim) ? $slim->request->post():'';
        $agent_code = $form_data['agent_code'];
        $get_agent = "SELECT api_key, agent_code FROM agents WHERE agent_code = '$agent_code'";
        $result = $this->dbService->executeQuery($get_agent);
        $utility_service = new Utilities();
        if(!empty($result)){
            $api_key = $result[0]->api_key;
            $string = $api_key.'|'.$agent_code;
            $encryption_service = new Encrypt();
            $encoded_data = $encryption_service->encode($string);
            $token = ['token' => $encoded_data];
            return $utility_service->returnJson($token, 200, 'Agent API Token');
        }else{
            $result = self::UnsuccessfulError('Invalid Agent Code');
            return json_encode($result);
        }
    }

}
