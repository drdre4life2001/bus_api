<?php

namespace Service;

use Libraries\Utilities;
use Slim\Slim;


class BookingService
{

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->apiService = new \Service\ApiService();
        $this->parkService = new ParksService();
        $this->customerService = new CustomerService();
        $this->utility = new Utilities();
    }

    public function saveBooking(Slim $request)
    {
        $form_data = $request->request->post();
        $trip = self::getBookingTrip($form_data['trip_id']);
        $parent_trip = $trip[0]->parent_trip_id;
        $booking_operator = $this->parkService->getOperator($trip[0]->operator_id);
        $booking_code = $booking_operator[0]->code . rand(100000, 999999);
        $passport_type = isset($form_data['passport_type']) ? $form_data['passport_type'] : null;
        $passport_number = isset($form_data['passport_number']) ? $form_data['passport_number'] : null;
        $passport_date_of_issue = isset($form_data['passport_date_of_issue']) ? $form_data['passport_date_of_issue'] : null;
        $passport_place_of_issue = isset($form_data['passport_place_of_issue']) ? $form_data['passport_place_of_issue'] : null;
        $passport_expiry_date = isset($form_data['passport_expiry_date']) ? $form_data['passport_expiry_date'] : null;
        if ($trip[0]->international == 1) {
            switch ($passport_type) {
                case 'virgin_passport';
                    $passport_cost = $trip[0]->virgin_passport;
                    $final_cost = $form_data['final_cost'] + $passport_cost;
                    break;
                case 'regular_passport';
                    $passport_cost = $trip[0]->regular_passport;
                    $final_cost = $form_data['final_cost'] + $passport_cost;
                    break;
                case 'no_passport';
                    $passport_cost = $trip[0]->no_passport;
                    $final_cost = $form_data['final_cost'] + $passport_cost;
                    break;
                case null;
                    $passport_cost = 0;
                    $final_cost = $form_data['final_cost'];
                    break;
            }
        } else {
            $final_cost = $form_data['final_cost'];
            $passport_cost = 0;
        }
        $customer = $this->customerService->selectACustomer($form_data['contact_phone']);
        if (count($customer) !== 0) {
            $customer_data = [
                'name' => $form_data['contact_name'],
                'email' => $form_data['contact_email'],
                'phone' => $form_data['contact_phone'],
                'gender' => $form_data['gender'],
            ];
            self::updateCustomerDetails($customer_data, $customer[0]->id);
        }
        $booking_data = [
            'trip_id' => $form_data['trip_id'],
            'date' => $form_data['date'],
            'passenger_count' => isset($form_data['passenger_count']) ? $form_data['passenger_count'] : 1,
            'departure_date' => $form_data['departure_date'],
            'unit_cost' => $form_data['unit_cost'],
            'final_cost' => $final_cost,
            'paid_date' => isset($form_data['paid_date']) ? $form_data['paid_date']: null,
            'contact_name' => $form_data['contact_name'],
            'contact_phone' => $form_data['contact_phone'],
            'contact_email' => $form_data['contact_email'],
            'contact_gender' => $form_data['gender'],
            'booking_code' => $booking_code,
            'next_of_kin' => $form_data['next_of_kin_name'],
            'next_of_kin_phone' => $form_data['next_of_kin_phone'],
            'customer_id' => $form_data['customer_id'],
            'passport_type' => $passport_type,
            'passport_number' => $passport_number,
            'passport_date_of_issue' => $passport_date_of_issue,
            'passport_place_of_issue' => $passport_place_of_issue,
            'passport_expiry_date' => $passport_expiry_date,
            'passport_cost' => $passport_cost
            
        ];
        
        if($trip[0]->international == 1){
            self::saveInternationalBookingData($booking_data);
        }else{
           $test = self::saveLocalBookingData($booking_data);
          
        }
        $new_booking_details = self::getBookingDetailsWithCode($booking_code);
    
        if (!empty($parent_trip)) {
            $_trip_ = $parent_trip;
        } else {
            $_trip_ = $form_data['trip_id'];
        }
        // saving first passenger...
        $passenger_data = [
            'booking_id' => $new_booking_details[0]->id,
            'name' => $form_data['contact_name'],
            'seat' => $form_data['seat'],
            'gender' => $form_data['gender'],
            'trip_id' => $_trip_
        ];
        self::savePassengerData($passenger_data);
        if (!empty($form_data['return_trip_id'])) {
            $new_booking_code = $booking_operator[0]->code . rand(100000, 999999);
            $passenger_booking_data = [
                'trip_id' => $form_data['return_trip_id'],
                'date' => $form_data['date'],
                'passenger_count' => $form_data['passenger_count'],
                'departure_date' => $form_data['departure_date'],
                'unit_cost' => $form_data['unit_cost'],
                'final_cost' => $final_cost,
                'paid_date' => $form_data['paid_date'],
                'contact_name' => $form_data['contact_name'],
                'contact_phone' => $form_data['contact_phone'],
                'contact_email' => $form_data['contact_email'],
                'contact_gender' => $form_data['gender'],
                'booking_code' => $new_booking_code,
                'next_of_kin' => $form_data['next_of_kin_name'],
                'next_of_kin_phone' => $form_data['next_of_kin_phone'],
                'customer_id' => $form_data['customer_id'],
                'passport_type' => $form_data['passport_type'],
                'passport_number' => $form_data['passport_number'],
                'passport_date_of_issue' => $form_data['passport_date_of_issue'],
                'passport_place_of_issue' => $form_data['passport_place_of_issue'],
                'passport_expiry_date' => $form_data['passport_expiry_date'],
                'passport_cost' => $form_data['passport_cost'],
                'parent_booking_id' => $new_booking_details[0]->id
            ];
            if($trip[0]->international == 1){
                self::saveInternationalBookingData($passenger_booking_data);
            }else{
                self::saveLocalBookingData($passenger_booking_data);
            }
        }

        $booking_source_park = $this->parkService->getPark($trip[0]->source_park_id);
        $booking_dest_park = $this->parkService->getPark($trip[0]->dest_park_id);
        if (!empty($new_booking_details)) {
            $collection = [
                'booking' => $new_booking_details[0],
                'trip' => $trip[0],
                'sourcepark' => $booking_source_park[0],
                'destpark' => $booking_dest_park
            ];
            return $this->utility->returnJson($collection,true,'new booking');
        } else {
            return $this->utility->returnJson(null,false,'new booking error');

        }
    }

    public function GetTripFromBookingCode(Slim $request){
        $form_data = $request->request->post();
        $booking_code = isset($form_data['booking_code'])? $form_data['booking_code']: "";
        $booking_details = self::getBookingDetailsWithCode($booking_code);
        $booking_id = $booking_details[0]->id;
        $trip_id = $booking_details[0]->trip_id;
        $trip_sql = "SELECT * FROM trips WHERE id = '$trip_id'";
        $_trip = $this->dbService->executeQuery($trip_sql);
        $trip = (array) $_trip;
        $psg_sql = "SELECT * FROM passengers WHERE booking_id = '$booking_id'";
        $_psg = $this->dbService->executeQuery($psg_sql);
        $psg = (array) $_psg;
        $seat_sql = "SELECT * FROM seats WHERE booking_id = '$booking_id'";
        $_seat = $this->dbService->executeQuery($seat_sql);
        $seat = (array) $_seat;
        $bus = new BusService();
        $park = new ParksService();
        $bus_details = $bus->getBusDetails($trip[0]->bus_type_id);
        $operator_details = $park->getOperator($trip[0]->operator_id);
        $src_prk_details = $park->getPark($trip[0]->source_park_id);
        $dest_prk_details = $park->getPark($trip[0]->dest_park_id);
        $passenger_details = $psg;
        $final_collection = [
            'booking' => $booking_details[0],
            'trip' => $trip[0],
            'operator' => (array) $operator_details[0],
            'busType' => (array) $bus_details[0],
            'destpark' => $dest_prk_details[0],
            'sourcepark' => $src_prk_details[0],
            'seats' => !empty($seat) ? (array) $seat[0] : [],
            'passengers' => $passenger_details,
        ];
        return $this->utility->returnJson($final_collection,true,'A single trip');
    }

    public function savePassenger(Slim $request){
        $form_data = $request->request->post();
        $passenger_data = [
            'booking_id' => $form_data['booking_id'],
            'name' => $form_data['contact_name'],
            'seat' => $form_data['seat'],
            'gender' => $form_data['gender'],
            'trip_id' => $form_data['trip_id']
        ];
        $result = self::savePassengerData($passenger_data);
        return $this->utility->returnJson($result,true,'passenger saved');
    }

    public function getBookingTrip($id = null)
    {
        $query = "SELECT * FROM trips WHERE id='$id'";
        $trip = $this->dbService->executeQuery($query);
        return $trip;
    }

    public function getBookingDetails($id = null)
    {
        $query = "SELECT * FROM bookings WHERE id='$id'";
        $trip = $this->dbService->executeQuery($query);
        return $trip;
    }

    public function updateCustomerDetails(array $data, $id)
    {
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $gender = $data['gender'];
        $update_customer_query = "UPDATE bookings SET 
                                          contact_name = '$name',
                                          contact_email = '$email', 
                                          contact_phone = '$phone',
                                          contact_gender = '$gender'
                                          WHERE id = '$id' ";
        return $this->dbService->executeSaveQuery($update_customer_query);
    }

    public function saveLocalBookingData(array $form_data)
    {
        
        $trip_id = $form_data['trip_id'];
        $parent_booking_id = isset($form_data['parent_booking_id']) ? $form_data['parent_booking_id'] : 'null';
        $date = $form_data['date'];
        $passenger_count = $form_data['passenger_count'];
        $departure_date = $form_data['departure_date'];
        $unit_cost = $form_data['unit_cost'];
        $final_cost = $form_data['final_cost'];
        $contact_name = $form_data['contact_name'];
        $contact_phone = $form_data['contact_phone'];
        $contact_email = $form_data['contact_email'];
        $contact_gender = $form_data['contact_gender'];
        $booking_code = $form_data['booking_code'];
        $next_of_kin = $form_data['next_of_kin'];
        $next_of_kin_phone = $form_data['next_of_kin_phone'];
        $customer_id =0;
        $save_booking_query = "INSERT INTO bookings (trip_id, date, passenger_count, departure_date,unit_cost,final_cost,contact_name,contact_phone,contact_email,contact_gender,booking_code,next_of_kin,customer_id,next_of_kin_phone)VALUES('$trip_id','$date','$passenger_count','$departure_date','$unit_cost', '$final_cost','$contact_name', '$contact_phone', '$contact_email', '$contact_gender', '$booking_code', '$next_of_kin', '$customer_id','$next_of_kin_phone')";
        return $this->dbService->executeSaveQuery($save_booking_query);
    }

    public function saveInternationalBookingData(array $form_data)
    {
        $trip_id = $form_data['trip_id'];
        $parent_booking_id = isset($form_data['parent_booking_id']) ? $form_data['parent_booking_id']:null;
        $date = $form_data['date'];
        $passenger_count = $form_data['passenger_count'];
        $departure_date = $form_data['departure_date'];
        $unit_cost = $form_data['unit_cost'];
        $final_cost = $form_data['final_cost'];
        $contact_name = $form_data['contact_name'];
        $contact_phone = $form_data['contact_phone'];
        $contact_email = $form_data['contact_email'];
        $contact_gender = $form_data['contact_gender'];
        $booking_code = $form_data['booking_code'];
        $next_of_kin = $form_data['next_of_kin'];
        $next_of_kin_phone = $form_data['next_of_kin_phone'];
        $customer_id = $form_data['customer_id'];
        $passport_type = $form_data['passport_type'];
        $passport_number = $form_data['passport_number'];
        $passport_date_of_issue = $form_data['passport_date_of_issue'];
        $passport_place_of_issue = $form_data['passport_place_of_issue'];
        $passport_expiry_date = $form_data['passport_expiry_date'];
        $passport_cost = $form_data['passport_cost'];
        $save_booking_query = "INSERT INTO bookings (trip_id, date, passenger_count, departure_date,unit_cost,final_cost,contact_name,contact_phone,contact_email,contact_gender,booking_code,next_of_kin,customer_id,passport_type,passport_number,passport_date_of_issue,passport_place_of_issue,passport_expiry_date,passport_cost,next_of_kin_phone)VALUES('$trip_id','$date','$passenger_count','$departure_date','$unit_cost', '$final_cost','$contact_name', '$contact_phone', '$contact_email', '$contact_gender', '$booking_code', '$next_of_kin', '$customer_id','$passport_type','$passport_number', '$passport_date_of_issue', '$passport_place_of_issue', '$passport_expiry_date','$passport_cost','$next_of_kin_phone')";
        return $this->dbService->executeSaveQuery($save_booking_query);
    }

    public function savePassengerData(array $form_data)
    {
        $booking_id = $form_data['booking_id'];
        $name = $form_data['name'];
        $seat = $form_data['seat'];
        $gender = $form_data['gender'];
        $trip_id = $form_data['trip_id'];
        $save_booking_query = "INSERT INTO passengers (trip_id, booking_id, name, seat,gender)
                    VALUES('$trip_id','$booking_id','$name','$seat','$gender')";
        return $this->dbService->executeSaveQuery($save_booking_query);
    }

    public function getBookingDetailsWithCode($booking_code = null)
    {
        $query = "SELECT * FROM bookings WHERE booking_code='$booking_code' LIMIT 1";
        $booking = $this->dbService->executeQuery($query);
        return $booking;
    }

    public function getBookingAmountWithCode($booking_code = null)
    {
        $query = "SELECT final_cost FROM bookings WHERE booking_code='$booking_code'";
        $booking = $this->dbService->executeQuery($query);
        return $booking;
    }

    public function getCustomerBookingById(Slim $request){
        $form_data = $request->request->post();
        $booking_code = isset($form_data['booking_id'])? $form_data['booking_id']: "";
        $booking_details = self::getBookingDetails($booking_code);
        $booking_id = $booking_details[0]->id;
        $trip_id = $booking_details[0]->trip_id;
        $trip_sql = "SELECT * FROM trips WHERE id = '$trip_id'";
        $_trip = $this->dbService->executeQuery($trip_sql);
        $trip = (array) $_trip;
        $psg_sql = "SELECT * FROM passengers WHERE booking_id = '$booking_id'";
        $_psg = $this->dbService->executeQuery($psg_sql);
        $psg = (array) $_psg;
        $seat_sql = "SELECT * FROM seats WHERE booking_id = '$booking_id'";
        $_seat = $this->dbService->executeQuery($seat_sql);
        $seat = (array) $_seat;
        $bus = new BusService();
        $park = new ParksService();
        $bus_details = $bus->getBusDetails($trip[0]->bus_type_id);
        $operator_details = $park->getOperator($trip[0]->operator_id);
        $src_prk_details = $park->getPark($trip[0]->source_park_id);
        $dest_prk_details = $park->getPark($trip[0]->dest_park_id);
        $passenger_details = $psg;
        $final_collection = [
            'booking' => $booking_details[0],
            'trip' => $booking_details[0]->trip_id,
            'operator' => (array) $operator_details[0],
            'busType' => (array) $bus_details[0],
            'destpark' => $dest_prk_details[0],
            'sourcepark' => $src_prk_details[0],
            'seats' => !empty($seat) ? (array) $seat[0] : [],
            'passengers' => $passenger_details,
        ];
        return $this->utility->returnJson($final_collection,true,'A single trip');

    }

    public function getTripFromCharterId(Slim $request){
        $form_data = $request->request->post();
        $booking_code = isset($form_data['id'])? $form_data['id']: "";
        $booking_details = "SELECT * FROM chartered_bookings WHERE id = '$booking_code'";
        $booking_details = $this->dbService->executeQuery($booking_details);
        $booking_details = (array) $booking_details;
        return $this->utility->returnJson($booking_details,true,'A single trip');
    }

    public function saveCharterBooking(Slim $request){
        $form_data = $request->request->post();
        $pickup = $form_data['pickup'];
        $dropoff = $form_data['dropoff'];
        $contact_name = $form_data['contact_name'];
        $start = date('Y-m-d', strtotime($form_data['start']));
        $operator_id = $form_data['operator'];
        $contact_phone = $form_data['contact_phone'];
        $phone_kin = $form_data['phone_kin'];
        $kin = $form_data['kin'];
        $passenger = $form_data['passenger'];
        $budget = $form_data['budget'];
        $pick_up_address = $form_data['pick_up_address'];
        $drop_off_address = $form_data['drop_off_address'];
        $date = date('Y-m-d H:i:s');
        $save_booking_query = "INSERT INTO chartered_bookings (operator_id, pickup_location,travel_date, destination, contact_name, contact_mobile,pick_up_address,budget,drop_off_address,created_at,next_of_kin,next_of_kin_mobile,no_of_passengers)
                    VALUES('$operator_id','$pickup','$start','$dropoff','$contact_name','$contact_phone','$pick_up_address','$budget','$drop_off_address','$date','$kin','$phone_kin','$passenger')";
        $this->dbService->executeSaveQuery($save_booking_query);
        $booking_details = "SELECT * FROM chartered_bookings WHERE contact_name = '$contact_name' AND operator_id = '$operator_id' AND pickup_location = '$pickup' AND destination = '$dropoff' LIMIT 1";
        $booking_details = $this->dbService->executeQuery($booking_details);

        return $this->utility->returnJson($booking_details,true,'Saved Booking');
    }

    public function saveTransactions(Slim $request){
        $form_data = $request->request->post();
        $booking_id = $form_data['booking_id'];
        $booking_code = $form_data['booking_code'];
        $status = $form_data['response'];
        $response = $form_data['status'];
        $date = date('Y-m-d H:i:s');
        $save_booking_query = "INSERT INTO transactions (booking_id, booking_code,response, status, created_at)
                    VALUES('$booking_id','$booking_code','$response','$status','$date')";
        $save_booking = $this->dbService->executeSaveQuery($save_booking_query);

        if(isset($form_data['payment_method']) && trim($form_data['payment_method']) == "PayStack"){
            $update_booking_query = "UPDATE bookings SET status = 'paid', paid_date = $date, payment_method_id = 2 WHERE id = '$booking_id'";
            $this->dbService->executeSaveQuery($update_booking_query);
        }

        if(isset($form_data['cancelled'])){
            $cancel_booking_query = "UPDATE bookings SET status = 'CANCELLED' WHERE id = '$booking_id'";
            $this->dbService->executeSaveQuery($cancel_booking_query);
        }

        if($save_booking){
            $booking_details = "SELECT * FROM bookings WHERE id = '$booking_id'";
            $booking_data = $this->dbService->executeQuery($booking_details);
            return $this->utility->returnJson($booking_data,true,'Booking Information');
        }else{
            return $this->utility->returnJson(null,false,'Empty Booking Information');
        }
    }

    public function saveZenithTransactions($booking_code = null,$booking_id = null,$status = null,$response = null){
        $date = date('Y-m-d H:i:s');
        $save_booking_query = "INSERT INTO transactions (booking_id, booking_code, response, status, created_at)
                    VALUES('$booking_id','$booking_code','$response','$status','$date')";
        $save_booking = $this->dbService->executeSaveQuery($save_booking_query);

        $update_booking_query = "UPDATE bookings SET status = '$status', payment_method_id = 3 WHERE id = '$booking_id'";
        $updated = $this->dbService->executeSaveQuery($update_booking_query);
        if($save_booking && $updated){
            $booking_details = "SELECT * FROM bookings WHERE id = '$booking_id'";
            $booking_data = $this->dbService->executeQuery($booking_details);
            return $this->utility->returnJson($booking_data[0],true,'Booking Updated Successfully');
        }else{
            return $this->utility->returnJson(null,false,'Empty Booking Information');
        }

    }

}
