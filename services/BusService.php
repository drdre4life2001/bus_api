<?php

namespace Service;

use Libraries\Utilities;
use Slim\Slim;
use Service\ApiService;
use Service\CustomerService;
use Service\DatabaseService;
use Service\FeederService;
use Service\PaymentService;


class BusService
{

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->customerService = new CustomerService();
        $this->apiService = new \Service\ApiService();
        $this->utility = new Utilities();
    }

    public function getBusDetails($id)
    {
        $query = "SELECT * FROM bus_types WHERE id ='$id' LIMIT 1";
        $readings = $this->dbService->executeQuery($query);
        return $readings;
    }

    public function getBuses()
    {
        $query = "SELECT id,name FROM bus_types";
        $readings = $this->dbService->executeQuery($query);
        return $readings;
    }

    public function getSeats($departure_date, $trip_id){
        $query = "SELECT *  FROM seats WHERE departure_date = $departure_date AND trip_id = $trip_id";
        $readings = $this->dbService->executeQuery($query);
        return $readings;
    }

    public function saveBookedSeats(Slim $request){
        $form_data = $request->request->post();
        $trip_id = $form_data['trip_id'];
        $seat_no = $form_data['seat_no'];
        $booking_id = $form_data['booking_id'];
        $departure_date = $form_data['departure_date'];
        $created = date('Y-m-d H:i:s');
        $save_seat_query = "INSERT INTO seats (trip_id, seat_no, booking_id, departure_date,created_at)
                    VALUES('$trip_id','$seat_no','$booking_id','$departure_date','$created')";
        return $this->dbService->executeSaveQuery($save_seat_query);
    }

}
