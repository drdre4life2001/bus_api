<?php

namespace Service;

use Libraries\Utilities;
use Service\DatabaseService;
use Slim\Slim;
use Service\ApiService;
use Service\CustomerService;
use Service\MeterService;
use Service\PaymentService;

/**
 * Handle all operations partaining to the bus Affiliate program
 * @author Dave Partner Ozoalor
 */
class AffiliateService
{

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->apiService = new \Service\ApiService();
        $this->customerService = new \Service\CustomerService();
        $this->utilities = new Utilities();
    }

    /**
     *
     * Get all affiliates in the system
     * @request_type get
     * @return json $result List of all affiliates
     */
    public function index()
    {
        $query = "SELECT * FROM affiliates WHERE active = 1";
        $databaseService = new DatabaseService();
        $affiliates = $databaseService->executeQuery($query);
        $result = $this->utilities->returnJson($affiliates, 200, null);
        return $result;

        $result = $this->utilities->returnJson($affiliates,200,"success");

         return $this->utilities->returnJson($result,true,'List of all affiliates');
    }

    /**
     * View an affiliate's details
     * Affiliate's profile
     * @param get $request
     * @param integer $affiliate_id The unique identifier of the affiliate
     *
     * @return json The details of the affiliate
     */

    public function view($affiliate_id)
    {

        $query = "SELECT * FROM affiliates WHERE id = '$affiliate_id'";


        $databaseService = new DatabaseService();
        $response = $databaseService->executeQuery($query);
        if(!empty($response)){
            return $this->utilities->returnJson($response[0],200,"success");
        }else{
            return $this->utilities->returnJson($response,false,'No affiliate was found with that ID');
        }
    }

    /**
     * Create a new affiliate
     * @param post $request Contains details of the new affiliate to be created
     *
     * @return json $result details of the new affiliate
     */
    public function add(Slim $request)
    {
        $databaseService = new DatabaseService();
        //payload
        $email = $request->email;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $gender = $request->gender;
        $address = $request->address;
        $city = $request->city;
        $state = $request->state_id;
        $state = $request->state_id;
        
        //$country_id = $request->country_id; //default is Nigeria
        $password = password_hash($request->password, PASSWORD_BCRYPT);
        $save_booking_query = "INSERT INTO affiliates (email, first_name, last_name, gender, address,city, state_id, password)
                    VALUES('$email','$first_name','$last_name','$gender','$address','$city','$state_id','$password')";
        $response = $this->dbService->executeSaveQuery($save_booking_query);


        if ($response) {
            return $this->utilities->returnJson($request, 200, "success");
        } else {
            return $this->utilities->returnJson($request, false, "Error: query failed");
        }

    }

    /**
     * Update the details of an affiliate
     * @param undefined $request Contains details of the affilate to be updated
     *
     * @return json $result The details of the just updated affiliate
     */
    public function edit(Slim $request)
    {

        $databaseService = new DatabaseService();
        $id = $request->id;
        $email = $request->email;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $gender = $request->gender;
        $address = $request->address;
        $city = $request->city;
        $state_id = $request->state_id;
        //$country_id = $request->country_id; //default is Nigeria

        //there should be another method for changing passwords
        //$password = password_hash($request->password, PASSWORD_BCRYPT); 

        $save_booking_query = "
            UPDATE affiliates
            SET email='$email', first_name='$first_name', last_name='$last_name', gender='$gender', 
            address='$address', city='$city', state_id='$state_id'
            WHERE id='$id'";
        $response = $this->dbService->executeSaveQuery($save_booking_query);

         if ($response) {
             return $this->utilities->returnJson($request, 200, "success");
         } else {
             return $this->utilities->returnJson($request, false, "Error: querry did not run");
         }

	}

    /**
     * Delete an affiliate
     * @param integer $id The ID of the affiliate
     *
     * @return json $result message status about the delete operation
     */
    public function delete($affiliate_id)
    {
        $query = "DELETE FROM affiliates where affiliate_id = '$affiliate_id' ";
        $bookings = $this->dbService->executeQuery($query);
        if($bookings ){
            return $this->utilities->returnJson(true, 200, "Delete successful");
        }else{
            return $this->utilities->returnJson(true, false, "Delete Failed");
        }
         
    }

    /**
     * Get all bookings made by this affiliate's referals
     * @param affiliate_id $id
     *
     * @return
     */
    public function getAffiliateBookings($affiliate_id, $start_date = null, $end_date = null)
    {

        if ($start_date == null and $end_date == null) {
            $start_date = date('Y-m-d', strtotime('-4weeks'));
            $end_date = date('Y-m-d');
        }
        $query = "SELECT * FROM bookings WHERE affiliate_id='$affiliate_id' and created_at >= $start_date and created_at <= $end_date  ";
        $response = $this->dbService->executeQuery($query);
        if ($response) {
            return $this->utilities->returnJson($response, 200, "success");
        } else {
            return $this->utilities->returnJson($response, false, "Empty result");
        }
    }

    /**
     * Get total amount to be paid to this affiliate
     * @param undefined $id
     *
     * @return
     */
    public function getAffiliateBookingsTotalAmountToBePaid($affiliate_id, $start_date = null, $end_date = null)
    {

        if ($start_date == null and $end_date == null) {
            $start_date = date('Y-m-d', strtotime('-4weeks'));
            $end_date = date('Y-m-d');
        }

        $query = "SELECT * FROM bookings WHERE affiliate_id='$affiliate_id' and created_at >= $start_date and created_at <= $end_date  and affiliate_paid_status !='paid'";
        
        $bookings = $this->dbService->executeQuery($query);
        
       
        //calculate the total amount to be paid
        $totalAmountToBePaid = 0;

        $response  = $totalAmountToBePaid;
       if($bookings){

            foreach ($bookings as $booking) {
                $totalAmountToBePaid = +intval($booking['final_cost']);
            }

            
            //get 3% of total, that's what Bus.com.ng pays affiliates at the moment
            //A better way to store the percentage in the affiliates profile on the database
            //But no more time left, I am leaving the company soon and go twerk on the Burj :) 
            
            //Dont know who you will be, but I tried to comment this file very well so you dont have any issues
            $totalAmountToBePaid = (0.03 * $totalAmountToBePaid);

            $totalBookings = count($bookings);
            $response = Array('total_amount_to_be_paid'=>$totalAmountToBePaid, 'total_bookings'=> $totalBookings );

       }
       

            return $this->utilities->returnJson($response, 200, "success");
        


    }

    /**
     * Pay this affiliate
     * @param integer $affiliate_id
     *
     * @return
     */
    public function payAffiliate($affiliate_id)
    {
        /**
         * Find all bookings made by referals of this affiliate that havent been marked as paid_off
         * Calculate the total amount to be paid.
         * Then mark each of the bookings as paid off.
         * Update affiliate_payments table with the details of this affiliate and the payment made.
         * What mode of payment are we using sef?
         */

        $query = "SELECT * FROM bookings WHERE affiliate_id='$affiliate_id' and affiliate_paid_status != paid ";
        $bookings = $this->dbService->executeQuery($query);

        //calculate the total amount to be paid
        $totalAmountToBePaid = 0;
        foreach ($bookings as $booking) {
            $totalAmountToBePaid = +intval($booking['final_cost']);
        }

        //get 3% of total, that's what Bus.com.ng pays affiliates at the moment
        $totalAmountToBePaid = (0.03 * $totalAmountToBePaid);

        //update all bookings table and mark affiliates_paid_status as paid
        $query2 = " UPDATE bookings
                    SET affiliate_paid_status='paid'
                    WHERE affiliate_id='$affiliate_id' and   affiliate_paid_status !='paid' ";

        if ($query2) {
            return $this->utilities->returnJson($query2, true, null);
        } else {
            return $this->utilities->returnJson($query2, false, null);
        }
    }


}
