<?php

namespace Service;

use Libraries\Utilities;
use Slim\Slim;
use Service\ApiService;
use Service\CustomerService;
use Service\DatabaseService;
use Service\FeederService;
use Service\PaymentService;


class TripsService
{

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->customerService = new CustomerService();
        $this->apiService = new \Service\ApiService();
        $this->bus = new BusService();
        $this->utility = new Utilities();
    }

    public function searchTrips(Slim $request)
    {
        $form_data = $request->request->post();
        $current_time = date('H:i:s');
        $current_date = date('Y-m-d');
        $park = new ParksService();
        $bus = new BusService();
        $page = isset($form_data['page']) ? $form_data['page'] : 1;
        $per_page = isset($form_data['per_page']) ? $form_data['per_page'] : 10;
        $date = isset($form_data['date']) ? $form_data['date'] : $current_date;
        $filter_price = isset($form_data['filter_price']) ? $form_data['filter_price'] : '';
        $operator = isset($form_data['operator_id']) ? $form_data['operator_id'] : '';
        $src_prk = $form_data['fromPark'];
        $dest_prk = $form_data['toPark'];
        $skip = intval($per_page * ($page - 1));
        $sql = '';
        if (empty($operator)) {
            $sql = "SELECT * FROM trips WHERE source_park_id = $src_prk AND dest_park_id = $dest_prk AND active = 1";
        } elseif (!empty($operator)) {
            $sql = "SELECT * FROM trips WHERE source_park_id = $src_prk AND dest_park_id = $dest_prk AND active = 1 AND operator_id = $operator";
        }

        /* if($date == $current_date)
             $sql .= " AND departure_time > '$current_time''";*/
        if (!empty($filter_price)) {
            $fpa = explode(":", $form_data['filter_price']);
            $price_min = $fpa[0];
            $price_max = $fpa[1];
            $sql .= " AND fare > $price_min AND fare < $price_max";
        }
        /*if(isset($request->bus_type) && !empty($request->bparent_trip_codeus_type))
             $query = $query->where('bus_type_id', $request->bus_type);*/

        if (!empty($form_data['ac']))
            $sql .= " AND ac = 1";

        if (!empty($form_data['tv']))
            $sql .= " AND tv = 1";

        if (!empty($form_data['security']))
            $sql .= " AND security = 1";

        if (!empty($form_data['passport']))
            $sql .= " AND passport = 1";

        if (!empty($form_data['insurance']))
            $sql .= " AND insurance = 1";
        /*        $query = $query->searchPaginator($page, $per_page, $skip);*/
        if (isset($form_data['sort_by'])) {
            $s_arr = explode(":", $form_data['sort_by']);
            $sql .= " ORDER BY $s_arr[0]";
            /*            $query = $query->orderBy($s_arr[0], $s_arr[1]);*/
        } else {
            $sql .= " ORDER BY departure_time asc";
        }

        $query = $this->dbService->executeQuery($sql);
        $trip_count = count($query);

        //getting other filter data...
        $operators = $park->getOperators();
        $busTypes = $bus->getBuses();
        $fromPark = $park->getPark($form_data['fromPark']);
        $toPark = $park->getPark($form_data['toPark']);
        $trips = $this->dbService->executeQuery($sql);
        

      // $trips = array_merge($trips, $gigm);
     
        $trips_collection = [];
        foreach ($trips as $trip) {
            $bus_details = $bus->getBusDetails($trip->bus_type_id);
            $operator_details = $park->getOperator($trip->operator_id);
            $src_prk_details = $park->getPark($src_prk);
            $dest_prk_details = $park->getPark($dest_prk);
            $trips_collection[] = [
                'trip' =>$trip,
                'operator' => $operator_details[0],
                'bus_type' => $bus_details[0],
                'destpark' => $dest_prk_details[0],
                'sourcepark' => $src_prk_details[0]
            ];
        }
        $final_collection = [
            'trips' => $trips_collection,
            'operators' => (array) $operators,
            'busTypes' => (array) $busTypes,
            'trip_count' => $trip_count,
            'fromPark' => (array) $fromPark[0],
            'toPark' => (array) $toPark[0]
        ];

        if ($trip_count !== 0) {
           
            return $this->utility->returnJson($final_collection, true, 'Trips data');
        } else {
            return $this->utility->returnJson(null, false, 'No Trips data');
        }
    }

    public function GetTrip(Slim $request){
        $form_data = $request->request->post();
        $trip_id = isset($form_data['trip_id'])? $form_data['trip_id']: "";
        $departure_date = isset($form_data['departure_date'])? $form_data['departure_date']: "";
        $trip_sql = "SELECT * FROM trips WHERE id = '$trip_id'";
        $_trip = $this->dbService->executeQuery($trip_sql);
        $trip = (array) $_trip;
        $op_id = $trip[0]->operator_id;
        $bus = new BusService();
        $park = new ParksService();
        $bus_details = $bus->getBusDetails($trip[0]->bus_type_id);
        $operator_details = $park->getOperator($trip[0]->operator_id);
        $src_prk_details = $park->getPark($trip[0]->source_park_id);
        $dest_prk_details = $park->getPark($trip[0]->dest_park_id);
        $seats = !empty($bus->getSeats($departure_date,$trip_id)) ? $bus->getSeats($departure_date,$trip_id):[ 0 => []];
        $final_collection = [
            'trip' => $trip[0],
            'operator' => (array) $operator_details[0],
            'busType' => (array) $bus_details[0],
            'destpark' => $dest_prk_details[0],
            'sourcepark' => $src_prk_details[0],
            'seats' => (array) $seats[0]
        ];
        return $this->utility->returnJson($final_collection,true,'A single trip');
    }

    public function GetTripFromBookingId(Slim $request){
        $form_data = $request->request->post();
        $booking_id = isset($form_data['booking_id'])? $form_data['booking_id']: "";
        $departure_date = isset($form_data['departure_date'])? $form_data['departure_date']: "";
        $trip_sql = "SELECT * FROM trips WHERE booking_id = '$booking_id'";
        $_trip = $this->dbService->executeQuery($trip_sql);
        $trip = (array) $_trip;
        $op_id = $trip[0]->operator_id;
        $bus = new BusService();
        $park = new ParksService();
        $bus_details = $bus->getBusDetails($trip[0]->bus_type_id);
        $operator_details = $park->getOperator($trip[0]->operator_id);
        $src_prk_details = $park->getPark($trip[0]->source_park_id);
        $dest_prk_details = $park->getPark($trip[0]->dest_park_id);
        $seats = !empty($bus->getSeats($departure_date,$trip[0]->id)) ? $bus->getSeats($departure_date,$trip[0]->id):[ 0 => []];
        $final_collection = [
            'trip' => $trip[0],
            'operator' => (array) $operator_details[0],
            'busType' => (array) $bus_details[0],
            'destpark' => $dest_prk_details[0],
            'sourcepark' => $src_prk_details[0],
            'seats' => (array) $seats[0]
        ];
        return $this->utility->returnJson($final_collection,true,'A single trip');
    }

    public function getTripsPrices($operator){
        $query = "SELECT * FROM operators WHERE code = '$operator' LIMIT 1";
        $operator_details = $this->dbService->executeQuery($query, true);
        $operator_id = $operator_details[0]->id;
        $parks_query = "SELECT * FROM parks";
        $park_details = $this->dbService->executeQuery($parks_query, true);
        $trip = "SELECT (SELECT name FROM parks WHERE id = source_park_id) as source_park , (SELECT name FROM parks WHERE id = dest_park_id) as destination, fare FROM trips WHERE operator_id = '$operator_id'";
        $trip_details = $this->dbService->executeQuery($trip, true);
        $collection = [
            'operator' => $operator_details,
            'parks' => $park_details,
            'trips' => $trip_details
        ];
        return $this->utility->returnJson($collection, 200, 'Get Operator Trips');
    }

}
