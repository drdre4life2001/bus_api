<?php

namespace Service;

use Libraries\Utilities;
use Service\DatabaseService;
use Slim\Slim;
use Service\ApiService;
use Service\CustomerService;
use Service\MeterService;
use Service\PaymentService;


class ParksService
{

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->apiService = new \Service\ApiService();
        $this->customerService = new \Service\CustomerService();
        $this->utilities = new Utilities();
    }

    public function getParks()
    {
        $query = "SELECT * FROM parks WHERE active = 1";
        $databaseService = new DatabaseService();
        $parks = $databaseService->executeQuery($query);
        $result = $this->utilities->returnJson($parks,200,null);
        return $result;
    }

    public function getSinglePark(Slim $request = null, $park_id = null){
        $databaseService = new DatabaseService();
        $form_data = isset($request) ? $request->request->post():'';
        $src_id = isset($request) ? $form_data['park_id'] : $park_id;
        $query = "SELECT * FROM parks WHERE id = '$src_id'";
        $response = $databaseService->executeQuery($query);
        if(!empty($response)){
            return $this->utilities->returnJson($response[0],true,null);
        }else{
            return $this->utilities->returnJson($response,false,null);
        }
    }

    public function getPark($park_id = null){
        $databaseService = new DatabaseService();
        $query = "SELECT * FROM parks WHERE id = '$park_id'";
        $response = $databaseService->executeQuery($query);
        return $response;
    }

    public function getBanks()
    {
        $banks_query = "SELECT * FROM banks WHERE active = 1";
        $cash_office_query = "SELECT * FROM cash_offices";
        $databaseService = new DatabaseService();
        $banks = $databaseService->executeQuery($banks_query);
        $cash_offices = $databaseService->executeQuery($cash_office_query);
        $collection = [
            'banks' => $banks,
            'cash_offices' => $cash_offices
        ];
        $result = $this->utilities->returnJson($collection,200,null);
        return $result;
    }

    public function getOperator($id)
    {
        $query = "SELECT 
                  id,name,code,contact_person,phone,email,details,local_terms,
                  commission,booking_rules,local_children_discount,intl_children_discount,
                  intl_terms,img,should_sync,active,rule FROM operators WHERE id = '$id'";
        $databaseService = new DatabaseService();
        $operator = $databaseService->executeQuery($query);
        return $operator;
    }

    public function getOperators()
    {
        $query = "SELECT id,name,img FROM operators";
        $databaseService = new DatabaseService();
        $operator = $databaseService->executeQuery($query);
        return $operator;
    }

    public function getDestParks($park_id = null){
        $query = "SELECT * FROM parks WHERE active = 1";
        $databaseService = new DatabaseService();
        $parks = $databaseService->executeQuery($query);
        $value =  json_encode($parks);
        $parks = json_decode($value, true);
        $newparks = [];
        foreach ($parks as $park){
            $Pids = $park['id'];
            $query = "SELECT dest_park_id FROM trips WHERE active = 1 AND source_park_id = '$Pids'";
            $dest_park_ids = $databaseService->executeQuery($query);

            $ids = [];
            foreach ($dest_park_ids as $des){
                $ids[] = $des->dest_park_id;
            }
            $ids = implode(',', $ids);
            $destinations = '('.$ids.')';
            $result = "SELECT id, name FROM parks WHERE id IN $destinations ORDER BY name ASC";
            $dests = $databaseService->executeQuery($result);
            if($dests>=1){
                $newparks[] = $park;
            }

        }

        return $this->utilities->returnJson($newparks,true,null);
    }

    public function GetDests(Slim $request){
        $databaseService = new DatabaseService();
        $form_data = $request->request->post();
        $src_id = $form_data['source_park_id'];
        $query = "SELECT dest_park_id FROM trips WHERE active = 1 AND source_park_id = '$src_id'";
        $dest_park_ids = $databaseService->executeQuery($query);
        $ids = [];
        foreach ($dest_park_ids as $des){
            $ids[] = $des->dest_park_id;
        }
        $ids = implode(',', $ids);
        $destinations = '('.$ids.')';
        $result = "SELECT id, name FROM parks WHERE id IN $destinations ORDER BY name ASC";
        $dests = $databaseService->executeQuery($result);
        if(!empty($dests)){
            return $this->utilities->returnJson($dests,true,null);
        }else{
            return $this->utilities->returnJson($dests,false,null);
        }
    }

}
