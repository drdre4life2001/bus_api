<?php
namespace Libraries;
/**
 * Created by PhpStorm.
 * User: noibilism
 * Date: 3/30/17
 * Time: 6:30 PM
 */
class Utilities
{
    function array_map_assoc($callback, $array)
    {
        $r = array();
        foreach ($array as $key => $value)
            $r[$key] = $callback($key, $value);
        return $r;
    }

    function multi_implode($array, $glue)
    {
        $ret = '';

        foreach ($array as $item) {
            if (is_array($item)) {
                $ret .= multi_implode($item, $glue) . $glue;
            } else {
                $ret .= $item . $glue;
            }
        }

        $ret = substr($ret, 0, 0 - strlen($glue));

        return $ret;
    }

    function is_valid_xml($xml)
    {
        libxml_use_internal_errors(true);
        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml);
        $errors = libxml_get_errors();
        return empty($errors);
    }

    function getJsonData($object){
        $var = get_object_vars($object);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value,'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

    function decode_token($token){
        $decoded_token = base64_decode($token);
        $bare_token_data = explode('|', $decoded_token);
        return $bare_token_data;
    }

    function replicate_string($string = null, $frequency = null){
        $new_array = [];
        for($i = 1; $i <=$frequency; $i++){
            $new_array[$i] = $string;
        }
        return implode('',$new_array);
    }

    function returnJson( $data = null, $status = null, $msg = null){
        $json_data = [
            'status' => $status,
            'data' => $data,
            'message' => $msg
        ];
        return json_encode($json_data);
    }

    function validate_data(array $data){
        $errors = [];
        foreach ($data as $datum){
            if(empty($datum)){
                $errors[] = true;
            }
        }
        if(!empty($errors)){
            return $this->returnJson('Invalid Data Set',400,'Data Validation Error');
        }else{
            return true;
        }
    }

}