<?php
require 'vendor/autoload.php';
require 'services/CustomerService.php';
require 'services/ParksService.php';
require 'services/AffiliateService.php';
require 'services/StatesService.php';
require 'services/BusService.php';
require 'services/TripsService.php';
require 'services/BookingService.php';
require 'vendor/tuupola/slim-jwt-auth/src/JwtAuthentication.php';

$app = new \Slim\Slim();

$authenticateApi = function (\Slim\Slim $slim) {
    return function () use ($slim) {
        if ($slim->request->isGet()) {
            $token = $slim->request->get('token');
        } else {
            $token = $slim->request->post('token');

        }
//        $apiService = new \Service\ApiService();
//        $auth = $apiService->authenticate_api_call($token);
//        if ($auth['status'] === 'error') {
//            echo json_encode(['status' => 'Error!', 'message' => $auth['message']]);
//            exit;
//        }
    };
};

$app->error(function (\ErrorException $e) use ($app) {
    $app->render(json_encode(['status' => 'Error!', 'message' => $e->getMessage()]));
});

$app->notFound(function () {
    echo json_encode(['status' => 'Error!', 'message' => 'Route not found!']);
});

$app->response->headers->set('Content-Type', 'application/json');

$app->post('/get-parks', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $result = $parkService->getParks();
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-operators', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $utilities = new \Libraries\Utilities();
        $result = $parkService->getOperators();
        echo $utilities->returnJson($result, true, null);
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->get('/banks', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $result = $parkService->getBanks();
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->get('/get_operator_trips/:operator', $authenticateApi($app), function ($operator) use ($app) {
    try {
        $parkService = new \Service\TripsService();
        $result = $parkService->getTripsPrices($operator);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-dest-parks', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $result = $parkService->getDestParks();
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-feedbacks', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\CustomerService();
        $result = $parkService->getFeedbacks();
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-dests', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $result = $parkService->GetDests($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/single-park', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\ParksService();
        $result = $parkService->getSinglePark($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/search-trip', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\TripsService();
        $result = $parkService->searchTrips($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-trip', $authenticateApi($app), function () use ($app) {
    try {
        $parkService = new \Service\TripsService();
        $result = $parkService->GetTrip($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-customer-data', $authenticateApi($app), function () use ($app) {
    try {
        $customerService = new \Service\CustomerService();
        $result = $customerService->GetCustomerData($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-customer-data-from-phone', $authenticateApi($app), function () use ($app) {
    try {
        $customerService = new \Service\CustomerService();
        $phone = $app->request->post('phone');
        $result = $customerService->selectACustomerFromCustomers($phone);
        $util = new \Libraries\Utilities();
        $result = $util->returnJson($result, 200, 'Single Customer');
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-booking', $authenticateApi($app), function () use ($app) {
    try {
        $bookingService = new \Service\BookingService();
        $result = $bookingService->saveBooking($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-customer', $authenticateApi($app), function () use ($app) {
    try {
        $bookingService = new \Service\CustomerService();
        $result = $bookingService->SaveCustomer($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-psg', $authenticateApi($app), function () use ($app) {
    try {
        $bookingService = new \Service\BookingService();
        $result = $bookingService->savePassenger($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-booked-seat', $authenticateApi($app), function () use ($app) {
    try {
        $busService = new \Service\BusService();
        $result = $busService->saveBookedSeats($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-trip-from-booking-id', $authenticateApi($app), function () use ($app) {
    try{
        $bookingService = new \Service\BookingService();
        $result = $bookingService->GetTripFromBookingCode($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-customer-booking-by-id', $authenticateApi($app), function () use ($app) {
    try{
        $customerService = new \Service\BookingService();
        $result = $customerService->getCustomerBookingById($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-charter', $authenticateApi($app), function () use ($app) {
    try{
        $bookingService = new \Service\BookingService();
        $result = $bookingService->saveCharterBooking($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/save-transactions', $authenticateApi($app), function () use ($app) {
    try{
        $bookingService = new \Service\BookingService();
        $result = $bookingService->saveTransactions($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-trip-from-chartered-booking-id', $authenticateApi($app), function () use ($app) {
    try{
        $bookingService = new \Service\BookingService();
        $result = $bookingService->getTripFromCharterId($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/get-agent-token', function () use ($app) {
    try{
        $apiService = new \Service\ApiService();
        $result = $apiService->encodeAgentDetails($app);
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->get('/', $authenticateApi($app), function () use ($app) {
    echo json_encode(['status' => 'Ok', 'message' => 'welcome']);
});


/**
 *
 * State Routes
 *
 */

$app->get('/states/index', function () use ($app) {
    $states = new \Service\StatesService();
    $result = $states->index();
    echo $result;
});


/**
 *
 * Affiliate Routes
 *
 */

$app->get('/affiliates/index', function () use ($app) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->index();
    echo $result;
});

$app->get('/affiliates/view/:affiliate_id', function($affiliate_id) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->view($affiliate_id);
    echo $result;
});

$app->post('/affiliates/add', function () use ($app) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->add($app);
    echo $result;
});
$app->delete('/affiliates/delete/:affiliate_id}', function($affiliate_id) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->delete($affiliate_id);
    echo $result;
});


$app->get('/affiliates/getAffiliateBookings/:affiliate_id(/:start_date(/:end_date))', function ($affiliate_id, $start_date = null, $end_date = null ) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->getAffiliateBookings($affiliate_id, $start_date = null, $end_date = null);
    echo $result;
});


$app->get('/affiliates/getAffiliateBookingsTotalAmountToBePaid/:affiliate_id(/:start_date(/:end_date))', function($affiliate_id, $start_date = null,$end_date = null ) {
    $affiliateService = new \Service\AffiliateService();
    $result = $affiliateService->getAffiliateBookingsTotalAmountToBePaid($affiliate_id,$start_date = null,$end_date = null );
    echo $result;
});

/**
 *
 * For Zenith Bank
 *
 */

$app->post('/verify-booking', $authenticateApi($app), function () use ($app) {
    try {
        $booking_code = $app->request->post('booking_code');
        $bookingService = new \Service\BookingService();
        $result = $bookingService->getBookingDetailsWithCode($booking_code);
        $util = new \Libraries\Utilities();
        $result = $util->returnJson($result, 200, 'Booking Verified');
        echo $result;
    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});

$app->post('/booking_payment_zenithbank', $authenticateApi($app), function () use ($app) {
    try {
        $booking_code = $app->request->post('booking_code');
        $booking_id = $app->request->post('booking_id');
        $amount = $app->request->post('amount');
        $status = $app->request->post('status');
        $zenith_response = $app->request->post('response');
        $bookingService = new \Service\BookingService();
        $booking = $bookingService->getBookingDetailsWithCode($booking_code);
        if($amount == $booking[0]->final_cost){
            $result = $bookingService->saveZenithTransactions($booking_code, $booking_id, $status, $zenith_response);
            echo $result;
        }

    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});


$app->post('/update-booking-payment', $authenticateApi($app), function () use ($app) {
    try {
        $booking_code = $app->request->post('booking_code');
        $booking_id = $app->request->post('booking_id');
        $amount = $app->request->post('amount');
        $status = $app->request->post('status');
        $zenith_response = $app->request->post('response');
        $bookingService = new \Service\BookingService();
        $booking = $bookingService->getBookingDetailsWithCode($booking_code);
        if($amount == $booking[0]->final_cost){
            $result = $bookingService->saveZenithTransactions($booking_code, $booking_id, $status, $zenith_response);
            echo $result;
        }

    } catch (\ErrorException $exception) {
        echo json_encode(['status' => 'Error!', 'message' => $exception->getMessage()]);
    }
});
$app->run();