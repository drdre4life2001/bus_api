<?php

namespace Service;

require 'services/DatabaseService.php';
require 'services/ApiService.php';
require 'Libraries/Utilities.php';

use Libraries\Utilities;
use Slim\Slim;


class CustomerService
{

    private $dbService = null;
    private $apiService = null;
    private $apiRef = null;
    private $utility = null;
    private $paymentService = null;
    private $feederService = null;

    public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->apiService = new \Service\ApiService();
        $this->utility = new Utilities();
    }

    public function GetCustomerData(Slim $request)
    {
        $form_data = $request->request->post();
        $customer = self::selectACustomer($form_data['phone']);
        return $this->utility->returnJson($customer[0],200,'Customer Data');
    }

    public function selectACustomer($phone = null)
    {
        $query = "SELECT * FROM bookings WHERE contact_phone='$phone' LIMIT 1";
        $customer = $this->dbService->executeQuery($query);
        return $customer;
    }

    public function selectACustomerFromCustomers($phone = null)
    {
        $query = "SELECT * FROM customers WHERE phone='$phone' LIMIT 1";
        $customer = $this->dbService->executeQuery($query);
        return $customer;
    }

    public function getFeedbacks()
    {
        $query = "SELECT * FROM feedbacks WHERE active= 1";
        $feedbacks = $this->dbService->executeQuery($query);
        return $this->utility->returnJson($feedbacks,200,'Customer Data');
    }

    public function SaveCustomer(Slim $request)
    {
        $form_data = $request->request->post();
        $customer = self::selectACustomerFromCustomers($form_data['phone']);
        if(empty($customer)){
            $password = sha1($form_data['phone']);
                $name = $form_data['name'];
                $email = $form_data['email'];
                $phone = $form_data['phone'];
                $password = $password;
            $query = "INSERT INTO customers (name, email, phone, password) VALUES ('$name','$email','$phone','$password')";
            if($this->dbService->executeSaveQuery($query))
                $customer_details = self::selectACustomerFromCustomers($phone);
            else
                $customer_details = [];
        }else{
            $customer_details = $customer;
        }
        return $this->utility->returnJson($customer_details[0],200,'Single customer');
    }
}
