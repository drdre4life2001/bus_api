<?php

namespace Service;

use Libraries\Utilities;
use Service\DatabaseService;
use Slim\Slim;
use Service\ApiService;

/**
 * Handle all operations partaining to the bus Affiliate program
 * @author Dave Partner Ozoalor
 */
class StatesService
{
 public function __construct()
    {
        $this->dbService = new \Service\DatabaseService();
        $this->apiService = new \Service\ApiService();
        $this->utilities = new Utilities();
    }

    /**
     *
     * Get all states in the system
     * 
     * 
     */
    public function index()
    {
        $query = "SELECT * FROM states";
        $databaseService = new DatabaseService();
        $states = $databaseService->executeQuery($query);
        $result = 0;
        if($states){
		 $result = $this->utilities->returnJson($states, 200, "success");
		}else{
			 $result = $this->utilities->returnJson($result,false,'Error: query failed');
		}
        return $result;
    }
}
