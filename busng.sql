-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 02, 2018 at 07:27 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `busng`
--

-- --------------------------------------------------------

--
-- Table structure for table `affiliates`
--

CREATE TABLE `affiliates` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `country_id` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `affiliates`
--

INSERT INTO `affiliates` (`id`, `email`, `password`, `phone`, `first_name`, `last_name`, `gender`, `city`, `state`, `state_id`, `country_id`, `active`) VALUES
(1, 'davepartner@gmail.com', 'wesdsfsdsds', '08068944517', 'Dave', 'Partner', 'male', 'VI', 'Lagos', '36', '24', 1),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` text COLLATE utf8_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `address` text COLLATE utf8_unicode_ci,
  `notes` text COLLATE utf8_unicode_ci,
  `balance` double NOT NULL DEFAULT '0',
  `percentage` double(8,2) NOT NULL DEFAULT '0.00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `agent_code` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `email`, `username`, `password`, `api_key`, `last_login`, `phone`, `active`, `address`, `notes`, `balance`, `percentage`, `deleted_at`, `created_at`, `updated_at`, `agent_code`) VALUES
(5, 'BUS Master Agent', 'info@bus.com.ng', 'wale', 'password', '3a4f74acacaa4366a99246d94898d4ed', NULL, '0803242', 1, 'fgdf\r\nd\r\nfg\r\ndf\r\ngd', NULL, 117250, 3.00, NULL, '2016-03-24 04:08:16', '2016-03-24 05:33:17', 'BUS-2003-CS'),
(6, 'Zenith Bank', 'info@zenithbank.com', 'zenith', 'zenith', 'cdd74403-6b13-42aa-beaa-92e9e0a4b312', NULL, '0803242', 1, 'fgdf\r\nd\r\nfg\r\ndf\r\ngd', NULL, 117250, 3.00, NULL, '2016-03-24 04:08:16', '2016-03-24 05:33:17', 'BUS-2017-ZN');

-- --------------------------------------------------------

--
-- Table structure for table `agent_transactions`
--

CREATE TABLE `agent_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `amount_paid` double NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agent_transactions`
--

INSERT INTO `agent_transactions` (`id`, `agent_id`, `type`, `value`, `amount_paid`, `approved`, `deleted_at`, `created_at`, `updated_at`) VALUES
(16, 5, 'TOPUP', 20000, 20000, 1, NULL, '2016-03-24 04:39:38', '2016-03-24 04:39:38'),
(17, 5, 'BOOKING', 5750, 5750, 1, NULL, '2016-03-24 05:05:36', '2016-03-24 05:05:36'),
(18, 5, 'TOPUP', 103000, 100000, 1, NULL, '2016-03-24 05:33:17', '2016-03-24 05:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acount_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `acount_number`, `account_name`, `rank`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Stanbic Back', '0019630518', 'Transport software limited', 0, 1, NULL, NULL, NULL),
(2, 'Diamond bank', '0084757723', 'Transport Software Limited', 1, 1, NULL, NULL, NULL),
(3, 'Zenith Bank Plc', '1014884671', 'Transport Software Limited', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(225) DEFAULT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `body` text,
  `active` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `slug`, `body`, `active`, `created_at`, `updated_at`) VALUES
(1, '#BeLikeSheila', 'belikesheila', '<p>Be like Sheila, Sheila is smart and books her tickets online eliminating the need to join long ticket queues, while Gloria has to endure long queues and process just to get her ticket. Follow us on Social Media and watch Sheila as grows smarter.</p>\r\n', 1, '2017-01-13 12:56:42', '2017-01-13 12:56:42'),
(2, 'Our Charter Services', 'charter-services', '<p>Book your charter services with us, with destinations to over 30 states nationwide including trips to neighbouring countries like Lome, Cotonou, Kumassi and Accra, planning the holidays just got very convenient. Charter buses for events and holidays. Go now to bus.com.ng.</p>\r\n', 1, '2017-01-13 12:57:18', '2017-01-13 12:59:57'),
(3, 'We are hiring!!', 'we-are-hiring', '<p><img alt=\"\" src=\"http://saskwrgc.com/blogs/image/we-are-hiring.png\" style=\"height:210px; width:450px\" /></p>\r\n', 1, '2017-01-23 14:15:07', '2017-01-23 14:15:07'),
(4, 'Bus', 'bus', '<p><img alt=\"\" src=\"http://www.japan-guide.com/g2/2366_04.jpg\" style=\"height:221px; width:500px\" /></p>\r\n', 0, '2017-02-03 12:25:23', '2017-02-03 12:30:55');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `trip_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `passenger_count` int(11) NOT NULL,
  `unit_cost` double NOT NULL,
  `passport_cost` double DEFAULT NULL,
  `final_cost` double NOT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `paid_date` datetime DEFAULT NULL,
  `booking_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `next_of_kin_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_intl_trip` tinyint(1) NOT NULL DEFAULT '0',
  `passport_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_expiry_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_date_of_issue` date DEFAULT NULL,
  `passport_place_of_issue` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_booking_id` int(10) UNSIGNED DEFAULT NULL,
  `operator_booking_id` int(10) UNSIGNED DEFAULT NULL,
  `agent_id` int(10) UNSIGNED DEFAULT NULL,
  `departure_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affiliate_paid_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affiliate_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `trip_id`, `date`, `passenger_count`, `unit_cost`, `passport_cost`, `final_cost`, `payment_method_id`, `bank_id`, `customer_id`, `status`, `paid_date`, `booking_code`, `contact_name`, `contact_phone`, `contact_email`, `contact_gender`, `next_of_kin`, `next_of_kin_phone`, `is_intl_trip`, `passport_type`, `passport_number`, `passport_expiry_date`, `passport_date_of_issue`, `passport_place_of_issue`, `parent_booking_id`, `operator_booking_id`, `agent_id`, `departure_date`, `deleted_at`, `created_at`, `updated_at`, `affiliate_paid_status`, `affiliate_id`) VALUES
(798, 1408, '2017-12-25', 1, 10000, NULL, 10350, NULL, NULL, 0, 'CANCELLED', NULL, 'OKY765815', 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'Male', 'Mummy', '08080764159', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25', NULL, '2017-12-24 06:24:15', NULL, NULL, NULL),
(799, 1402, '2017-12-26', 5, 9500, NULL, 48650, NULL, NULL, 0, 'PENDING', NULL, 'OKY563820', 'Donkells Emmanuel ', '08108254514', 'Donkells ', 'Male', 'Mr silvernus', '08064664078', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26', NULL, '2017-12-24 16:34:24', NULL, NULL, NULL),
(800, 1367, '2017-12-25', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY141252', 'Susan Ohaekelem', '08064795286', 'nnfankon@yahoo.com', 'Female', 'Frank Nwigwe', '07039222660', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25', NULL, '2017-12-25 02:39:44', NULL, NULL, NULL),
(801, 1367, '2017-12-25', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY785118', 'Susan Ohaekelem', '08064795286', 'nnfankon@yahoo.com', 'Female', 'Frank Nwigwe', '07039222660', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25', NULL, '2017-12-25 02:41:20', NULL, NULL, NULL),
(802, 1415, '2017-12-25', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY964571', 'Susan Ohaekelem', '08064795186', 'nnfankon@yahoo.com', 'Female', 'Frank', '07039222660', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25', NULL, '2017-12-25 03:16:23', NULL, NULL, NULL),
(803, 1399, '2017-12-27', 2, 11000, NULL, 22550, NULL, NULL, 0, 'PENDING', NULL, 'OKY526703', 'chidoziem joseph Ekeocha', '08063739256', 'echidoziem@yahoo.com', 'Male', 'chukwuemeka', '08065318504', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-25 15:34:45', NULL, NULL, NULL),
(804, 1433, '2017-12-27', 2, 8500, NULL, 17550, NULL, NULL, 0, 'PENDING', NULL, 'OKY717991', 'Metu Anthonia chioma', '8032794717', 'toniametu@gmail.com', 'Female', 'METU TONIA', '08032794717', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-26 15:05:17', NULL, NULL, NULL),
(805, 1433, '2017-12-27', 2, 8500, NULL, 17550, NULL, NULL, 0, 'PENDING', NULL, 'OKY334833', 'Metu Anthonia chioma', '8032794717', 'toniametu@gmail.com', 'Female', 'Mrs Joyce Afolabi', '080668777888', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-26 15:08:01', NULL, NULL, NULL),
(806, 1433, '2017-12-27', 2, 8500, NULL, 17550, NULL, NULL, 0, 'PENDING', NULL, 'OKY964678', 'Metu Anthonia chioma', '08032794717', 'toniametu@gmail.com', 'Female', 'Mrs Joyce Afolabi', '08066777888', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-26 15:39:02', NULL, NULL, NULL),
(807, 1433, '2017-12-27', 2, 8500, NULL, 17550, NULL, NULL, 0, 'PENDING', NULL, 'OKY691496', 'Metu Anthonia chioma', '8032794717', 'toniametu@gmail.com', 'Female', 'Mrs Joyce Afolabi', '08066777888', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-26 15:50:08', NULL, NULL, NULL),
(808, 1433, '2017-12-27', 2, 8500, NULL, 17550, NULL, NULL, 0, 'PENDING', NULL, 'OKY826707', 'Metu Anthonia chioma', '8032794717', 'toniametu@gmail.com', 'Female', 'Mrs Joyce Afolabi', '08066777888', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27', NULL, '2017-12-26 16:08:02', NULL, NULL, NULL),
(809, 1465, '2017-12-28', 1, 8500, NULL, 8850, NULL, NULL, 0, 'PENDING', NULL, 'OKY456616', 'Nmadu Sharon ', '08165504771', 'babe.sharon@rocketmail.com', 'Female', 'Nmadu Ifolo ', '08035154197', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-28', NULL, '2017-12-26 20:01:13', NULL, NULL, NULL),
(810, 1408, '2017-12-29', 1, 10000, NULL, 10350, NULL, NULL, 0, 'PENDING', NULL, 'OKY648943', 'simonl loveth', '07063560600', 'simonloveth73  gmail.com', 'Female', 'father', '08033737556', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-29', NULL, '2017-12-26 22:12:17', NULL, NULL, NULL),
(811, 1408, '2017-12-29', 1, 10000, NULL, 10350, NULL, NULL, 0, 'PENDING', NULL, 'OKY886096', 'simonl loveth', '07063560600', 'simonloveth73  gmail.com', 'Female', 'simon loveth', '08033737556', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-29', NULL, '2017-12-26 22:13:33', NULL, NULL, NULL),
(812, 1467, '2017-12-28', 1, 12000, NULL, 12350, NULL, NULL, 0, 'PENDING', NULL, 'OKY711312', 'Onyejiaka Michael ', '08137017944', 'respectchammer@gmail.com', 'Male', 'Anita', '08140658889', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-28', NULL, '2017-12-27 19:39:06', NULL, NULL, NULL),
(813, 1467, '2017-12-28', 1, 12000, NULL, 12350, NULL, NULL, 0, 'PENDING', NULL, 'OKY620089', 'Onyejiaka Michael ', '08137017944', 'respectchammer@gmail.com', 'Male', 'Anita Onyejiaka', '08140658889', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-28', NULL, '2017-12-27 19:49:53', NULL, NULL, NULL),
(814, 1467, '2017-12-28', 1, 12000, NULL, 12350, NULL, NULL, 0, 'PENDING', NULL, 'OKY912405', 'Onyejiaka Michael ', '08137017944', 'respectchammer@gmail.com', 'Male', 'Anita Onyejiaka', '08140658889', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-28', NULL, '2017-12-27 19:51:12', NULL, NULL, NULL),
(815, 1434, '2017-12-30', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY122530', 'Oko Chiedozie', '08063466263', 'oko.chiedozie@yahoo.com', 'Male', 'Osita Oko', '08068522636', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30', NULL, '2017-12-28 23:36:58', NULL, NULL, NULL),
(816, 1410, '2018-04-01', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY310597', 'JACOB IKECHUKWU', '07066625624', 'ikjacob99@gmail.com', 'Male', 'JACOB ORJI', '08101029957', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-01', NULL, '2017-12-29 07:26:12', NULL, NULL, NULL),
(817, 1431, '2017-12-30', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY482398', 'uchechukwu nuela ', '09029891479 ', '', 'Female', 'uchechukwu ', '08165370959 ', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30', NULL, '2017-12-29 17:19:30', NULL, NULL, NULL),
(818, 1404, '2017-01-02', 3, 11500, NULL, 35250, NULL, NULL, 0, 'PENDING', NULL, 'OKY369890', 'Christiana eze', '08109995971', 'Christianaeze78@gmail.com', 'Female', 'Emmanuel eze', '08118036151', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-02', NULL, '2017-12-31 22:06:42', NULL, NULL, NULL),
(819, 1404, '2017-01-02', 3, 11500, NULL, 35250, NULL, NULL, 0, 'PENDING', NULL, 'OKY618562', 'Christiana eze', '08109995971', 'Christianaeze78@gmail.com', 'Female', 'Emmanuel eze', '08118036151', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-02', NULL, '2017-12-31 22:22:20', NULL, NULL, NULL),
(820, 1368, '1970-01-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY213339', 'Edward Jumbo ', '08148319100 ', 'Edwardjumbo101@gmail.com ', 'Male', 'Mr. Jumbo ', '07034413220', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '2018-01-01 15:42:30', NULL, NULL, NULL),
(821, 1368, '1970-01-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY295530', 'Edward Jumbo ', '08148319100 ', 'Edwardjumbo101@gmail.com ', 'Male', 'Mr. Jumbo ', '07034413220', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '2018-01-01 15:43:08', NULL, NULL, NULL),
(822, 1432, '2018-01-08', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY667743', 'vivian', '07035088414', 'vivianomereonye@gmail.com', 'Female', 'mr collins omereonye', '09056438971', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-08', NULL, '2018-01-03 09:25:15', NULL, NULL, NULL),
(823, 1400, '2018-01-08', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY554807', 'chiamaka vivian omereonye', '09087237645', 'vivianomereonye@gmail.com', 'Female', 'mr collins omereonye', '08076443126', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-08', NULL, '2018-01-03 09:29:03', NULL, NULL, NULL),
(824, 1400, '2018-01-08', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY775842', 'chiamaka vivian omereonye', '09087237645', 'vivianomereonye@gmail.com', 'Female', 'mr collins omereonye', '08076443126', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-08', NULL, '2018-01-03 09:29:25', NULL, NULL, NULL),
(825, 1434, '2018-01-04', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY205772', 'Mgbakor George', '08029363227', 'nwamelogo.gm@gmail.com', 'Male', 'Chinyere Mgbakor', '08034341889', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-04', NULL, '2018-01-04 02:16:55', NULL, NULL, NULL),
(826, 1436, '2018-01-12', 2, 11500, NULL, 23550, NULL, NULL, 0, 'PENDING', NULL, 'OKY920445', 'jon', '08098767892', 'jon@yahoo.com', 'Male', 'mome', '08098767892', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-12', NULL, '2018-01-08 11:26:48', NULL, NULL, NULL),
(827, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY763006', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 17:40:04', NULL, NULL, NULL),
(828, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY436214', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 17:40:20', NULL, NULL, NULL),
(829, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY102244', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 17:41:23', NULL, NULL, NULL),
(830, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY699917', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 17:45:19', NULL, NULL, NULL),
(831, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY937482', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 17:46:34', NULL, NULL, NULL),
(832, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY570808', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 18:25:25', NULL, NULL, NULL),
(833, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY737340', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ajayi Nurudeen', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 23:01:21', NULL, NULL, NULL),
(834, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY338502', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ajayi Nurudeen', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 23:02:11', NULL, NULL, NULL),
(835, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY188517', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ajayi Nurudeen', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 23:08:24', NULL, NULL, NULL),
(836, 1399, '2018-02-01', 1, 11000, NULL, 11350, NULL, NULL, 0, 'PENDING', NULL, 'OKY768526', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ajayi Nurudeen', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-01-31 23:13:50', NULL, NULL, NULL),
(855, 1400, '2018-02-01', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS646213', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01', NULL, '2018-02-01 01:32:28', NULL, NULL, NULL),
(869, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY247403', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:04:53', NULL, NULL, NULL),
(870, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY733921', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:06:50', NULL, NULL, NULL),
(874, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY238900', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:12:47', NULL, NULL, NULL),
(875, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY990577', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:13:35', NULL, NULL, NULL),
(876, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY893335', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:14:22', NULL, NULL, NULL),
(877, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY369537', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:15:23', NULL, NULL, NULL),
(878, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY923960', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:16:34', NULL, NULL, NULL),
(879, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY385511', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:28:36', NULL, NULL, NULL),
(880, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY258808', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:37:13', NULL, NULL, NULL),
(881, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY577914', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:51:01', NULL, NULL, NULL),
(882, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY148053', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:51:26', NULL, NULL, NULL),
(883, 1405, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY256662', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', '', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:52:51', NULL, NULL, NULL),
(884, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY206533', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 11:59:16', NULL, NULL, NULL),
(885, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY694266', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:12:52', NULL, NULL, NULL),
(886, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY854434', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:15:26', NULL, NULL, NULL),
(887, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY672634', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:20:36', NULL, NULL, NULL),
(888, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY811289', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:27:10', NULL, NULL, NULL),
(889, 1402, '2018-02-02', 1, 9500, NULL, 9850, NULL, NULL, 0, 'PENDING', NULL, 'OKY289546', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:31:49', NULL, NULL, NULL),
(890, 1404, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY477238', 'Olatunji', '08022888282', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:36:07', NULL, NULL, NULL),
(891, 1404, '2018-02-02', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY116829', 'Segun olawhite', '08022888282', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 12:36:58', NULL, NULL, NULL),
(901, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS848036', 'Olatunji', '08022888282', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 13:53:32', NULL, NULL, NULL),
(915, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS838494', 'Olatunji', '0802090127', '', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 15:48:23', NULL, NULL, NULL),
(916, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS205299', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:07:22', NULL, NULL, NULL),
(917, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS631746', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:08:14', NULL, NULL, NULL),
(918, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS373160', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:08:19', NULL, NULL, NULL),
(919, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS440809', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:09:21', NULL, NULL, NULL),
(920, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS968657', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:10:26', NULL, NULL, NULL),
(921, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS233196', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:15:14', NULL, NULL, NULL),
(922, 400, '2018-02-02', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS526272', 'Segun owalata', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02', NULL, '2018-02-01 16:20:28', NULL, NULL, NULL),
(923, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS962057', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:27:37', NULL, NULL, NULL),
(924, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS195102', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:28:35', NULL, NULL, NULL),
(925, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS320778', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:29:52', NULL, NULL, NULL),
(926, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS177378', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:30:12', NULL, NULL, NULL),
(927, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS772129', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:32:39', NULL, NULL, NULL),
(928, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS719469', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:34:40', NULL, NULL, NULL),
(929, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS423664', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 00:36:26', NULL, NULL, NULL),
(930, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS373080', 'Segun olawhite', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 05:56:05', NULL, NULL, NULL),
(931, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS946957', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 06:03:28', NULL, NULL, NULL),
(932, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS285664', 'Olatunji', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '080123281948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 06:04:08', NULL, NULL, NULL),
(933, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS346747', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:18:36', NULL, NULL, NULL),
(934, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS986831', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:19:18', NULL, NULL, NULL),
(935, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS354393', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:25:37', NULL, NULL, NULL),
(936, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS933287', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:26:38', NULL, NULL, NULL),
(937, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS111660', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:27:29', NULL, NULL, NULL),
(938, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS645689', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:29:11', NULL, NULL, NULL),
(939, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS189175', 'Kazeem', '07012345678', 'kazeem.noibi@techadvance.ng', 'Male', 'Dare', '09012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 07:31:13', NULL, NULL, NULL),
(940, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS368972', 'Olatunji', '08020901278', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012328194', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 09:44:49', NULL, NULL, NULL),
(941, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS371767', 'Olatunji', '08020901278', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012328194', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 09:53:35', NULL, NULL, NULL),
(942, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS945328', 'Olatunji', '08012345678', 'drdre4life2001@yahoo.com', 'Male', 'wale', '07012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 09:55:54', NULL, NULL, NULL),
(943, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS764767', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'wale', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 09:57:14', NULL, NULL, NULL),
(944, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY380977', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:21:02', NULL, NULL, NULL),
(945, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY759350', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:21:28', NULL, NULL, NULL),
(946, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY723168', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:22:00', NULL, NULL, NULL),
(947, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY458175', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:36:46', NULL, NULL, NULL),
(948, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY849555', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:37:36', NULL, NULL, NULL),
(949, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY584123', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:40:29', NULL, NULL, NULL),
(950, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY323745', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:41:09', NULL, NULL, NULL),
(951, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY121162', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 10:47:36', NULL, NULL, NULL),
(952, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY857203', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 12:22:05', NULL, NULL, NULL),
(953, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY270703', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 12:58:22', NULL, NULL, NULL),
(954, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY306827', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 12:59:39', NULL, NULL, NULL),
(955, 1405, '2018-02-03', 1, 11500, NULL, 11850, NULL, NULL, 0, 'PENDING', NULL, 'OKY783490', 'Olatunji', '07017400751', 'drdre4life2001@yahoo.com', 'Female', 'Ola oluwa', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 13:03:24', NULL, NULL, NULL),
(956, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS793987', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 13:12:53', NULL, NULL, NULL),
(957, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS624855', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:10:06', NULL, NULL, NULL),
(958, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS184935', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:33:02', NULL, NULL, NULL),
(959, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS837745', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:33:29', NULL, NULL, NULL),
(960, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS472714', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:34:55', NULL, NULL, NULL),
(961, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS581273', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:35:11', NULL, NULL, NULL),
(962, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS588399', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:38:34', NULL, NULL, NULL),
(963, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS719461', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:45:23', NULL, NULL, NULL),
(964, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS568105', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:45:56', NULL, NULL, NULL),
(965, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS605140', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:46:43', NULL, NULL, NULL),
(966, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS973854', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:47:27', NULL, NULL, NULL),
(967, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS501392', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:48:33', NULL, NULL, NULL),
(968, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS616800', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:48:55', NULL, NULL, NULL),
(969, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS619544', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:49:13', NULL, NULL, NULL),
(970, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS590567', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:50:24', NULL, NULL, NULL),
(971, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS885773', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:51:32', NULL, NULL, NULL),
(972, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS891311', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:51:44', NULL, NULL, NULL),
(973, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS535896', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:52:19', NULL, NULL, NULL),
(974, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS650541', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:52:43', NULL, NULL, NULL),
(975, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS272289', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:54:44', NULL, NULL, NULL),
(976, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS295246', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', 'nurudeen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:55:03', NULL, NULL, NULL),
(977, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS373709', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', '07012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:56:17', NULL, NULL, NULL),
(978, 400, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS630465', 'Ajayi', '07017400751', 'drdre4life2001@yahoo.com', 'Male', 'shole', '07012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 14:56:54', NULL, NULL, NULL),
(979, 22, '2018-02-03', 1, 100, NULL, 450, NULL, NULL, 0, 'PENDING', NULL, 'CHS144802', 'Segun', '0802090127', 'drdre4life2001@yahoo.com', 'Female', 'Ajayi Nurudeen', '08012345678', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-03', NULL, '2018-02-02 16:13:53', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_notes`
--

CREATE TABLE `booking_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `booking_refunds`
--

CREATE TABLE `booking_refunds` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `booking_amount` double NOT NULL,
  `refunded_amount` double NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `approved_user_id` int(10) UNSIGNED NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bus_types`
--

CREATE TABLE `bus_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `no_of_seats` int(11) NOT NULL DEFAULT '15',
  `no_of_columns` int(11) NOT NULL DEFAULT '4',
  `no_of_rows` int(11) NOT NULL DEFAULT '4',
  `seat_width` int(11) NOT NULL DEFAULT '125',
  `seat_height` int(11) NOT NULL DEFAULT '45',
  `holder_width` int(11) NOT NULL DEFAULT '573',
  `holder_height` int(11) NOT NULL DEFAULT '317',
  `no_seats` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1, 0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bus_types`
--

INSERT INTO `bus_types` (`id`, `name`, `show`, `no_of_seats`, `no_of_columns`, `no_of_rows`, `seat_width`, `seat_height`, `holder_width`, `holder_height`, `no_seats`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Hiace', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, '2016-03-02 17:10:38', '2016-03-02 17:10:38'),
(2, 'Hiace', 1, 14, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, '2016-03-02 17:10:38', '2016-03-02 17:10:38'),
(3, 'MCV', 1, 45, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, '2016-03-02 17:10:38', '2016-03-02 17:10:38'),
(4, 'Luxury', 1, 59, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, '2016-03-02 17:10:38', '2016-03-02 17:10:38'),
(5, 'Sienna', 1, 5, 2, 3, 125, 45, 573, 317, '-1, 0', NULL, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `call_logs`
--

CREATE TABLE `call_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cash_offices`
--

CREATE TABLE `cash_offices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cash_offices`
--

INSERT INTO `cash_offices` (`id`, `name`, `contact_name`, `address`, `phone`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Mile Cash Office', 'Dele Alli', 'Plot2, Onifowose av. MIle 2, Lagos.', '0800000000', 0, '2016-05-02 10:11:46', '2016-05-02 10:11:46'),
(2, 'Anthony Village Cash Office', 'Ashley Nwosu', '6 Bakare Road, Anthony Village, Ikeja', '080000000', 0, '2016-05-02 10:12:39', '2016-05-02 10:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `chartered_bookings`
--

CREATE TABLE `chartered_bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `trip_id` int(10) UNSIGNED DEFAULT NULL,
  `travel_date` datetime NOT NULL,
  `pickup_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `park_id` int(10) UNSIGNED DEFAULT NULL,
  `agreed_price` double NOT NULL,
  `bus_type_id` int(10) UNSIGNED DEFAULT NULL,
  `bus_features` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_passengers` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `paid_date` datetime NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `pick_up_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `budget` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `drop_off_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chartered_bookings`
--

INSERT INTO `chartered_bookings` (`id`, `operator_id`, `trip_id`, `travel_date`, `pickup_location`, `destination`, `park_id`, `agreed_price`, `bus_type_id`, `bus_features`, `contact_name`, `contact_mobile`, `contact_email`, `next_of_kin`, `next_of_kin_mobile`, `no_of_passengers`, `status`, `paid_date`, `comments`, `payment_method_id`, `pick_up_address`, `budget`, `drop_off_address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 42, NULL, '2016-11-04 00:00:00', 'Surulere Lagos', 'Enugu', NULL, 0, NULL, '', 'Mrs Ada', '08027610615', '', 'Gloria', '08169267204', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'surulere', '100', '234 Enugu road', NULL, '2016-11-01 13:12:03', '2016-11-01 13:12:03'),
(3, 41, NULL, '2016-11-25 00:00:00', 'igando', 'aba', NULL, 0, NULL, '', 'israel', '07062982266', '', 'irubor', '08062427841', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'igando', '60', 'aba', NULL, '2016-11-08 08:30:20', '2016-11-08 08:30:20'),
(4, 71, NULL, '2016-11-27 00:00:00', 'RCCG jesus vineyard model parish ugbuwangue,  warri,  delta state', 'williams square, off deco road warri delta state', NULL, 0, NULL, '', 'kpenosen meyiwa ', '+2348142206725', '', 'kpenosen Tsolaye ', '08142206725', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'RCCG JESUS VINEYARD MODEL PARISH UGBUWANGUE WARRI,  DELTA STATE', '7000 naira', 'WILLIAMS SQUARE OFF DECO ROAD WARRI DELTA STATE', NULL, '2016-11-11 11:58:05', '2016-11-11 11:58:05'),
(6, 41, NULL, '2016-11-15 00:00:00', 'Lagos, LA, Nigeria', 'Owerri, NG, Nigeria', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-11-15 10:39:42', '2016-11-15 10:39:42'),
(11, 23, NULL, '2016-12-28 00:00:00', 'lagos', 'Orlu, Imo State', NULL, 0, NULL, '', 'Franklin Njiaju', '08033372546', '', 'Adanna Njiaju', '07089182022', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '6 Bola Ajibola St. Off Allen Avenue, Ikeja', '', '', NULL, '2016-11-22 10:06:44', '2016-11-22 10:06:44'),
(13, 42, NULL, '2016-12-03 00:00:00', 'lekki, Lagos', 'fiditi, oyo state', NULL, 0, NULL, '', 'chukwuka', '08035051505', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '8b wole olateju crescent, lekki phase 1', '', 'fiditi grammar school, fiditi, oyo state.', NULL, '2016-11-25 14:13:35', '2016-11-25 14:13:35'),
(14, 15, NULL, '2016-11-25 00:00:00', 'lagid', 'abuja', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-11-25 18:23:52', '2016-11-25 18:23:52'),
(15, 42, NULL, '2016-11-28 00:00:00', '', '', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-11-28 12:46:48', '2016-11-28 12:46:48'),
(17, 15, NULL, '2016-12-25 00:00:00', 'lagos', 'warri', NULL, 0, NULL, '', 'Matthew', '08052008857', '', 'Nkiruka', '07039465758', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'airport Lagos', '30,000', 'warri', NULL, '2016-12-01 14:58:38', '2016-12-01 14:58:38'),
(23, 15, NULL, '2016-12-06 00:00:00', 'Lagos, LA, Nigeria', 'Port Harcourt, RI, Nigeria', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-12-04 22:31:06', '2016-12-04 22:31:06'),
(24, 15, NULL, '2016-12-17 00:00:00', 'lagos', 'Abuja, FC, Nigeria', NULL, 0, NULL, '', 'Matthew Bolofinde', '08022220902', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '50000', '', NULL, '2016-12-05 08:48:38', '2016-12-05 08:48:38'),
(25, 19, NULL, '2016-12-10 00:00:00', 'Lagos, LA, Nigeria', 'Enugu', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-12-08 07:45:02', '2016-12-08 07:45:02'),
(27, 42, NULL, '2016-12-10 00:00:00', 'Lagos, LA, Nigeria', 'Abuja, FC, Nigeria', NULL, 0, NULL, '', 'dayo', '08023038699', '', 'Dipo', '08023038699', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '20 Allen Avenue Ikeja', '3000', 'Ahmad\'s Bello way Garki Abuja', NULL, '2016-12-10 11:52:18', '2016-12-10 11:52:18'),
(31, 23, NULL, '2016-12-17 00:00:00', 'Ikeja, Lagos', 'Portharcourt', NULL, 0, NULL, '', 'Kazeem', '8080764159', '', 'me', '8080764159', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way', '25000', '77, Lagos Abeokuta Express Way', NULL, '2016-12-15 12:03:40', '2016-12-15 12:03:40'),
(34, 42, NULL, '2016-12-19 00:00:00', 'Lagos, LA, Nigeria', 'Lagos, LA, Nigeria', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-12-19 16:35:31', '2016-12-19 16:35:31'),
(35, 42, NULL, '2016-12-22 00:00:00', 'Gwarimpa Abuja', 'Owerri, NG, Nigeria', NULL, 0, NULL, '', 'Mrs Ada', '08033020339', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Gwarimpa', '', 'Concorde', NULL, '2016-12-20 16:06:18', '2016-12-20 16:06:18'),
(36, 42, NULL, '2016-12-25 00:00:00', 'ibadan', 'okitipupa', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-12-20 19:43:20', '2016-12-20 19:43:20'),
(37, 42, NULL, '2016-12-23 00:00:00', 'lagos', 'calabar', NULL, 0, NULL, '', 'Harrygold', '8092325348', '', 'uwem james', '8092325348', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '19 Adeosun street, Surulere Lagos, Nigeria', '6500', 'stadium, calabar', NULL, '2016-12-21 14:20:08', '2016-12-21 14:20:08'),
(38, 42, NULL, '2016-12-23 00:00:00', 'lagos state', 'niger state', NULL, 0, NULL, '', 'joseph', '08136939812', '', 'victore', '08062745929', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'lekki phase one', '1', 'minna', NULL, '2016-12-21 20:29:47', '2016-12-21 20:29:47'),
(40, 42, NULL, '2016-12-26 00:00:00', 'Lagos, LA, Nigeria', 'Ibadan, OY, Nigeria', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2016-12-26 09:41:01', '2016-12-26 09:41:01'),
(41, 42, NULL, '1970-01-01 00:00:00', 'HpDxLOUA', 'StKeuQFhdoY', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'NcwJtMxENe', '99193463434', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'wohqYyOJaaKmkm', '5', 'TNDrXZDfJBilMeiGtL', NULL, '2016-12-28 17:04:44', '2016-12-28 17:04:44'),
(42, 42, NULL, '2016-12-28 00:00:00', 'ododuwa university osun', 'kaduna', NULL, 0, NULL, '', 'ishola usman olaide', '09050198901', '', 'ola', '07085617458', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ododuwa university osun', '3500', 'kaduna', NULL, '2016-12-28 17:35:39', '2016-12-28 17:35:39'),
(44, 42, NULL, '2016-12-30 00:00:00', 'osun', 'kano', NULL, 0, NULL, '', 'ishola usman olaide ', '09050198901', '', 'Mrs ishola', '07085617458', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ododuwa university frontage ', '#3000', 'kano garage', NULL, '2016-12-30 09:03:49', '2016-12-30 09:03:49'),
(45, 42, NULL, '1970-01-01 00:00:00', 'LjIjQGtkEiDASFjH', 'hLVLZHhW', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'uftmKRZBye', '65186485573', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'faSWUPAYXuERoccto', '4', 'YWIeadcbkDZE', NULL, '2016-12-30 18:26:03', '2016-12-30 18:26:03'),
(47, 42, NULL, '1970-01-01 00:00:00', 'kqxuJTyvRNR', 'ltmobDBimP', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'OWdPSBRiBWKZQbfK', '81162347429', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'KwqbUSgWMB', '42', 'sudnWORHkKKlJwOqC', NULL, '2017-01-01 12:21:47', '2017-01-01 12:21:47'),
(49, 42, NULL, '2017-01-03 00:00:00', 'unnbi', 'owirre', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2017-01-01 20:10:36', '2017-01-01 20:10:36'),
(50, 42, NULL, '2017-01-03 00:00:00', 'anambra unbio', 'owerri mbaise', NULL, 0, NULL, '', 'ik', '07033667728', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, ' sam & sam bus stop unbio', '', 'pls call let me expl', NULL, '2017-01-01 20:43:18', '2017-01-01 20:43:18'),
(51, 42, NULL, '2017-01-04 00:00:00', 'benin', 'lagos', NULL, 0, NULL, '', 'bob', '08060700555', '', 'key ahit', '09079066864', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'pj', '500\\l', 'abuha', NULL, '2017-01-01 22:24:26', '2017-01-01 22:24:26'),
(54, 42, NULL, '1970-01-01 00:00:00', 'VpyNMbVpPAggJanaqsJ', 'eOaUDLHaCsQXuvCv', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'dwxjyYOIZzjD', '69003127951', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'AXdLlzTKrouHP', '2', 'BmzerCuNjSzgO', NULL, '2017-01-03 23:17:35', '2017-01-03 23:17:35'),
(55, 42, NULL, '1970-01-01 00:00:00', 'uLaDXaNFwRLWHUH', 'DPeYRzkSYMfMhkojLM', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'dLGJqGjGQMduZYWkQby', '11065598677', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'yiyzOypFubFVgXzdp', '14', 'dsetYfnnFUjEi', NULL, '2017-01-04 01:01:31', '2017-01-04 01:01:31'),
(58, 19, NULL, '1970-01-01 00:00:00', 'YyjoqmKaGcG', 'fVwHvEMzyJknlIu', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'NSrmSQaByqsuvQfmYYC', '75710174024', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'rbNvhrTJEDA', '41', 'yPXHeJCRLFmoXgdxE', NULL, '2017-01-05 02:20:17', '2017-01-05 02:20:17'),
(59, 7, NULL, '1970-01-01 00:00:00', 'TXmOkHLQvFwOAg', 'ejnauuxbUHIG', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'XVeGJHKrevmKTFOqBtW', '97601690014', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'yyulAIssgBIXGLNSm', '7', 'mSKSvZWDbnNEFFrUjkj', NULL, '2017-01-05 21:26:35', '2017-01-05 21:26:35'),
(60, 5, NULL, '1970-01-01 00:00:00', 'qBLrLQtUhBjPOQjXCO', 'wRPZQQgpP', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'chdCJQmSsTG', '80420080471', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'MklDNmVQDibTSD', '89', 'ZViqZyNOXXLEA', NULL, '2017-01-06 21:23:30', '2017-01-06 21:23:30'),
(61, 10, NULL, '1970-01-01 00:00:00', 'gwRkLdsnLa', 'PzxWtHWKUmpcdEBGGWF', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'prqHqLokywUBBhW', '50174168931', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'bSYTfllhQLsUyFjsxqj', '2', 'eWJXByuLatHG', NULL, '2017-01-07 17:40:26', '2017-01-07 17:40:26'),
(62, 4, NULL, '1970-01-01 00:00:00', 'kPdnLOwhCwyq', 'ywtTFdrmKa', NULL, 0, NULL, '', 'jfvynms4281rt@hotmail.com', 'jfvynms4281rt@hotmail.com', '', 'TSrAiUgysROfzda', '51437380128', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'DQYpZLEdTNDpTIvbp', '30', 'WISBaZzzQDUsuuF', NULL, '2017-01-08 04:31:30', '2017-01-08 04:31:30'),
(63, 1, NULL, '2017-01-17 00:00:00', 'Port-harcourt', 'Onitsha ', NULL, 0, NULL, '', 'Ifeoma', '07033579251 ', '', 'Sylvia ', '08033899963', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Plot71 Haruk Road rumuigbo by obiwali Port-harcourt ', '30,000', 'No23 Kengozz housing estate 33 onitsha', NULL, '2017-01-13 13:05:22', '2017-01-13 13:05:22'),
(64, 1, NULL, '2017-01-13 00:00:00', 'River state', 'Anambra ', NULL, 0, NULL, '', 'Ifeoma', '07033579251 ', '', 'Sylvia ', '08033899963', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Plot71 Haruk Road rumuigbo by obiwali Port-harcourt ', '30,000', 'No23 Kengozz housing estate 33 onitsha', NULL, '2017-01-13 13:07:40', '2017-01-13 13:07:40'),
(65, 71, NULL, '1970-01-01 00:00:00', 'AFAHA NSIT', 'EBONY PARK', NULL, 0, NULL, '', 'BASSEY IME JAMES', '09060250191', '', 'LORIDA BASSEY IME', '07065947522', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '2james akpan afaha nsit', 'MUSIC SHOW AT EBONY', 'ebony park', NULL, '2017-02-10 10:07:27', '2017-02-10 10:07:27'),
(66, 71, NULL, '1970-01-01 00:00:00', 'AFAHA NSIT', 'EBONY PARK', NULL, 0, NULL, '', 'BASSEY IME JAMES', '09060250191', '', 'LORIDA BASSEY IME', '07065947522', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '2james akpan afaha nsit', 'MUSIC SHOW AT EBONY', 'ebony park', NULL, '2017-02-10 10:13:37', '2017-02-10 10:13:37'),
(68, 71, NULL, '1970-01-01 00:00:00', 'uyo', 'ebony', NULL, 0, NULL, '', 'BASSEY IME JAMES', '09060250191', '', 'QUEEN', ' 07065947522', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'UYO ITAM', 'MUSIC SHOW', 'EBONY PACK', NULL, '2017-02-12 22:08:10', '2017-02-12 22:08:10'),
(69, 15, NULL, '1970-01-01 00:00:00', 'sDdzbjSdpMmwDTvVyz', 'kUSiMTGm', NULL, 0, NULL, '', 'jimos4581rt@hotmail.com', 'jimos4581rt@hotmail.com', '', 'TKOARTUZtorGGtJN', '45383814236', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'DPYhvjwZrselVWgmn', '55', 'AcJuCJBemIME', NULL, '2017-02-19 12:52:53', '2017-02-19 12:52:53'),
(70, 15, NULL, '2017-02-27 00:00:00', 'Abuja, FC, Nigeria', 'bauchi', NULL, 0, NULL, '', 'toke', '08126406701', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'nls bwari', 'nil', 'bauchi', NULL, '2017-02-27 09:39:51', '2017-02-27 09:39:51'),
(71, 2, NULL, '2017-06-14 00:00:00', 'Lagos, Nigeri', 'Osogbo', NULL, 0, NULL, '', 'Kazeem', '8080764159', '', 'Mummy', '7039828381', 23, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way', '50000', '77, Lagos Abeokuta Express Way', NULL, '2017-06-12 13:57:46', NULL),
(72, 15, NULL, '2017-06-13 00:00:00', 'Ikeja, Lagos', 'Lekki, Lagos', NULL, 0, NULL, '', 'Jide Okenwa', '08087855000', '', 'Kay', '08023724087', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'shoprite ikeja', '40,000', 'elegushi beach', NULL, '2017-06-12 14:37:42', NULL),
(73, 15, NULL, '2017-06-14 00:00:00', 'Ikeja, Lagos', 'Lekki, Lagos', NULL, 0, NULL, '', 'Jide Okenwa', '08084585839', '', 'Kay', '08023724087', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Shoprite,Ikeja', '40,000', 'elegushi beach', NULL, '2017-06-13 10:30:20', NULL),
(74, 5, NULL, '2017-06-14 00:00:00', 'Ikeja, Lagos', 'Lekki,  Lagos', NULL, 0, NULL, '', 'Jide Okenwa', '08084585839', '', 'Kay', '08023724087', 24, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Shoprite,Ikeja', '50,000', 'elegushi beach', NULL, '2017-06-13 14:36:27', NULL),
(75, 1, NULL, '2017-06-17 00:00:00', 'Lagos', 'Abeokuta', NULL, 0, NULL, '', 'Kazeem', '8080764159', '', 'KAZEEM', 'KAZEEM', 25, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way', '25000', '77, Lagos Abeokuta Express Way', NULL, '2017-06-14 10:53:12', NULL),
(76, 1, NULL, '2017-06-22 00:00:00', 'Lagos', 'Abeokut', NULL, 0, NULL, '', 'Noibi', '8080764159', '', 'KAZEEM', '8080764159', 54, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way, Dalemo', '500000', '77, Lagos Abeokuta Express Way', NULL, '2017-06-14 10:56:05', NULL),
(77, 9, NULL, '2017-06-24 00:00:00', 'Lagos', 'Abuja, FC, Nigeria', NULL, 0, NULL, '', 'Samuel Dalyop', '08036348466', '', 'Mila Dalyop', '08037034752', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Lagos', '7000', 'Abuja', NULL, '2017-06-23 23:39:12', NULL),
(78, 15, NULL, '2017-07-02 00:00:00', 'Kano', 'Calaber', NULL, 0, NULL, '', 'Bashar', '08064815728', '', '', '08036575757', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Kano', '1', 'Calaber', NULL, '2017-07-02 12:00:05', NULL),
(79, 15, NULL, '2017-07-02 00:00:00', 'Kano', 'Calabar', NULL, 0, NULL, '', 'Bashar', '08064815728', '', '08036575757', '08036575757', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Kano', '1', 'Calaber', NULL, '2017-07-02 12:03:26', NULL),
(80, 42, NULL, '2016-11-04 00:00:00', 'Lagos', 'Osogbo', NULL, 0, NULL, '', 'Saheed', '07039828381', '', 'Kazeem', '080761324242', 52, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ojo, Lagos', '549000', '2016-11-01', NULL, '2017-07-04 16:14:40', NULL),
(81, 42, NULL, '2016-11-04 00:00:00', 'Lagos', 'Osogbo', NULL, 0, NULL, '', 'Saheed', '07039828381', '', 'Kazeem', '080761324242', 52, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ojo, Lagos', '549000', '2016-11-01', NULL, '2017-07-04 17:32:35', NULL),
(82, 42, NULL, '2016-11-04 00:00:00', 'Lagos', 'Osogbo', NULL, 0, NULL, '', 'Saheed', '07039828381', '', 'Kazeem', '080761324242', 52, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ojo, Lagos', '549000', '2016-11-01', NULL, '2017-07-05 13:48:55', NULL),
(83, 42, NULL, '2016-11-04 00:00:00', 'Lagos', 'Osogbo', NULL, 0, NULL, '', 'Saheed', '07039828381', '', 'Kazeem', '080761324242', 52, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ojo, Lagos', '549000', '2016-11-01', NULL, '2017-07-05 14:15:26', NULL),
(84, 42, NULL, '2016-11-04 00:00:00', 'Lagos', 'Osogbo', NULL, 0, NULL, '', 'Saheed', '07039828381', '', 'Kazeem', '080761324242', 52, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ojo, Lagos', '549000', '2016-11-01', NULL, '2017-07-05 18:59:44', NULL),
(85, 1, NULL, '2017-07-07 00:00:00', 'Ikorodu', 'Ogba', NULL, 0, NULL, '', 'Ganiu', '08038374868', '', 'Ernest', '08035639192', 16, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ikorodu garage', '15000', 'Acme Road, Ogba', NULL, '2017-07-06 08:55:01', NULL),
(86, 1, NULL, '2017-07-18 00:00:00', 'lagos', 'laos', NULL, 0, NULL, '', 'Temiye', '7038122917', '', '', '7038122917', 5, 'PENDING', '0000-00-00 00:00:00', '', NULL, '23 new road ajah', '6', '23 new road ajah', NULL, '2017-07-18 09:37:12', NULL),
(87, 1, NULL, '2017-09-08 00:00:00', 'lagos', 'Kano', NULL, 0, NULL, '', 'Hassan', '8127137052', '', '', '8127137052', 27, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Unilag', '135000', 'BUK New site', NULL, '2017-09-07 23:00:08', NULL),
(88, 42, NULL, '2017-10-04 00:00:00', 'Ibadan', 'Ikeja', NULL, 0, NULL, '', 'Ifeoluwa k', '08187393185', '', 'Tola', '08187393185', 2, 'PENDING', '0000-00-00 00:00:00', '', NULL, '14 Bodija close', '4000', 'Allen, Ikeja', NULL, '2017-10-03 13:32:12', NULL),
(89, 15, NULL, '2017-10-03 00:00:00', 'Lagos ', 'Kano ', NULL, 0, NULL, '', 'Stephanie Peter Omale', '08141665546', '', 'Richard Omale ', '', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Lagos ', '200,000', 'Kano', NULL, '2017-10-21 23:18:21', NULL),
(90, 15, NULL, '2017-10-24 00:00:00', 'Lagos ', 'Kano ', NULL, 0, NULL, '', 'Stephanie Peter Omale', '8141665546', '', 'Peter Omale ', '08137963967', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Lagos mainland ', '200,000', 'Kano ', NULL, '2017-10-21 23:20:28', NULL),
(91, 5, NULL, '2013-11-30 00:00:00', 'Ibadan', 'Yaba', NULL, 0, NULL, '', 'Akeem', '08033844790', '', 'ajayi', '07017400751', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '60, Unity Street, Off Governors Road', '48000', '60, Unity Street', NULL, '2017-10-23 08:39:23', NULL),
(92, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:40:07', NULL),
(93, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:40:46', NULL),
(94, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:42:58', NULL),
(95, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:44:39', NULL),
(96, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:46:29', NULL),
(97, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:46:41', NULL),
(98, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:46:59', NULL),
(99, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:47:04', NULL),
(100, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:47:27', NULL),
(101, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:47:53', NULL),
(102, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 08:50:37', NULL),
(103, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '8023998118', '', 'Olakunle', '8023998118', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '60 Unity Street, Off Governors Road, IKOTUN', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-23 08:51:36', NULL),
(104, 2, NULL, '2015-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'olawale', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '60 Unity Street, Off Governors Road, IKOTUN', '10000', '7 Foxwood Court', NULL, '2017-10-23 09:04:46', NULL),
(105, 1, NULL, '2017-10-17 00:00:00', 'Lagos', 'Abuja', NULL, 0, NULL, '', '08080764159', '08095480534', '', 'Me', '8080764159', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way, Dalemo', '35000', '77, Lagos Abeokuta Express Way', NULL, '2017-10-23 10:14:31', NULL),
(106, 1, NULL, '2017-10-17 00:00:00', 'Lagos', 'Abuja', NULL, 0, NULL, '', '08080764159', '08095480534', '', 'Me', '8080764159', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, '77, Lagos Abeokuta Express Way, Dalemo', '35000', '77, Lagos Abeokuta Express Way', NULL, '2017-10-23 10:14:37', NULL),
(107, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '8180738420', '', 'Olakunle', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:07:34', NULL),
(108, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '07017400751', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '60 Unity Street, Off Governors Road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:12:08', NULL),
(109, 2, NULL, '2016-11-29 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '8180738420', '', 'Olakunle', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '1000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:14:29', NULL),
(110, 2, NULL, '2016-11-29 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '8180738420', '', 'Olakunle', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '1000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:19:19', NULL),
(111, 1, NULL, '2017-12-31 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '8180738420', '', 'Olakunle', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:22:05', NULL),
(112, 3, NULL, '2013-12-30 00:00:00', 'yaba', 'lagos', NULL, 0, NULL, '', 'Ajayi', '8180738420', '', 'nurudeen', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '12, badagry road', NULL, '2017-10-23 11:37:28', NULL),
(113, 2, NULL, '2013-11-28 00:00:00', 'yaba', 'lagos', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '08180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '10000', '60 Unity Street, Off Governors Road', NULL, '2017-10-23 11:39:04', NULL),
(114, 69, NULL, '1970-01-01 00:00:00', 'QwlxcZwqFnJxBNHOYt', 'tErRDCOii', NULL, 0, NULL, '', 'jimosa4ccf2@hotmail.com', 'jimosa4ccf2@hotmail.com', '', 'BfpbnNNJSxqoH', '18932432725', 45, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'cyftvQGyBciCvoQb', '29', 'jAVapiHVaPjwmCu', NULL, '2017-10-23 14:04:33', NULL),
(115, 42, NULL, '2017-10-24 00:00:00', 'Yaba', 'Awgu', NULL, 0, NULL, '', 'Ayo', '07056589212', '', 'Kaboi', '09088899900', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, '6, surulere street', '150,000', 'Awgu town', NULL, '2017-10-23 17:23:30', NULL),
(116, 42, NULL, '2017-10-24 00:00:00', 'Yaba', 'Awgu', NULL, 0, NULL, '', 'Ayo', '07056589212', '', 'Kaboi', '09088899900', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, '6, surulere street', '150,000', 'Awgu town', NULL, '2017-10-23 17:23:59', NULL),
(117, 42, NULL, '2017-10-24 00:00:00', 'Surulere ', 'Awgu', NULL, 0, NULL, '', 'JAY', '08087415522', '', 'Ay', '08096655425', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Surulere ', '150,000', 'Awgu', NULL, '2017-10-23 18:35:39', NULL),
(118, 2, NULL, '2016-11-30 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Nurudeen', '08023998118', '', 'Olakunle', '8180738420', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '60 Unity Street, Off Governors Road', '10000', '12, badagry road', NULL, '2017-10-24 10:01:44', NULL),
(119, 2, NULL, '2017-10-20 00:00:00', 'Lagos', 'Yaba', NULL, 0, NULL, '', 'Ajayi', '07017400751', '', 'Olakunle', '8023998118', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, badagry road', '80000', '7 Foxwood Court', NULL, '2017-10-24 10:04:31', NULL),
(120, 2, NULL, '2017-11-10 00:00:00', 'Lagos', 'Lokoja', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '3015158749', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Foxwood Court', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-24 10:06:15', NULL),
(121, 2, NULL, '2017-11-10 00:00:00', 'Lagos', 'Lokoja', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '3015158749', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Foxwood Court', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-24 10:07:00', NULL),
(122, 2, NULL, '2017-11-10 00:00:00', 'Lagos', 'Lokoja', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '3015158749', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Foxwood Court', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-24 10:07:11', NULL),
(123, 2, NULL, '2017-11-10 00:00:00', 'Lagos', 'Lokoja', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '3015158749', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Foxwood Court', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-24 10:07:22', NULL),
(124, 2, NULL, '2017-11-10 00:00:00', 'Lagos', 'Lokoja', NULL, 0, NULL, '', 'Nurudeen', '8180738420', '', 'Olakunle', '3015158749', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Foxwood Court', '1000', '1B, FICUS BENJAMIN CLOSE, GADUWA ESTATE', NULL, '2017-10-24 10:07:32', NULL),
(125, 5, NULL, '2017-10-26 00:00:00', 'Okota,lagos', 'Nnewi, Anambra', NULL, 0, NULL, '', 'Ukamaka', '07057062911', '', 'Joy', '7057062911', 5, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'No 17 Ben onyeka str, greenfield estate, ago palace way', '40,000', 'Nnewi Anambra', NULL, '2017-10-24 22:31:28', NULL),
(126, 37, NULL, '2017-11-24 00:00:00', 'Orlu (Imo State)', 'Area 2 (Abuja)', NULL, 0, NULL, '', 'Onyii', '080869020260', '', 'Leo', '08068955511', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Orlu, Imo State', 'Business', 'Ataku, Abuja', NULL, '2017-10-26 09:59:07', NULL),
(127, 37, NULL, '2017-11-24 00:00:00', 'Orlu (Imo State)', 'Area 2 (Abuja)', NULL, 0, NULL, '', 'Onyii', '080869020260', '', 'Leo', '08068955511', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Orlu, Imo State', 'Business', 'Ataku, Abuja', NULL, '2017-10-26 09:59:16', NULL),
(128, 51, NULL, '2017-11-03 00:00:00', 'Iyana-Ipaja, Lagos', 'Nyanya, Abuja', NULL, 0, NULL, '', 'Yemi', '08061377706', '', 'Abbey', '07033045262', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Iyana-Ipaja, Lagos', '6000 per passenger', 'Nyanya, Abuja', NULL, '2017-10-26 11:25:51', NULL),
(129, 51, NULL, '2017-11-03 00:00:00', 'Iyana-Ipaja, Lagos', 'Nyanya, Abuja', NULL, 0, NULL, '', 'Yemi', '08061377706', '', 'Abbey', '07033045262', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Iyana-Ipaja, Lagos', '6000 per passenger', 'Nyanya, Abuja', NULL, '2017-10-26 11:27:44', NULL),
(130, 51, NULL, '2017-11-03 00:00:00', 'Iyana-Ipaja, Lagos', 'Nyanya, Abuja', NULL, 0, NULL, '', 'Yemi', '08061377706', '', 'Abbey', '07033045262', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Iyana-Ipaja, Lagos', '6000 per passenger', 'Nyanya, Abuja', NULL, '2017-10-26 11:27:46', NULL),
(131, 33, NULL, '2017-12-27 00:00:00', 'Lagos', 'Coutonou', NULL, 0, NULL, '', 'Dara', '09090408055', '', '', '', 2, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Yaba', '20000', 'Ouidah', NULL, '2017-10-30 01:29:42', NULL),
(132, 70, NULL, '2017-11-17 00:00:00', 'Alagbado Lagos', 'Ipao Ekiti Ikole Local government', NULL, 0, NULL, '', 'Taiwo Adewale', '08186161090', '', '', '', 18, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Alagbado', '30,000', 'Ipao Ekiti', NULL, '2017-11-03 10:08:57', NULL),
(133, 1, NULL, '2017-11-29 00:00:00', 'Lagos, Lagos', 'Akure, Ondo', NULL, 0, NULL, '', 'xyz', '07023232424', '', 'JDHSD', '07023232434', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ASsdsd', '1234', 'DSD', NULL, '2017-11-08 12:26:51', NULL),
(134, 1, NULL, '2017-11-29 00:00:00', 'Lagos, Lagos', 'Akure, Ondo', NULL, 0, NULL, '', 'xyz', '07023232424', '', 'JDHSD', '07023232434', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ASsdsd', '1234', 'DSD', NULL, '2017-11-08 12:27:12', NULL),
(135, 33, NULL, '2017-11-08 00:00:00', 'Lagos', 'Bayelsa', NULL, 0, NULL, '', 'David', '8106973976', '', '', '8106973976', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, '44, inec road,  kpansia, yenagoa,', '4000', '44, inec road,  kpansia, yenagoa,', NULL, '2017-11-08 19:19:58', NULL),
(136, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 09:20:15', NULL),
(137, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 11:38:09', NULL),
(138, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 12:06:00', NULL),
(139, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:38:46', NULL),
(140, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:39:40', NULL),
(141, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:41:46', NULL),
(142, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:42:06', NULL),
(143, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:43:40', NULL),
(144, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:43:55', NULL),
(145, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:49:38', NULL),
(146, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:50:07', NULL),
(147, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:50:41', NULL),
(148, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:51:27', NULL),
(149, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:52:04', NULL),
(150, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:52:24', NULL),
(151, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 13:52:42', NULL),
(152, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 14:02:46', NULL),
(153, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 14:04:13', NULL),
(154, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:29:13', NULL),
(155, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:37:34', NULL),
(156, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:38:08', NULL),
(157, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:38:56', NULL),
(158, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:39:42', NULL),
(159, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:41:52', NULL),
(160, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:43:15', NULL),
(161, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:43:43', NULL),
(162, 1, NULL, '2017-01-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 14:59:54', NULL),
(163, 3, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:04:34', NULL),
(164, 3, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:07:20', NULL),
(165, 2, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:08:53', NULL),
(166, 2, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:14:43', NULL),
(167, 2, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:15:29', NULL),
(168, 2, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:32:35', NULL),
(169, 2, NULL, '2017-02-11 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 15:40:58', NULL),
(170, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 16:09:45', NULL),
(171, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:07:55', NULL),
(172, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:18:55', NULL),
(173, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:19:15', NULL),
(174, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:21:19', NULL),
(175, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:22:10', NULL),
(176, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:38:09', NULL),
(177, 31, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:39:38', NULL),
(178, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:43:08', NULL),
(179, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:49:05', NULL),
(180, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:51:40', NULL),
(181, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:53:19', NULL),
(182, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:54:55', NULL),
(183, 30, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0893482849595', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 17:55:37', NULL),
(184, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:02:01', NULL),
(185, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:04:11', NULL),
(186, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:05:28', NULL),
(187, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:05:56', NULL),
(188, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:10:39', NULL),
(189, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:12:52', NULL),
(190, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:13:29', NULL),
(191, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:15:01', NULL),
(192, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:15:42', NULL),
(193, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:26:59', NULL),
(194, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:27:28', NULL),
(195, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:28:15', NULL),
(196, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:31:09', NULL),
(197, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:32:21', NULL),
(198, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:38:28', NULL),
(199, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 18:39:07', NULL),
(200, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:09:36', NULL),
(201, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:24:31', NULL),
(202, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:24:59', NULL),
(203, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:25:57', NULL),
(204, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:27:11', NULL),
(205, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:27:44', NULL),
(206, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:28:14', NULL),
(207, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:28:42', NULL),
(208, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:29:16', NULL),
(209, 3, NULL, '2017-02-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:29:56', NULL),
(210, 3, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-09 19:47:33', NULL),
(211, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:16:44', NULL),
(212, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:18:51', NULL),
(213, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:23:58', NULL);
INSERT INTO `chartered_bookings` (`id`, `operator_id`, `trip_id`, `travel_date`, `pickup_location`, `destination`, `park_id`, `agreed_price`, `bus_type_id`, `bus_features`, `contact_name`, `contact_mobile`, `contact_email`, `next_of_kin`, `next_of_kin_mobile`, `no_of_passengers`, `status`, `paid_date`, `comments`, `payment_method_id`, `pick_up_address`, `budget`, `drop_off_address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(214, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:28:08', NULL),
(215, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:29:14', NULL),
(216, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:30:40', NULL),
(217, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:31:45', NULL),
(218, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:32:56', NULL),
(219, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:33:24', NULL),
(220, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:34:49', NULL),
(221, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:36:16', NULL),
(222, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:38:23', NULL),
(223, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:39:01', NULL),
(224, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:42:02', NULL),
(225, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:45:11', NULL),
(226, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:46:15', NULL),
(227, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:47:12', NULL),
(228, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:48:06', NULL),
(229, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:55:20', NULL),
(230, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-09 20:56:31', NULL),
(231, 6, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-10 09:43:56', NULL),
(232, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 09:48:49', NULL),
(233, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 09:53:00', NULL),
(234, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:20:24', NULL),
(235, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:29:15', NULL),
(236, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:30:24', NULL),
(237, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:32:10', NULL),
(238, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:33:31', NULL),
(239, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:34:32', NULL),
(240, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:35:37', NULL),
(241, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:37:37', NULL),
(242, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:41:06', NULL),
(243, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 10:44:54', NULL),
(244, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 12:37:21', NULL),
(245, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 12:39:49', NULL),
(246, 1, NULL, '2017-11-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 12:51:30', NULL),
(247, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:10:58', NULL),
(248, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:13:49', NULL),
(249, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:15:00', NULL),
(250, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:15:43', NULL),
(251, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:16:50', NULL),
(252, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 13:17:52', NULL),
(253, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-10 14:47:03', NULL),
(254, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 16:04:08', NULL),
(255, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 17:17:19', NULL),
(256, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 22:28:50', NULL),
(257, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 22:29:35', NULL),
(258, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 22:31:48', NULL),
(259, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 22:32:31', NULL),
(260, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 22:48:03', NULL),
(261, 1, NULL, '2017-12-01 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 23:28:17', NULL),
(262, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-11 23:59:20', NULL),
(263, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:00:40', NULL),
(264, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:08:38', NULL),
(265, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:10:49', NULL),
(266, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:17:10', NULL),
(267, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:25:27', NULL),
(268, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:37:34', NULL),
(269, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:38:48', NULL),
(270, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:44:10', NULL),
(271, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:45:13', NULL),
(272, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:55:09', NULL),
(273, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:56:04', NULL),
(274, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:57:07', NULL),
(275, 1, NULL, '2017-10-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 00:57:58', NULL),
(276, 1, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-12 23:09:27', NULL),
(277, 1, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-12 23:17:03', NULL),
(278, 1, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '300000', '13, ora street', NULL, '2017-11-12 23:20:23', NULL),
(279, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:29:11', NULL),
(280, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:29:51', NULL),
(281, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:37:41', NULL),
(282, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:38:21', NULL),
(283, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:41:18', NULL),
(284, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:54:22', NULL),
(285, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-12 23:56:06', NULL),
(286, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 00:00:09', NULL),
(287, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 00:03:26', NULL),
(288, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 00:10:37', NULL),
(289, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 00:11:49', NULL),
(290, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 00:15:50', NULL),
(291, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 06:40:03', NULL),
(292, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 06:42:41', NULL),
(293, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:07:04', NULL),
(294, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:08:06', NULL),
(295, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:09:23', NULL),
(296, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:11:51', NULL),
(297, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:13:38', NULL),
(298, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:15:13', NULL),
(299, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:16:37', NULL),
(300, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:17:29', NULL),
(301, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:18:29', NULL),
(302, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:19:50', NULL),
(303, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:25:10', NULL),
(304, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:27:10', NULL),
(305, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:28:32', NULL),
(306, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:30:45', NULL),
(307, 2, NULL, '2017-12-10 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 07:32:21', NULL),
(308, 1, NULL, '2017-11-13 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 09:44:30', NULL),
(309, 1, NULL, '2017-11-13 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-13 10:53:07', NULL),
(310, 2, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-15 10:01:23', NULL),
(311, 1, NULL, '2017-11-18 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Labi', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-16 17:59:06', NULL),
(312, 37, NULL, '2017-11-24 00:00:00', 'Abuja', 'Akokwa,imo state', NULL, 0, NULL, '', 'Chukwudi', '07017003370', '', 'Nonye', '08039295284', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, '3rd avenue gwarinpa,abuja', '50000', 'Akwu,akokwa,ideato north,imo state', NULL, '2017-11-17 10:10:31', NULL),
(313, 31, NULL, '2017-11-19 00:00:00', 'iyana ipaja Lagos', 'Umunze Anambra', NULL, 0, NULL, '', 'Ifeanyi', '08146931259', '', 'Joe', '08035050980', 7, 'PENDING', '0000-00-00 00:00:00', '', NULL, '3 sopetie street off morenike carena Orile Agege Lagos', '30,000', 'Amuhia Eziagu', NULL, '2017-11-18 12:14:12', NULL),
(314, 31, NULL, '2017-11-19 00:00:00', 'iyana ipaja Lagos', 'Umunze Anambra', NULL, 0, NULL, '', 'Ifeanyi', '08146931259', '', 'Joe', '08035050980', 7, 'PENDING', '0000-00-00 00:00:00', '', NULL, '3 sopetie street off morenike carena Orile Agege Lagos', '30,000', 'Amuhia Eziagu', NULL, '2017-11-18 12:16:23', NULL),
(315, 21, NULL, '2017-11-21 00:00:00', 'Isolo, lagos', 'Edu, kwara', NULL, 0, NULL, '', 'Ayodeji', '09029176336', '', 'Femi famuwagun ', '08033935049', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Blk 18a LBIC bungalow jakande estate', '8000', 'NYSC PERM. ORIENT. CAMP, YIKPATA, EDU  LGA  KWARA STATE', NULL, '2017-11-18 15:04:05', NULL),
(316, 21, NULL, '2017-11-21 00:00:00', 'Isolo, lagos', 'Edu, kwara', NULL, 0, NULL, '', 'Ayodeji', '09029176336', '', 'Femi famuwagun ', '08033935049', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Blk 18a LBIC bungalow jakande estate', '8000', 'NYSC PERM. ORIENT. CAMP, YIKPATA, EDU  LGA  KWARA STATE', NULL, '2017-11-18 15:06:11', NULL),
(317, 21, NULL, '2017-11-21 00:00:00', 'Isolo, lagos', 'Edu, kwara', NULL, 0, NULL, '', 'Ayodeji', '09029176336', '', 'Femi famuwagun ', '08033935049', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Blk 18a LBIC bungalow jakande estate', '8000', 'NYSC PERM. ORIENT. CAMP, YIKPATA, EDU  LGA  KWARA STATE', NULL, '2017-11-18 15:07:24', NULL),
(318, 70, NULL, '2017-12-08 00:00:00', 'Lagos', 'Ibadan', NULL, 0, NULL, '', 'Leon', '07060522696', '', '', '', 20, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ikeja', '30000', 'Bodija', NULL, '2017-11-19 21:02:41', NULL),
(319, 70, NULL, '2017-12-08 00:00:00', 'Lagos', 'Ibadan', NULL, 0, NULL, '', 'Leon', '07060522696', '', '', '', 20, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Ikeja', '30000', 'Bodija', NULL, '2017-11-19 21:02:53', NULL),
(320, 1, NULL, '2017-12-12 00:00:00', 'Lagos', 'Aba', NULL, 0, NULL, '', 'Segun', '0802090127', '', 'Oju', '08012345678', 4, 'PENDING', '0000-00-00 00:00:00', '', NULL, '12, olu street', '30,0000', '13, ora street', NULL, '2017-11-20 15:14:06', NULL),
(321, 1, NULL, '2017-11-21 00:00:00', 'Kaduna, Kaduna north', 'Bayelsa', NULL, 0, NULL, '', 'Onuh', '08130058588', '', 'Ojonugwa', '08037023947', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Kaduna', '5000', 'Bayelsa', NULL, '2017-11-20 19:39:27', NULL),
(322, 33, NULL, '2017-11-23 00:00:00', 'Abuja', 'Lagos', NULL, 0, NULL, '', 'Ajibola', '8160027317', '', 'Oluga', '8160027317', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, '7 Darazo Street', '25000', '7 Darazo Street', NULL, '2017-11-21 18:17:04', NULL),
(323, 42, NULL, '2018-02-12 00:00:00', 'Computer Village', 'Akure', NULL, 0, NULL, '', 'John', '080878552245', '', 'Jay', '08025256241', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, '6, Otigba Street, Ikeja', '100,000', 'FUTA', NULL, '2017-11-22 11:49:13', NULL),
(324, 1, NULL, '2017-12-15 00:00:00', '  illishan-remo ,ogun state', 'eleme junction  rivers state', NULL, 0, NULL, '', 'onunekwu miriam', '09091493636', '', 'sir simon onunekwu', '08038507870', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'babcock university', '50000', 'eleme junction portharcourt', NULL, '2017-11-24 13:13:38', NULL),
(325, 1, NULL, '2017-12-15 00:00:00', '  illishan-remo ,ogun state', 'eleme junction  rivers state', NULL, 0, NULL, '', 'onunekwu miriam', '09091493636', '', 'sir simon onunekwu', '08038507870', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'babcock university', '50000', 'eleme junction portharcourt', NULL, '2017-11-24 13:29:57', NULL),
(326, 30, NULL, '2017-11-27 00:00:00', 'Ikeja, Lagos', 'Port Harcourt, rivers state', NULL, 0, NULL, '', 'Neo Daniels', '08140012437', '', 'Mrs Ruth', '', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Murtala Muhamed International Airport', '100,000', 'Rumuwhara Farm Road, Eneka/Igwuruta Rd Port Harcourt ', NULL, '2017-11-26 08:52:11', NULL),
(327, 30, NULL, '2017-11-27 00:00:00', 'Ikeja, Lagos', 'Port Harcourt, rivers state', NULL, 0, NULL, '', 'Neo Daniels', '08140012437', '', 'Mrs Ruth', '', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Murtala Muhamed International Airport', '100,000', 'Rumuwhara Farm Road, Eneka/Igwuruta Rd Port Harcourt ', NULL, '2017-11-26 08:52:44', NULL),
(328, 41, NULL, '2017-11-27 00:00:00', 'Lagos airport, lagos state ', 'Port harcourt, Rivers State ', NULL, 0, NULL, '', 'Okafor Daniel ', '08140012437 ', '', 'Victor ', '07034964705 ', 5, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Lagos airport ', '60,000', 'Eneka, port harcourt, Rivers State ', NULL, '2017-11-26 09:22:44', NULL),
(329, 33, NULL, '2018-01-11 00:00:00', 'Lagos', 'Abuja', NULL, 0, NULL, '', 'Kelvin aboi', '08056412378', '', 'Aboi Michael', '08031094578', 12, 'PENDING', '0000-00-00 00:00:00', '', NULL, '14kolawole Street, lagos', '60000', 'Abuja', NULL, '2017-11-27 03:59:03', NULL),
(330, 8, NULL, '2018-01-31 00:00:00', 'New York', 'New York', NULL, 0, NULL, '', 'test', '564646', '', '', '', 5, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'test', '422', 'test', NULL, '2017-11-27 07:04:45', NULL),
(331, 15, NULL, '2017-12-16 00:00:00', 'ilorin landmark university', 'abuja', NULL, 0, NULL, '', 'emmanuel', '08164831299', '', 'andii', '08164831299', 14, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'landmark university omu aran kwara', '65000', 'utako abuja', NULL, '2017-11-27 21:45:37', NULL),
(332, 1, NULL, '2017-12-06 00:00:00', 'Accra ghana ', 'Lagos Nigeria ', NULL, 0, NULL, '', 'Ojekunle  Oladapo ', '+233240036566', '', 'Mr ojekunle ', '+233240036566', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Accra ghana ', '1', 'Lagos Nigeria ', NULL, '2017-12-06 09:52:05', NULL),
(333, 15, NULL, '2017-12-06 00:00:00', 'Accra ghana ', 'Lagos Nigeria ', NULL, 0, NULL, '', 'Ojekunle  Oladapo ', '+233240036566', '', 'Mr ojekunle ', '+233240036566', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Accra ghana ', '1', 'Lagos Nigeria ', NULL, '2017-12-06 09:53:58', NULL),
(334, 31, NULL, '2017-12-09 00:00:00', 'Kano, Kano', 'Yaba, Lagos', NULL, 0, NULL, '', 'King', '08097098798', '', 'hdkfkjd', '7879868789', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, '86 yub9b', '676767', '6 76bghgjh', NULL, '2017-12-09 04:59:41', NULL),
(335, 31, NULL, '2017-12-09 00:00:00', 'Kano, Kano', 'Yaba, Lagos', NULL, 0, NULL, '', 'King', '08097098798', '', 'hdkfkjd', '7879868789', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, '86 yub9b', '676767', '6 76bghgjh', NULL, '2017-12-09 04:59:42', NULL),
(336, 31, NULL, '2017-12-09 00:00:00', 'Kano, Kano', 'Yaba, Lagos', NULL, 0, NULL, '', 'King', '08097098798', '', 'hdkfkjd', '7879868789', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, '86 yub9b', '676767', '6 76bghgjh', NULL, '2017-12-09 05:01:00', NULL),
(337, 67, NULL, '2017-12-21 00:00:00', 'Kaduna ', 'port Harcourt ', NULL, 0, NULL, '', 'Nurudeen ', '08033114162', '', '', '', 9, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'kaduna', '70000', 'ogboeche road port harcourt', NULL, '2017-12-10 09:50:36', NULL),
(338, 31, NULL, '2017-12-09 00:00:00', 'Kano, Kano', 'Yaba, Lagos', NULL, 0, NULL, '', 'King', '08097098798', '', 'hdkfkjd', '7879868789', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, '86 yub9b', '676767', '6 76bghgjh', NULL, '2017-12-10 16:13:33', NULL),
(339, 1, NULL, '2018-01-02 00:00:00', 'Ikeja, Lagos', 'Ilesa, Osun', NULL, 0, NULL, '', 'Ademola Somotun', '08035392595', '', 'Saidat Somotun', '08030571601', 16, 'PENDING', '0000-00-00 00:00:00', '', NULL, '2, Odunukan Crescent, Olusosun, Off Kudirat Abiola Way, Ikeja, Lagos', '35000', 'Ilesa,Osun State', NULL, '2017-12-11 16:26:00', NULL),
(340, 31, NULL, '1970-01-01 00:00:00', 'lagos', 'onitsha', NULL, 0, NULL, '', 'som', '08034007144', '', 'chu', '08032486184', 1, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'alafia lagos', '', 'onitsha', NULL, '2017-12-14 04:28:21', NULL),
(341, 33, NULL, '2018-01-18 00:00:00', 'Ibadan', 'Benin', NULL, 0, NULL, '', 'Samuel', '+393392454124', '', 'Kayode', '08036422611', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'New gbagi ', '20', 'Beside uniben', NULL, '2017-12-14 14:48:21', NULL),
(342, 33, NULL, '2018-01-18 00:00:00', 'Ibadan', 'Benin', NULL, 0, NULL, '', 'Samuel', '+393392454124', '', 'Kayode', '08036422611', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'New gbagi ', '20', 'Beside uniben', NULL, '2017-12-14 14:48:31', NULL),
(343, 33, NULL, '2018-01-18 00:00:00', 'Ibadan', 'Benin', NULL, 0, NULL, '', 'Samuel', '+393392454124', '', 'Kayode', '08036422611', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'New gbagi ', '20', 'Beside uniben', NULL, '2017-12-14 14:48:35', NULL),
(344, 33, NULL, '2018-01-18 00:00:00', 'Ibadan', 'Benin', NULL, 0, NULL, '', 'Samuel', '+393392454124', '', 'Kayode', '08036422611', 15, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'New gbagi ', '20', 'Beside uniben', NULL, '2017-12-14 14:48:52', NULL),
(346, 33, NULL, '2017-12-21 00:00:00', 'lagos', 'kano', NULL, 0, NULL, '', '', '', '', '', '', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, '', '', '', NULL, '2017-12-18 11:18:22', NULL),
(347, 10, NULL, '2017-12-23 00:00:00', 'Lagos', 'Onitsha', NULL, 0, NULL, '', 'Ngozi Medani', '08089523821', '', 'Gina Medani', '08130184430', 3, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Graceland Estate Ajah', '40000', 'Ogidi, Anambra State', NULL, '2017-12-18 13:12:21', NULL),
(348, 50, NULL, '2017-12-21 00:00:00', 'Ajah Lagos', 'Ikeja lagos', NULL, 0, NULL, '', 'Tosin', '08188101007', '', 'Ayo', '08188101007', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Langbasa', '20000', 'Ikeja', NULL, '2017-12-20 15:25:33', NULL),
(349, 19, NULL, '2017-12-26 00:00:00', 'Awka, Anambra', 'Enugu,', NULL, 0, NULL, '', 'Zubby', '08063735894', '', 'Emeka', '08063735894', 2, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Nnewi', '20000', 'Enugu', NULL, '2017-12-25 23:33:42', NULL),
(350, 50, NULL, '1970-01-01 00:00:00', 'ikeja,Lagos', 'atican,lagos', NULL, 0, NULL, '', 'dolapo', '07065375074', '', 'moyo', '08132567774', 18, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ikeja', '25000', 'ikeja', NULL, '2017-12-26 21:28:16', NULL),
(351, 70, NULL, '2017-12-29 00:00:00', 'owerri', 'arochukwu', NULL, 0, NULL, '', 'Tolu Talabi', '08099447939', '', 'Tolu Talabi', '08160119116', 10, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'sam mbakwe airport', '30000', 'Arochukwu, Abia', NULL, '2017-12-27 17:42:19', NULL),
(352, 31, NULL, '2018-01-13 00:00:00', 'Abuja', 'Abuja', NULL, 0, NULL, '', 'Adagun Hussainah', '07014807804', '', 'Azeezat', '08023244430', 40, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'University of lagos', '4500000', 'Abuja ', NULL, '2018-01-05 12:03:36', NULL),
(353, 45, NULL, '1970-01-01 00:00:00', 'GsyRsaVKWVcm', 'iBOPJzyizFkrOTv', NULL, 0, NULL, '', 'support@goldentabs.com', 'support@goldentabs.com', '', 'RVyXHbrEdG', '94782988753', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'mvlWRsJABeBmoH', '8', 'njwZPtMhJjcC', NULL, '2018-01-05 21:27:43', NULL),
(354, 48, NULL, '1970-01-01 00:00:00', 'VAVAmquvizI', 'rxBIaDAokOZzml', NULL, 0, NULL, '', 'support@goldentabs.com', 'support@goldentabs.com', '', 'CvUvHkOzczr', '63112087649', 8, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'LIznXNSUyJJDQ', '66', 'UCyZcIlnrfXRbwZv', NULL, '2018-01-07 23:39:55', NULL),
(355, 44, NULL, '1970-01-01 00:00:00', 'LPHewBNoWRu', 'rgCpiXiBmHmUD', NULL, 0, NULL, '', 'support@goldentabs.com', 'support@goldentabs.com', '', 'BFNLhgphciHt', '84268291105', 0, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'gpRwEPRqwQN', '70', 'ktssuZZKexFqnLYXGMl', NULL, '2018-01-08 00:32:48', NULL),
(356, 15, NULL, '2018-01-12 00:00:00', 'Abuja', 'Ekiti', NULL, 0, NULL, '', 'Dipo Adeyeye', '08086342565', '', 'omolola', '09038963538', 30, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Old Secretariat gate, Area 1, Garki, Abuja', 'N150,000', 'Government House, Ado-Ekiti', NULL, '2018-01-09 12:02:47', NULL),
(357, 15, NULL, '2018-01-12 00:00:00', 'Abuja', 'Ekiti', NULL, 0, NULL, '', 'Dipo Adeyeye', '08086342565', '', 'omolola', '09038963538', 30, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'Old Secretariat gate, Area 1, Garki, Abuja', 'N150,000', 'Government House, Ado-Ekiti', NULL, '2018-01-09 12:03:05', NULL),
(358, 20, NULL, '2018-01-17 00:00:00', 'Victoria island, lagos', 'Festac, lagos', NULL, 0, NULL, '', 'O', '08', '', 'A', '08', 13, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'VI', '40000', 'FST', NULL, '2018-01-11 15:46:40', NULL),
(359, 8, NULL, '2018-01-16 00:00:00', 'Lagos', 'ilorin,kwarastate', NULL, 0, NULL, '', 'gideon', '08144943752', '', 'mr dunmoye', '08144943752', 14, 'PENDING', '0000-00-00 00:00:00', '', NULL, 'ijaiye', '2000', 'ilorin', NULL, '2018-01-14 13:56:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chisco_trips`
--

CREATE TABLE `chisco_trips` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `operator_id` tinyint(4) DEFAULT NULL,
  `trip_code` varchar(255) DEFAULT NULL,
  `source_park_id` int(11) DEFAULT NULL,
  `dest_park_id` int(11) DEFAULT NULL,
  `departure_time` time DEFAULT NULL,
  `duartion` varchar(255) DEFAULT NULL,
  `fare` double NOT NULL,
  `new_fare` double DEFAULT NULL,
  `bus_type_id` tinyint(4) DEFAULT NULL,
  `no_of_seats` int(11) DEFAULT NULL,
  `trip_position` varchar(45) DEFAULT NULL,
  `parent_trip_id` varchar(45) DEFAULT NULL,
  `round_trip_fare` varchar(45) DEFAULT NULL,
  `ac` varchar(45) DEFAULT NULL,
  `security` varchar(45) DEFAULT NULL,
  `tv` tinyint(4) DEFAULT NULL,
  `insurance` tinyint(4) DEFAULT NULL,
  `passport` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `is_intl_trip` tinyint(4) DEFAULT NULL,
  `virgin_passport_fare` varchar(45) DEFAULT NULL,
  `no_passport_fare` varchar(45) DEFAULT NULL,
  `round_trip_virgin_passport_fare` varchar(45) DEFAULT NULL,
  `round_trip_no_passport_fare` varchar(45) DEFAULT NULL,
  `operator_trip_id` varchar(45) DEFAULT NULL,
  `virgin_passport` tinyint(4) DEFAULT NULL,
  `regular_passport` tinyint(4) DEFAULT NULL,
  `no_passport` tinyint(4) DEFAULT NULL,
  `international` tinyint(4) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fare_expiry_date` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` tinyint(4) DEFAULT NULL,
  `parent_trip_name` varchar(45) DEFAULT NULL,
  `parent_trip_code` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chisco_trips`
--

INSERT INTO `chisco_trips` (`id`, `name`, `operator_id`, `trip_code`, `source_park_id`, `dest_park_id`, `departure_time`, `duartion`, `fare`, `new_fare`, `bus_type_id`, `no_of_seats`, `trip_position`, `parent_trip_id`, `round_trip_fare`, `ac`, `security`, `tv`, `insurance`, `passport`, `active`, `is_intl_trip`, `virgin_passport_fare`, `no_passport_fare`, `round_trip_virgin_passport_fare`, `round_trip_no_passport_fare`, `operator_trip_id`, `virgin_passport`, `regular_passport`, `no_passport`, `international`, `deleted_at`, `fare_expiry_date`, `created_at`, `updated_at`, `parent_trip_name`, `parent_trip_code`) VALUES
(1, '         Chisco Executive Coach (59 Seats)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, '         Chisco Executive Coach (59 Seats)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, '         Chisco Executive Coach (59 Seats)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, 'LAGOS (Jibowu) => ABIA (Aba)', NULL, '0e812f30-e7c2-e711-80ec-001c4281ad62', NULL, NULL, NULL, NULL, 100, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, 'LAGOS (Jibowu) => ABIA (Aba)', 13, '0e812f30-e7c2-e711-80ec-001c4281ad62', 30, 1, NULL, NULL, 100, NULL, NULL, 59, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(22, 'LAGOS (Jibowu) => ABIA (Aba)', 13, '0e812f30-e7c2-e711-80ec-001c4281ad62', 1, 30, '06:00:00', NULL, 100, NULL, 1, 59, 'First Bus', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, 'LAGOS (Jibowu) => ABUJA(Utako)', 13, '2b6bb0d6-e5c2-e711-80ec-001c4281ad62', 1, 102, '06:00:00', NULL, 100, 0, 1, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(24, 'ABIA(Aba) => ABUJA(Utako)', 13, 'd74f1ccc-e5c2-e711-80ec-001c4281ad62', 30, 102, '06:00:00', NULL, 100, 0, 1, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(25, 'ABIA(Aba) => Lagos(Jibowu)', 13, 'c15096e6-e5c2-e711-80ec-001c4281ad62', 102, 1, '06:00:00', NULL, 100, 0, 1, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(26, 'ABUJA(Utako)=> ABIA(Aba)', 13, 'a2f7b804-e6c2-e711-80ec-001c4281ad62', 102, 30, '06:00:00', NULL, 100, 0, 4, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(27, 'ABUJA(Utako)=> LAGOS(Jibowu)', 13, 'fe44c5fb-e5c2-e711-80ec-001c4281ad62', 102, 1, '06:00:00', NULL, 100, 0, 1, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(28, 'LAGOS(Mazamaza)=> ABIA(Aba)', 13, '6f960b9b-1ac9-e711-80ed-001c4281ad62', 2, 30, '06:00:00', NULL, 100, 0, 1, 59, 'First Bus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Adaeze okpube', 'ada@bus.com.ng', '0802761-0615', '$2y$10$RSlHM871CZFK5fm/b7j4jug2TvP5ccvKB7k7IdMl.NaY1Mud9gbWC', NULL, NULL, '2016-11-01 12:53:35', '2016-11-01 12:53:35'),
(2, 'Rex Eyoita', 'rex41e@gmail.com', '08095646282', '$2y$10$teU84bPqs25LslSCIGijZuEtXlQhha0zV2qHJHQk.JDOeDBKCTbga', NULL, NULL, '2016-11-01 15:54:43', '2016-11-01 15:54:43'),
(3, 'Analo Veronica', 'veraanalo@yahoo.com', '07063170384', '$2y$10$FTmTTUpSoFBFFRZWH2SCx.y.AkDUTUlxMh/bqTP4yYM57zSHt1zRO', NULL, NULL, '2016-11-11 06:39:21', '2016-11-11 06:39:21'),
(4, 'test', 'test@test.com', '0987654321', '$2y$10$5blWDpCzMno4a8LEcyI7POAKZ31VS6qosyFiOJo5xV6qxlQ6PSmKi', NULL, NULL, '2016-11-24 18:06:59', '2016-11-24 18:06:59'),
(5, 'James Omotoso ', 'jamesomotoso@gmail.com', '08066985858', '$2y$10$SPE446xEUvRb.voESP8we.HiDkgpHv0409G3Gf7LHffhotsO7RIkO', NULL, NULL, '2016-12-03 10:37:26', '2016-12-03 10:37:26'),
(6, 'Dotun Owolabi', 'thomasdotun@gmail.com', '08033645542', '$2y$10$LxMc2M486Zoy9VJaNnX28.F59Mm/wPB6brE6v1yrw43GwdRmjZvhS', NULL, NULL, '2016-12-05 12:15:12', '2016-12-05 12:15:12'),
(7, 'Owoseeni Roseline', 'owoseeniroseline@yahoo.com', '08102919848', '$2y$10$x3ub6uZc5xDJwZEy4VwAP.Xhjck8d.o4UEniQOMijsfZ9sSksK5hC', NULL, NULL, '2016-12-05 18:22:35', '2016-12-05 18:22:35'),
(8, 'Chris Ugim', 'Ugimchris@gmail.com', '08027514164', '$2y$10$Rz511HGMT9LHpQS9SLcpU.wIZFUiXlywUiz1p.OWgAx/y7lXaiFM6', NULL, NULL, '2016-12-06 06:40:24', '2016-12-06 06:40:24'),
(9, 'Effi Gloria', 'gloriaeffi@gmail.com', '08034567305', '$2y$10$vw4jkDIg6S4xuUaxn8j4lO8DYDcectHPQU33m5r3FcFI.H3HzcxGi', NULL, NULL, '2016-12-07 11:55:43', '2016-12-07 11:55:43'),
(10, 'Joseph Marcus', 'techmarcs@gmail.com', '+2347053998908', '$2y$10$FPrsZ49/poYmiVIO71MSmuDsDmHgNYaSDzsGeHK2Zr6irCjA5UohO', NULL, NULL, '2016-12-13 13:29:33', '2016-12-13 13:29:33'),
(11, 'VICTOR IYERE', 'victor.iyere@coronaschools.org', '08033294597', '$2y$10$c2c7UFzQ//US3P0GxThVuebl3l374dcekm2vmXozWI7zqVALXmAZi', NULL, NULL, '2016-12-16 11:38:13', '2016-12-16 11:38:13'),
(12, 'Ukandu Gospel', 'gspeluk@gmail.com', '08063917744', '$2y$10$ZZeV4KlAEla9ogbXd7gckuB1vMiQd/S5ooWAF43SuTV/Ju1IQHqd.', NULL, NULL, '2016-12-21 15:47:20', '2016-12-21 15:47:20'),
(13, 'Barnypok', 'jfvynms4281rt@hotmail.com', '27544997857', '$2y$10$4v9hCKvlwKzYNxNpqV.lUuh/7aFQaja.VzwxF6lUVMH/GWX9wYDZy', NULL, NULL, '2016-12-28 09:32:31', '2016-12-28 09:32:31'),
(14, 'ishola usman olaide ', 'omowonuolabanjo@gmail.com', '09050198901', '$2y$10$lLcauz4Wl52F2183WIYuLeBqV2Kc/GV1YlrEJO2nXoPTz53i94hs6', NULL, NULL, '2016-12-28 17:46:19', '2016-12-28 17:46:19'),
(15, 'Barnypok', 'jimos4581rt@hotmail.com', '77745285233', '$2y$10$xjdAWhQRYG6lGTDpLzL6Cec8vq9t0daNg/xAXr.M7a6qimvOwUCce', NULL, NULL, '2017-01-08 23:45:42', '2017-01-08 23:45:42'),
(16, 'sfnsyd', 'dyubmc@tzdppa.com', 'IndyEHyjdKfFBNPEwBS', '$2y$10$.yC9VLVyNKYXf8/VWGqL0OvwF9f3hS6qOtKV24oHxTv5jDa82mTUe', NULL, NULL, '2017-01-09 19:58:33', '2017-01-09 19:58:33'),
(17, 'ekfpytadllc', 'ckowvs@lyvypt.com', 'yKvDwzuGuicidW', '$2y$10$0owRh8nmwUQA8Jnw1CjlU.naBhx.N1lV1Wr3nMcwejkdSl2m/2Yiq', NULL, NULL, '2017-01-11 00:34:41', '2017-01-11 00:34:41'),
(18, 'fezyffa', 'hnbxja@eyzfmg.com', '84197272285', '$2y$10$Y7F5lzzIU.H6fku06KXi8u.wO29GAWTlyZLDENK1mkiMC2XF9yANC', NULL, NULL, '2017-01-16 06:08:12', '2017-01-16 06:08:12'),
(19, 'Andre Bothma', 'bothmamail@gmail.com', '0027824584162', '$2y$10$0kIsEiU4qnoBBUiYUtrn2eCkZ6SuO5YeMTsjIMubwSr7Q3TdA5Hii', NULL, NULL, '2017-01-16 10:27:31', '2017-01-16 10:27:31'),
(20, 'jigqqqeuie', 'opogki@obllas.com', '67908089839', '$2y$10$9xp2T0diWLgDlBvvb7aTTusscvGYHld1YkVN.IAQgF7w1Vr4Dqv96', NULL, NULL, '2017-01-17 05:33:42', '2017-01-17 05:33:42'),
(21, 'I say olias', 'Olagadeyoisrael4christ@gmail.com ', '08020849500', '$2y$10$VSyV4WN7ELFE0ZEVchdVIe1KhDMNezJ1tpCV0JA38toYyxreCCl.m', NULL, NULL, '2017-02-10 20:56:01', '2017-02-10 20:56:01'),
(22, 'Bhadmus bayo ', 'Princeakdamuss@yahoo.com ', '09054998043', '$2y$10$0zXkNkswwaJ19Pj5XnvyrONyfJpClbDejFhWKI7vl50XTt7psHaL6', NULL, NULL, '2017-02-11 12:51:28', '2017-02-11 12:51:28'),
(23, 'Everest Okafor', 'everest4142@yahoo.com', '08033278641', '$2y$10$MAFdqay9eflsuAN7Vf2KD.kEAanhbrDZ9zX9J0CIwuLCyUt9atjC.', NULL, NULL, '2017-02-25 21:49:57', '2017-02-25 21:49:57'),
(24, 'noi', 'kkk@kkk.ng', '0846363536', 'ef3bc2445cc33c8414d3c8579aef76b862102c0d', NULL, NULL, NULL, NULL),
(29, 'Oladele', 'baale@odusfoundation.org', '8039120602', '9428eb5f20795eee132710520b100b36840ffbec', NULL, NULL, '2017-06-12 15:41:18', NULL),
(30, 'Kazeem', 'kazeem.noibi@techadvance.ng', '08095480534', '643fe104432feacb2df3685cf1ab4a719608b142', NULL, NULL, '2017-06-12 16:25:21', NULL),
(31, 'Noibi Kazeem', 'knoibi@yahoo.com', '07021334682', 'fa229a9bc554df9ec50803963a821dde8691fde4', NULL, NULL, '2017-06-12 17:05:07', NULL),
(32, 'Kazeem Noibi', 'kaduna@nasfat.org', '09090909', '04729fca00bd4da208e3c1aa40b51d436a85ebcc', NULL, NULL, '2017-06-12 17:24:18', NULL),
(33, 'Lucius', 'luciuslyeon@gmail.com', '9099020813', '1309dc78fda8843ea4550cc571cd5db5a9cab02f', NULL, NULL, '2017-06-13 08:59:28', NULL),
(34, 'Jide Okenwa', 'senkiesclown@yahoo.co.uk', '080788686785', 'f9ebc355b1191b89d9c8cb850dee47d73f91f082', NULL, NULL, '2017-06-13 09:02:10', NULL),
(35, 'Biola', 'codedruns2012@gmail.com', '08076541823', '3c16673ae628dfca7c5bd7bccbd15e83f1f52dc2', NULL, NULL, '2017-06-13 14:29:48', NULL),
(36, 'Noibi', 'noibilism@gmail.com', '8080764159', '946a7f1fdf430f8fb6bd736dc6b3c65b439028d0', NULL, NULL, '2017-06-13 15:54:51', NULL),
(40, 'Obioha Uche', 'obioh@yahoo.com', '07039828381', '08df4a90821db86a12056801f8fc0f104d3aa816', NULL, NULL, '2017-06-14 07:36:52', NULL),
(43, 'Victor Nze', 'vnzeh01@gmail.com', '08037239233', 'beaa1086767b1141b776adf48c48f4310baab9b3', NULL, NULL, '2017-06-15 22:30:25', NULL),
(44, 'Sedeke Uwak Esedeke', 'senztip@gmail.com', '08135120000', '8b0bab91d8d18aa124ea6fa79a47c1fdbf5ad961', NULL, NULL, '2017-06-16 18:55:54', NULL),
(46, 'Ade-Rufus Anuoluwapo T.', 'fredarpinkk@gmail.com', '08133458823', '12f3d0c5d37078991d2e8dde73259f5b0b1f3347', NULL, NULL, '2017-06-19 11:41:20', NULL),
(47, 'Gifty', 'Adogakay@gmail.com', '09072944390', 'd4bf4a6b8c2ed85e6888173d214671875faa990b', NULL, NULL, '2017-06-19 16:20:00', NULL),
(48, 'safds esnfd,nf', 'fsdldsfk@sfkdfks.cnd', '383535903593', '6e0a408a659501a240c3cda481a47f2dd8260b76', NULL, NULL, '2017-06-19 16:23:40', NULL),
(49, 'jane', 'mara@gmail.com', '000000000', '0f58d5a5515f1a8a9d179aa58858b67b2f8a3388', NULL, NULL, '2017-06-21 09:45:36', NULL),
(50, 'Ude Chelsea ', 'Iam favored418@gmail.com', '08065891679', '32e8524925fbd81d80ba46cb228fb2eabf83be11', NULL, NULL, '2017-06-22 14:41:01', NULL),
(52, 'Wale Ola', 'waleola@gmail.com', '08022373890', '9a94b48d0f4c958c9aba6172871d659db8838710', NULL, NULL, '2017-06-25 17:55:34', NULL),
(53, 'Ifeanyi agu', 'Ifeanyiagu64@gmail.com', '08134237275', 'c67e9fa1e676a14ba1b3c6ac69052624eee7c62e', NULL, NULL, '2017-06-29 14:29:47', NULL),
(54, 'odinchefu ibekwe', '', '08065321565', '24aad06b9ab06caf069f32d18bea1d0b7ef930bf', NULL, NULL, '2017-07-13 09:22:54', NULL),
(56, 'Dan', 'Dan@gmail.com', '090888766775', '48a0f9c87ecc724ba6589d25a7f2c2c9dbaac784', NULL, NULL, '2017-07-20 14:57:15', NULL),
(58, 'Cage', 'cage@gmail.com', '08079865906', 'c064f5694f8f2262f61c47a6dd4ce1659bd45df2', NULL, NULL, '2017-07-20 15:17:47', NULL),
(60, 'Ayobami Oluwatodimu Edward ', 'ebullient2312@gmail.com; oluwatodimuedward@gmail.com', '08055017264', '709dbf273e474902f5be8761c8f196d0f2e6b762', NULL, NULL, '2017-07-22 20:10:49', NULL),
(61, 'Godwin Elisha', 'tanduwubugya@yaho.com', '07044827881', '2fc35904ba75c661ac6c7eea9a37962be1a8902a', NULL, NULL, '2017-07-23 10:50:03', NULL),
(62, 'Olatunji joshua Kayode', 'kayodejoshua169@yahoo.com', '08168518511', '7fb40e7c668ce122b25e590554a361bdffd62cf6', NULL, NULL, '2017-07-23 21:44:11', NULL),
(63, 'hhhhji', 'gyhjjkk@gmail', '2407378883', '0921ec5cb24daa8787aa343f4d893c5333eb22dc', NULL, NULL, '2017-07-23 22:17:31', NULL),
(68, 'Dan', 'daborhib@gmail.com', '0908 797 8656', '9296971d2664fe43359009165d514f46c23b93d3', NULL, NULL, '2017-07-28 08:54:42', NULL),
(71, 'Jjjj', 'Hjjjjjjk', '00000', '6934105ad50010b814c933314b1da6841431bc8b', NULL, NULL, '2017-08-05 08:44:15', NULL),
(73, 'Okorafor Nkemjika Obasi', 'no2jal@gmail.com', '08138856706', 'e3f45d1f2467e3134f9832a1127f5b4c030b62be', NULL, NULL, '2017-08-10 17:41:41', NULL),
(74, 'dghjkkk', 'sdffggf@www', '24354656789', '464c899f8bc6090384cbecc60c0efae1823afe8c', NULL, NULL, '2017-08-10 22:06:18', NULL),
(75, 'Nneka', 'nneka@alphabet.xyz', '81321234556', 'e7166fb985afe4bb6f9ab21cb1c015a5458cf5d8', NULL, NULL, '2017-08-13 06:16:37', NULL),
(76, 'Lyris Okolie', 'lyrisokolie@gmail.com', '08032561673', '899b6bff4c74960c26a91edb6119f031a9ac2843', NULL, NULL, '2017-08-14 20:56:16', NULL),
(78, 'Wendy Moneke', 'ifymoneke@gmail.com', '08065511282', '221d739470e2781747a51fa8634eab9e1b4226cd', NULL, NULL, '2017-08-17 10:47:35', NULL),
(79, 'Usman Abiola', 'hamzinating@gmail.com', '07034837030', 'abfab9524625375c69fc784eb2e1176bbadc0c57', NULL, NULL, '2017-08-21 22:34:54', NULL),
(80, 'Abiola ', 'hamzinating@', '07034837020', '1f07560ebfab80dd29abedb7db0e0b7de74b561a', NULL, NULL, '2017-08-22 13:09:20', NULL),
(81, 'bokkos', 'bokkos@yopmail.com', '1234567890', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', NULL, NULL, '2017-08-26 07:43:24', NULL),
(82, 'Name name', 'email@yahoo.com', '08144555652', '4a4ad0e91b4c1d48e2f382e7fa2eed076bbcea91', NULL, NULL, '2017-09-11 08:18:29', NULL),
(83, 'Elvis', 'elvis.chidera@gmail.com', '8084848093', '1e14011ed6deb84705f9d54e8766285bf21b327b', NULL, NULL, '2017-09-18 06:23:21', NULL),
(86, 'Chike Obinugwu', 'henry.obinugwu@live.com', '09035310648', '8fc69986512ff66652d2d59faba4fb1422313f76', NULL, NULL, '2017-10-02 20:39:13', NULL),
(87, 'Jenifer', 'lola.majekodunmi@techadvance.ng', '08072022666', '55bf62fd9fb0c8d8e24d0904f55921edbfa2f1a3', NULL, NULL, '2017-10-03 08:49:33', NULL),
(88, 'Ifeoluwa King', 'ifeoluwaking24@gmail.com', '08187393185', 'a4d8acae2eab283e4907c55c9dce84d65e8a6dd8', NULL, NULL, '2017-10-03 11:27:09', NULL),
(89, 'TestG', 'testxxxxxg@gmail.com', '08092848458', '525d7518a6ade12e87e505f95d1a460bf1b97f25', NULL, NULL, '2017-10-05 17:27:47', NULL),
(91, 'Chloe', 'YINOSAVIN@YAHOO.COM', '+2348165624035', 'ebc3d2735e92a07fadaa52b52a98616649b89424', NULL, NULL, '2017-10-06 16:01:36', NULL),
(92, 'QWERYUIOH', 'CVGHJCVB@FGHJO', '07822234481', '022a202bb3c875464547f084e9370bd2c899e3df', NULL, NULL, '2017-10-08 02:12:27', NULL),
(93, 'ghmghm', 'hmgmghm', 'hfn', '58880834f5c3c18150fb5306e7ed0198473c7d2e', NULL, NULL, '2017-10-19 17:13:06', NULL),
(94, 'Kemdi Ebi', 'kemdi@enterfive.com', '+234 8167953202', 'a7d9e77e93140d5f5d5e6c3f5baf7e889f4410e0', NULL, NULL, '2017-10-20 20:06:02', NULL),
(95, 'Karina Aisinamen ', 'Ktackleberry@gmail.com ', '08182596842', '39b010a6cfb86e3becbd387b6396c7e0903c2a5e', NULL, NULL, '2017-10-21 21:18:44', NULL),
(97, 'Ajayi', 'ajayinurudeen998@gmail.com', '8023998118', '6eee7c1103eda0c1c8f84ba5cbe96519555e9d99', NULL, NULL, '2017-10-22 16:33:04', NULL),
(99, 'Ajayi', 'ydeveloper15@gmail.com', '8180738420', '9e40f21e35aab76286a3dc49e22cecf860c072be', NULL, NULL, '2017-10-22 16:49:09', NULL),
(101, 'Dare Okunyeye', 'oliver@nextdaysite.com', '08023447373', 'b6bb2b96b4dbcdeefa15ae0f57bb7aa151fb4996', NULL, NULL, '2017-10-22 19:56:03', NULL),
(107, 'Sarah Melson', 'sara@mail.com', '0903839299', '219b807669a29d3355942f4aef7aca8662c6e671', NULL, NULL, '2017-10-23 11:13:35', NULL),
(109, 'gbbbbbbbb', 'ccc@yahh.j', '000', '8aefb06c426e07a0a671a1e2488b4858d694a730', NULL, NULL, '2017-10-23 16:10:31', NULL),
(111, 'Ayo', 'senkiesclown@yahoo.com', '08085852265', '1f807fd58ca7980225cc019c15d4bfdd75b21436', NULL, NULL, '2017-10-23 17:32:06', NULL),
(117, 'abff', 'dd@gg.co', '999999999', 'f91d8f69c042267444b74cc0b3c747757eb0e065', NULL, NULL, '2017-10-24 17:53:39', NULL),
(119, 'Segun', 'drdr4@me.com', '0802090127', '5a6b818313c2ab877194c33d5c065aa4b548ec79', NULL, NULL, '2017-10-25 17:32:44', NULL),
(120, 'drizy', 'edrizoseni@gmail.com', '08077791194', '0e45834bf9f7cbd93f08bf78795530b68f4cf415', NULL, NULL, '2017-10-25 19:12:23', NULL),
(121, 'Soji Adeleye ', 'soji.adeleye@gmail.com ', '08126419921', '4444cfc3c5c3599dc07ba6b42316a76a4269fe0c', NULL, NULL, '2017-10-27 10:34:13', NULL),
(122, 'nwabueze marvee', 'ceomarvee@gmail.com', '08065339856', '2adf86990f0080993b8166973be3183e6a2315b1', NULL, NULL, '2017-10-27 18:38:31', NULL),
(124, 'Kw che', 'Kelcha2001@yahoo.com', '07534615892', '54fdd17440646b6b42c83fc490c88ffa67b3fef5', NULL, NULL, '2017-10-29 19:20:42', NULL),
(125, 'sfsdddddd', 'ssed@nd.com', '22222222222222222', '793f8ba94ee5f5734482af6e680993909f8fade3', NULL, NULL, '2017-10-29 20:22:16', NULL),
(126, 'Nwede chioma', 'Aushadesofbeauty@gmail.com', '08133151709', '833d84c8e3732a645be651baac3b435f835037d7', NULL, NULL, '2017-10-29 22:29:48', NULL),
(127, 'ola', 'olu@olu.com', '0893482849595', '659f41a483be530dde4b0daaa668f715b53d0fb0', NULL, NULL, '2017-10-30 00:06:28', NULL),
(128, 'Olatunji', 'wahab@gmail.com', '09099292921', '0a4588d2c8bc2d1efacb461f51634665269082e8', NULL, NULL, '2017-10-30 09:20:04', NULL),
(130, 'Ajayi', 'hayjay@mail.coms', '0802399811', '85f0dd27edcab7de95f91a0cb9dddcb4a8d26680', NULL, NULL, '2017-10-30 09:44:00', NULL),
(131, '', 'kundus@mail.com', '0902', '39e54a158cbd072006f00eddbf9a12712457328d', NULL, NULL, '2017-10-30 09:49:43', NULL),
(132, '', 'kazeem@bashorun.com', '08088228888', '14ebb5708311c96beb4a4b993c1f58d3918a90a8', NULL, NULL, '2017-10-30 09:53:30', NULL),
(134, '', 'lottery@mail.com', '08022828182', 'b56150b8bfc3d0c597a98b52301ad721f82984e2', NULL, NULL, '2017-10-30 10:09:55', NULL),
(135, 'chuks', 'Joseph', '09034563478', '2e9562f7de2c8c28d81623eb7acac785361b26a4', NULL, NULL, '2017-10-30 22:07:21', NULL),
(136, 'Ezendu chinelo', 'Swisstradephcity@gmail.com', '08066976483', 'c2415ecb380659d6cc975e53b0b4872c7b73d420', NULL, NULL, '2017-10-30 22:09:17', NULL),
(137, 'Pelumi ogundele', 'Ogundelepelumi042@gmail.com', '07035996076', 'ad011966c70a066a9e6706141c57cece8283a1cf', NULL, NULL, '2017-10-31 12:34:00', NULL),
(138, 'Noibi', 'bnoheeb@gmail.com', '7039828381', '7f0b01e6c02ba110650d24d9e75a625f328ac430', NULL, NULL, '2017-10-31 17:05:21', NULL),
(139, 'Ifeanyi Kalu ', 'Ifyinbox@gmail.com', '07038178349', 'e3833d6818d419830c2f69effbfdf549f0921fdd', NULL, NULL, '2017-10-31 20:01:31', NULL),
(140, 'OGECHUKWU NNADOZIE', 'ogennadozie @gmail.com', '08033763793', '05d5f206587e99c8c4309d3aef87bd1a90a69ead', NULL, NULL, '2017-11-01 09:23:22', NULL),
(144, 'WILSON MATIP', 'Matipboogwilsontelesphore@gmail.com', '08133034030', '02c25ccf89686cb9baf1033813eb53e718154527', NULL, NULL, '2017-11-01 19:55:54', NULL),
(145, 'Uwalaka ugochukwu Chris', 'chris4sure89@yahoo.com', '08039226549', '9d750d455a97405b4e90b71eb35ade462e67e261', NULL, NULL, '2017-11-02 16:09:20', NULL),
(146, 'emeka', 'tonye2@hotmail.com', '08023191521', '92b7c2cb280dc4e7617f3e1cca91e2f53f7bbcf7', NULL, NULL, '2017-11-03 07:09:04', NULL),
(147, 'Titilope Ogunlade', 'lade2002ng@yahoo.com', '08065826192', 'd9ec0d57fc3395e753a464574c43352394a2644b', NULL, NULL, '2017-11-03 12:23:49', NULL),
(148, 'Blessing alexandra', 'ruthonubie@yahoo.com', '08067780583', '5ca9601cf2a0c2b5f092399327d3ebe874b4a380', NULL, NULL, '2017-11-03 13:59:33', NULL),
(149, 'Eleojo Veronica Sadiq', 'vdeeksadiq@gmail.com', '08023997662', '61956d8f479e0245c8ed3f83c8bec077bcfb3e46', NULL, NULL, '2017-11-03 18:56:23', NULL),
(150, 'Adedoyin Yinka', 'timmytune002@gmail.com', '08154864010', 'de4aceabbd384a444d64c1619703ae3276a4df33', NULL, NULL, '2017-11-04 09:00:43', NULL),
(151, 'Mike ojo', 'mikeoj3000@gmail.com', '08034002763', '569d92cda797d688e1800ec21ebfe23a18c48695', NULL, NULL, '2017-11-06 08:17:26', NULL),
(157, 'Ola White', 'drdre4life2001@yahoo.com', '07038808765', '3328329056aee1ee9074ab30252dacdd493b625e', NULL, NULL, '2017-11-07 15:07:09', NULL),
(158, 'Emmanuel ifeanyi Nwogu', 'emmexgdc@gmail.com', '08189279890', '497ef373ddb0e59821d3439f3d2de37d6af8bf6c', NULL, NULL, '2017-11-08 00:58:57', NULL),
(159, 'George Iwola', 'georgeiwola@gmail.com', '07027312890', '112d2130774e7a71d97906d317b44b6f9a96294c', NULL, NULL, '2017-11-08 05:10:00', NULL),
(162, 'Blessing Iluebey', 'Iluebeyblessing@yahoo.com', '07065506494', 'cefe4b1838f41cac66fc3378b756faada9b55e6b', NULL, NULL, '2017-11-08 16:34:55', NULL),
(163, 'Alagba Tochukwu Ogeh', 'Princemedicpraise@gmail.com', '08188344225', 'c5bd7b3861de8a370a73449241f373fd79b1ca75', NULL, NULL, '2017-11-09 19:19:57', NULL),
(165, 'josegu udo', 'jogudo9sept@gmail.com', '08060999765', 'f5ac71475bdd6d8b0021c123e4519d7139125705', NULL, NULL, '2017-11-10 14:04:30', NULL),
(166, 'Afeez', 'abdhafiz87@gmail.com', '8025059390', 'c2429f1aff6095ffde3bbcc6cfed4897beea1589', NULL, NULL, '2017-11-11 08:45:22', NULL),
(167, 'Dare ayodele', 'Dareayodele123@yahoo.com', '08038786998', '8efbdb17a120e49d9be4131bb9c24436b269ca8d', NULL, NULL, '2017-11-11 19:25:08', NULL),
(169, 'xxxx', 'a@b.com', '08030000000', 'd007972c726ff656caeec383a35bd97d52833623', NULL, NULL, '2017-11-13 08:26:43', NULL),
(172, 'Rukevwe Akpochafo', 'mgold4u@gmail.com', '07062365556', '130e97322ea867b8ba4d93ddd345196ea3707bf4', NULL, NULL, '2017-11-14 16:22:27', NULL),
(176, 'Osagie Ernest ', 'Ernestosagie42@yahoo.com ', '07085450978', 'c70760bde9fdf68b6f7ee0e0c31f8c292e38dd0b', NULL, NULL, '2017-11-17 15:35:23', NULL),
(177, 'Olawolu Tosin', 'Mikkytee247@gmail.com', '08162645817', 'e3ec5a6b48a36d77d09d428e8d7b5815c93ef879', NULL, NULL, '2017-11-17 16:25:22', NULL),
(178, 'Alana stevens', 'Alana@cdo.com', '08034563323', '30526a302d7cdc50ab9db653d886ce455fafd2b9', NULL, NULL, '2017-11-17 22:36:22', NULL),
(179, 'Azeez Adigun', 'adigunazeez@gmail.com', '+2347038797386', '7bd6a896a3bf71af17652a0e5be8ea507250b003', NULL, NULL, '2017-11-18 13:17:35', NULL),
(180, 'Abdulhameed Yunusa', 'ayhameed176@gmail.com', '08110612516', '5de1e0c79bc8539eae15fcf8715ac4e89b154182', NULL, NULL, '2017-11-19 06:59:23', NULL),
(181, 'Ige opeyemi ', 'Yhemmie881@gmail.com', '08138239981', 'd6be9e02e903932b520e11ae932c9513f72b0c12', NULL, NULL, '2017-11-19 09:30:19', NULL),
(182, 'jejjejej', 'jejjeje@jrjrjr.com', '080000029929', '09475fa151686bec1e12d0d1f58d21e36fa748c6', NULL, NULL, '2017-11-19 15:55:31', NULL),
(183, 'Peace john', 'Pissjohn11@gmail.com', '08035393469', '13b9d3b0d31f2f88a8d431e40ddb3c59e1c0dfcd', NULL, NULL, '2017-11-19 19:40:35', NULL),
(184, 'muhammed dauda', 'slimddauda@mail.com', '08177771855', '516b8f8947583bd7f162fbf3afa9b6420eb94a51', NULL, NULL, '2017-11-20 00:36:28', NULL),
(187, 'Olaniyan Seun', 'olaniyan.seuntodara@gmail.com', '08137336318', '4f8d5e11d0391cf2a2578354090dca89ef498b2e', NULL, NULL, '2017-11-20 11:53:22', NULL),
(189, 'Olu Deniz', 'Odeniz@yahoo.com', '08765432123', 'a2d1e467858116e8d688490502d7c6a3cd1f03f8', NULL, NULL, '2017-11-21 20:43:51', NULL),
(197, 'Nwakalo Obianuju ', 'uujayu@gmail.com ', '08033102637', '707fe25dd9514d1bd48929fe3eb2fe93e1881c48', NULL, NULL, '2017-11-30 23:25:52', NULL),
(198, 'jhbh', 'jkj@dkd.com', '07030806423', 'b84a82048aa53b5866bd62788873bc38d9c8da90', NULL, NULL, '2017-11-30 23:50:27', NULL),
(199, 'Victory Thompson', 'victoryowhorji@gmail.com', '08115100787', 'c3aca1e63aac7b421f5d15df740f3ba84453c3d0', NULL, NULL, '2017-12-03 09:54:54', NULL),
(200, 'Ugochukwu', 'vincentnug@gmail.com', '08064301133', 'd81a840af00be37e0567911edf3b7fdcc7a1a2f1', NULL, NULL, '2017-12-03 22:58:46', NULL),
(201, 'MRS ANTHONIA IGBOEKWEZE', 'jantonia4real@yahoo.com', '07033404914', 'e2e41493dee45125cc705ce3c5665fe8e215cb98', NULL, NULL, '2017-12-04 09:44:19', NULL),
(202, 'Ogechi Benneth ', 'Bigogebenneth@gmail ', '08034145185', '09575a7b559aba3a34385634e66546efb4bba48f', NULL, NULL, '2017-12-05 08:08:43', NULL),
(203, 'GODIYA UMAN ', 'godiyausman68@gmail.com ', '08037456151', 'de1564bf0665cdce2d052b52c8fec14a4b4895f6', NULL, NULL, '2017-12-05 11:05:11', NULL),
(205, 'Ota Emmanuel Omorodion ', 'Emmanuel_ota@yahoo.com ', '09037874485', '6a18474557f7d43b961b4cef723fbefb33b0df2b', NULL, NULL, '2017-12-06 10:10:18', NULL),
(206, 'Test', 'Test@gmail.com', '5755777887666', '0bd1790de669d9d223164dfab4666ea76e01a047', NULL, NULL, '2017-12-06 16:13:58', NULL),
(207, 'Adefenwa Ibrahim', 'E60concept@gmail.com', '08033607198', '214064646d775232d9b3cb9364e3e7edcc78df1d', NULL, NULL, '2017-12-07 14:38:07', NULL),
(208, 'Augustine', 'austin0philip@yahoo.co.uk ', '08064622094', '2cc3c9dae8d653878eb402d053b011d6fcc8e89f', NULL, NULL, '2017-12-09 03:59:13', NULL),
(209, 'Adewumi Olatosimi', 'heryitee@icloud.com', '08024413871', '4779de6cd8b6ba91dcb48973908ec1b3d1fdd3d3', NULL, NULL, '2017-12-10 22:46:19', NULL),
(210, 'Aminat abu', 'abuaminat11@gmail.com', '8021124449', '6a9f18ff412d98460c9eb4c42e9031b324521e7f', NULL, NULL, '2017-12-10 22:48:42', NULL),
(211, 'Ff', 'tt@ff.com', '08090765487', 'ee6c24cbe14a850f1da67a8c4f9b2ebbcae2a4d4', NULL, NULL, '2017-12-10 23:02:09', NULL),
(212, 'MBANUGO VICTOR CHIBUIKE', 'vchibuike8@gmail.com', '08143319077', '7b1c0e1d3679a81a340131680d197d95436f816b', NULL, NULL, '2017-12-11 13:28:36', NULL),
(213, 'Okun Victor', 'vokun@live.com', '08035817066', '2ac1df334317b8b617562e5822b448f2a9d3d4a4', NULL, NULL, '2017-12-11 21:26:27', NULL),
(214, 'Amauche Jonathan', 'Debbyjoe114@gmail.com', '08035907498', 'bdb0ecfef9e7387b1d301f9cbeae46ba2539f0fd', NULL, NULL, '2017-12-12 17:16:31', NULL),
(215, 'queen beni', 'beniqu1994@gmail.com', '08035621897', '17cbcf67ee34467a95d1984acf378be46fd434ba', NULL, NULL, '2017-12-12 20:25:55', NULL),
(216, 'Agu Chijioke Raphael', 'aguraphael386@gmail.com', '08145108163', 'd57258caf3a2593d9a2bffefaf1a80759fee769a', NULL, NULL, '2017-12-13 04:40:59', NULL),
(217, 'Chinedu Okezie', 'chinedu123@yahoo.com', '07058653987', '7b1d0d9839cf76eb960eb00b56bf384eb5f8f1db', NULL, NULL, '2017-12-13 07:43:22', NULL),
(218, 'Olaniyi Olawunmi ', 'Elmbold2@yahoo.com', '08167099600', 'eef78aa73df8910254712097599282abee725658', NULL, NULL, '2017-12-13 15:41:40', NULL),
(219, 'Lanipekun Temitope B', 'ttblackk2000@gmail.com', '+2348038492421', 'bf13f33194872dec41d47963551751e96f7274fe', NULL, NULL, '2017-12-13 21:37:57', NULL),
(220, 'Ilang lugard chinaza', 'Ilanglugard2029@gmail.com', '0903361539', '36e9e8585abc9c8d68006e530816ec2eee2c1db3', NULL, NULL, '2017-12-13 22:59:29', NULL),
(223, 'Ogunleye Joshua Gbenga', 'Ogunleyejoshuag@gmail.com', '08104310941', 'c2e30906e013f496e228f2dd205f738e46934fdb', NULL, NULL, '2017-12-14 08:12:44', NULL),
(224, 'ENEBELI C CHUKWUMA ', 'chuks_2204@yahoo.com', '08029134712', 'adc3b16f426bc2fe91ca15445aa901973d43f279', NULL, NULL, '2017-12-14 09:25:03', NULL),
(229, 'Mark ver', 'Markebhojie@gmail.com ', '08166803921', 'aeacd9bfd8658029883d23d30ce7653fe458cad2', NULL, NULL, '2017-12-15 22:19:13', NULL),
(230, 'Ogbeche Mildred Ene', 'Roseogbeche@yahoo.com', '08085202321', 'ef03bb38bad363256916313a2c8a84dbc228493d', NULL, NULL, '2017-12-15 22:35:52', NULL),
(231, 'Ufomba okwuchi chinwendu', 'Ufombaokwuchichinwendu@gmail.com', '07030504500', '1303f0fee7bcba24327d4d8793eaa370bf313863', NULL, NULL, '2017-12-15 22:39:23', NULL),
(232, 'Adenusi Muyiwa ', 'nusimuyi@gmail.com', '08133987407', '1df451e3ed323f93566100ea6505707469bce9a2', NULL, NULL, '2017-12-15 22:40:07', NULL),
(233, 'nureni ade', 'nade1967@gmail.com', '07065343221', '56ec3b087556b62ec5882ec6bbe397298b4fb4ff', NULL, NULL, '2017-12-16 08:59:28', NULL),
(234, 'adewodun femidenu', 'femidenuadewodun@yahoo.com', '08027549974', '17f7fe92b4e3eb251c4165b07c47cf0aafa30297', NULL, NULL, '2017-12-16 12:33:08', NULL),
(235, 'Ahizechukwu unegbu ', 'Ahizechukwupaul@yahoo.com ', '08036244416 ', '3a5f8ae2c3ffb79fd470605b35be584ae9b86b75', NULL, NULL, '2017-12-16 22:25:12', NULL),
(236, 'Nnam Richard O.', 'grtrichnet12@gmail', '07015272600', '4ff8d42c017780c9c8e653a7000771b76b351303', NULL, NULL, '2017-12-17 02:39:06', NULL),
(237, 'Ahizechukwu Unegbu', 'Ahizechukwupaul @yahoo.com', '08082525343 ', '270e14762307c906b098becf46851622bdcf3ad7', NULL, NULL, '2017-12-17 13:24:02', NULL),
(238, 'Jimmy Akindele ', 'Mosesjumia@yahoo.com', '08087654321', 'cf87fc3daa3581555a39bb23397e613d7c2e1b2b', NULL, NULL, '2017-12-18 17:58:31', NULL),
(239, 'Benjamin Harrieth Ogochukwu', 'harriethb@yahoo.com', '08030570422', '066e01a1d89c1337b5a79fc07ca0c0514b75dae5', NULL, NULL, '2017-12-18 22:09:49', NULL),
(240, 'Chioma ', 'Oke Ira Ogba ', '08183151115', '84a376b2a0d6b2cac1ecf33b9b00e4a73cf5eb5f', NULL, NULL, '2017-12-19 09:47:50', NULL),
(241, 'Uzu tochi', 'Tochiuzu@gmail.com', '08027520276', '7c1329cad12d61b97cae74a0282223ce1f29066d', NULL, NULL, '2017-12-19 10:10:02', NULL),
(242, 'Uchechi hope echefu ', 'Talk2_ucgal@yahoo.com ', '09071831080', 'e8ee002d72c348721b7ac801c0d984a270481e51', NULL, NULL, '2017-12-19 18:18:13', NULL),
(243, 'Love Ekeke', 'ajikeo@gmail.com', '08064919933', 'f9ae15ef9860d08de41de2e8703419996f7aef9c', NULL, NULL, '2017-12-19 18:55:35', NULL),
(244, 'Okoli chukwuemeka Daniel', '7b oladunni Street Glory estate gbagada Lagos', '08065953200', '1faaa6d1c2139c591e95fc6772cdaa731728e4a0', NULL, NULL, '2017-12-19 20:25:45', NULL),
(245, 'Enendu Venus Emeka ', 'Venusemeka46@gmail.com', '08064903257', '3c2237026865e46f5ff18bafba87dd12d6c12701', NULL, NULL, '2017-12-20 06:07:37', NULL),
(246, 'Imoh Benedict', 'benedictimoh@gmail.com', '07065479973', '518a24c8648c95e39e9b86d5b136040374d37a20', NULL, NULL, '2017-12-20 09:19:34', NULL),
(247, 'OKEKE chukwukuebuka', 'Stephen.okeke@zenithbank.com', '08038002221', 'fe5a0d818485382869d868b0892c249186b55bd3', NULL, NULL, '2017-12-20 13:03:14', NULL),
(248, 'Okoli chika cynthia ', 'Chikafortune1234@gmail.com', '08105446955', '60b9dab5a67cc4dde90e95b66c3f03202bdf68f5', NULL, NULL, '2017-12-20 18:53:04', NULL),
(249, 'Emeka Igwilo', 'deiegit2k4@gmail.com', '08060792350', 'bd4125035aeb0bcbd626adcb71764554bc54a211', NULL, NULL, '2017-12-20 19:42:09', NULL),
(250, 'Nwabunwanne Michael Onyejiuwa ', 'Mieekikhash@gamil.com', '07062600281', '32e45291f4124abfa31ac6233e1867b950a5e04b', NULL, NULL, '2017-12-20 23:28:18', NULL),
(251, 'Uzokwe Francis chinazor', 'Francisuzokwe@yahoo.com', '08070767986', '6b836c39b1b488adddebbc022b1f1807779dda8c', NULL, NULL, '2017-12-21 07:20:11', NULL),
(252, 'wefefef', 'sshs@gmail.com', '234567898765', 'b45e0fbe2b7a66f1845120d88e7e1b64d77844ab', NULL, NULL, '2017-12-21 09:55:25', NULL),
(253, 'Edidiong James', 'edyjames96@gmail.com', '08134991878', '1b6d9159fafabe87abdcb4ee12031d46e2da7a2b', NULL, NULL, '2017-12-21 12:36:00', NULL),
(254, 'Rosemary', 'rose2anwuli@gmail.com', '08163529473', '49ac773bf73c16786d10373cdf5943de5e895b43', NULL, NULL, '2017-12-21 15:09:55', NULL),
(255, 'Favour Uzoeto', 'favour.uzoeto@yahoo.com', '08061294981', '8d4ca97979549b9336a163ca205b5960a59a3ecf', NULL, NULL, '2017-12-21 17:06:26', NULL),
(256, 'Maryjane Chikwem', 'No 3 Akintoye close Ojofu berger', '08037181889', 'cdb2d347c437f52db09c70e84f42f39b63253813', NULL, NULL, '2017-12-21 20:46:13', NULL),
(257, 'simonl loveth', 'simonloveth73  gmail.com', '07063560600', '44abae52a12f52fb1b50baef0c5113fc845bddfb', NULL, NULL, '2017-12-21 21:22:15', NULL),
(258, 'smith adesina', 'michog4real2007@yahoo.co.uk', '07037798854', 'f8ab9f03b0f570a125676f8d521b26ab5857f09c', NULL, NULL, '2017-12-22 13:00:39', NULL),
(259, 'Emeka Okejiri', 'okejiriemeka@ymail.com', '08035277176', '8e4a18997ca57726524173037a4d07fa0f5790ed', NULL, NULL, '2017-12-22 19:54:03', NULL),
(260, 'Text booking', 'info@bus.com.ng', '08027610615', 'd24fc18642972babcbab4e119470ee4cbd11b404', NULL, NULL, '2017-12-23 06:56:07', NULL),
(261, 'James Madison', 'yahoos@yahoo.com', '8745632145', '07e026fc1145f2c2be3d75d0ed84172367222f7d', NULL, NULL, '2017-12-23 14:50:17', NULL),
(262, 'Donkells Emmanuel ', 'Donkells ', '08108254514', '0902aa7b1227ff7044aee57cdb3983ed0a8a544e', NULL, NULL, '2017-12-24 16:34:24', NULL),
(263, 'Susan Ohaekelem', 'nnfankon@yahoo.com', '08064795286', '7a858a3d8fcb76e1ed12bd960967e057a1721511', NULL, NULL, '2017-12-25 02:39:44', NULL),
(265, 'chidoziem joseph Ekeocha', 'echidoziem@yahoo.com', '08063739256', '07dc6df8e5bf62e6b9cb237e455c37fd29e71b1d', NULL, NULL, '2017-12-25 15:34:45', NULL),
(266, 'METU', 'toniametu@gmail.com', '8032794717', 'dd76e3e849427db2da02ea0e5ec1f07b136dced2', NULL, NULL, '2017-12-26 15:05:17', NULL),
(268, 'Nmadu Sharon ', 'babe.sharon@rocketmail.com', '08165504771', '020d0dadb405209f44fe4ed832ca0c4a3ce43295', NULL, NULL, '2017-12-26 20:01:13', NULL),
(269, 'Onyejiaka Michael', 'respectchammer@gmail.com', '08137017944', '684d23f6a68e48ceed9903d61dc1fde675ff3556', NULL, NULL, '2017-12-27 19:39:06', NULL),
(270, 'Oko Chiedozie', 'oko.chiedozie@yahoo.com', '08063466263', 'bda5cb93f83decac5759695eb012073140f1d4c2', NULL, NULL, '2017-12-28 23:36:58', NULL),
(271, 'JACOB IKECHUKWU', 'ikjacob99@gmail.com', '07066625624', '12f3e26065d1a81bdb9d0c61f1aaefc0a99ab975', NULL, NULL, '2017-12-29 07:26:12', NULL),
(273, 'Christiana eze', 'Christianaeze78@gmail.com', '08109995971', 'b8e1d27eb1045c5dcaae54f4c514002e7729d43e', NULL, NULL, '2017-12-31 22:06:42', NULL),
(274, 'Edward Jumbo ', 'Edwardjumbo101@gmail.com ', '08148319100 ', 'a7a779bc89a980f3c9145f8a5a08205680017081', NULL, NULL, '2018-01-01 15:42:30', NULL),
(275, 'vivian', 'vivianomereonye@gmail.com', '07035088414', '47d9d5378b77120d5349de0ab478eb27e306531e', NULL, NULL, '2018-01-03 09:25:15', NULL),
(278, 'Mgbakor George', 'nwamelogo.gm@gmail.com', '08029363227', '86aa80b7fb1e737b82fcfe8525106eb0d7f9bacc', NULL, NULL, '2018-01-04 02:16:55', NULL),
(279, 'jon', 'jon@yahoo.com', '08098767892', 'cff443756904a5133febeb9354731cff137f4bf5', NULL, NULL, '2018-01-08 11:26:48', NULL),
(281, 'Segun', '1', '1', '356a192b7913b04c54574d18c28d46e6395428ab', NULL, NULL, '2018-02-02 06:47:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers_feedback`
--

CREATE TABLE `customers_feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feedback` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assigned_to` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_queue_jobs`
--

CREATE TABLE `failed_queue_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `feedback` text NOT NULL,
  `active` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `name`, `email`, `operator`, `feedback`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Noibi Kazeem', 'noibilism@gmail.com', 'GIGM', 'testing', 0, '2017-01-11 15:21:30', '2017-01-11 17:04:48'),
(2, 'Wale', 'wale@live.com', 'Okeyson', 'I Booked a bus from Bus.com.ng and it is so convenient I thought I was booking an airline ticket. Thank you ', 1, '2017-01-11 15:23:43', '2017-01-11 15:23:43'),
(3, 'Esther', 'esther4life@gmail.com', 'Aniekan', 'I Joined the Bus.com.ng beta test. Took a trip from Lagos to Owerri, the fact I was able to jump the queues and enter the bus was good o. You guys are doing it.', 1, '2017-01-11 15:36:49', '2017-02-09 13:53:22'),
(4, 'Chi Chi', 'chichimylove@yahoo.com', 'Okeyson', 'I used the Bus.com.ng site to get to Nsukka, very convenient and happy to see all the prices and pick a bus all from my home. Lagos traffic can be terrible. ', 1, '2017-01-11 15:38:23', '2017-01-11 15:38:23'),
(5, 'Dave Ozoalor', 'daveozoalor@gmail.com', '806', 'ddddd', 0, '2017-01-11 15:49:43', '2017-02-09 13:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `recipient_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `read_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_06_12_164855_create_agent_transactions_table', 0),
('2016_06_12_164855_create_agents_table', 0),
('2016_06_12_164855_create_banks_table', 0),
('2016_06_12_164855_create_booking_notes_table', 0),
('2016_06_12_164855_create_booking_refunds_table', 0),
('2016_06_12_164855_create_bookings_table', 0),
('2016_06_12_164855_create_bus_types_table', 0),
('2016_06_12_164855_create_call_logs_table', 0),
('2016_06_12_164855_create_cash_offices_table', 0),
('2016_06_12_164855_create_chartered_bookings_table', 0),
('2016_06_12_164855_create_devices_table', 0),
('2016_06_12_164855_create_failed_queue_jobs_table', 0),
('2016_06_12_164855_create_messages_table', 0),
('2016_06_12_164855_create_operators_table', 0),
('2016_06_12_164855_create_parks_table', 0),
('2016_06_12_164855_create_passengers_table', 0),
('2016_06_12_164855_create_password_resets_table', 0),
('2016_06_12_164855_create_payment_methods_table', 0),
('2016_06_12_164855_create_queue_jobs_table', 0),
('2016_06_12_164855_create_roles_table', 0),
('2016_06_12_164855_create_settings_table', 0),
('2016_06_12_164855_create_states_table', 0),
('2016_06_12_164855_create_transactions_table', 0),
('2016_06_12_164855_create_trips_table', 0),
('2016_06_12_164855_create_user_logs_table', 0),
('2016_06_12_164855_create_users_table', 0),
('2016_06_12_164857_add_foreign_keys_to_agent_transactions_table', 0),
('2016_06_12_164857_add_foreign_keys_to_booking_notes_table', 0),
('2016_06_12_164857_add_foreign_keys_to_booking_refunds_table', 0),
('2016_06_12_164857_add_foreign_keys_to_bookings_table', 0),
('2016_06_12_164857_add_foreign_keys_to_call_logs_table', 0),
('2016_06_12_164857_add_foreign_keys_to_chartered_bookings_table', 0),
('2016_06_12_164857_add_foreign_keys_to_devices_table', 0),
('2016_06_12_164857_add_foreign_keys_to_messages_table', 0),
('2016_06_12_164857_add_foreign_keys_to_parks_table', 0),
('2016_06_12_164857_add_foreign_keys_to_passengers_table', 0),
('2016_06_12_164857_add_foreign_keys_to_payment_methods_table', 0),
('2016_06_12_164857_add_foreign_keys_to_transactions_table', 0),
('2016_06_12_164857_add_foreign_keys_to_trips_table', 0),
('2016_06_12_164857_add_foreign_keys_to_user_logs_table', 0),
('2016_06_12_164857_add_foreign_keys_to_users_table', 0),
('2016_06_12_212048_add_sync_to_operator', 1),
('2016_06_14_232249_add_operator_booking_id_to_booking', 2),
('2016_08_25_004852_add_online_users', 3),
('2016_09_16_131219_create_customers_table', 4),
('2016_09_16_144950_create_customers_feedback_table', 4),
('2016_09_18_192912_add_customer_id_to_bookings_table', 5),
('2016_09_21_102639_create_social_accounts_table', 6),
('2016_09_27_145144_add_new_columns_to_passengers_table', 7),
('2016_09_27_145450_add_new_columns_to_chartered_bookings_table', 8),
('2016_09_27_163759_create_transaction_logs_table', 9),
('2016_09_28_130058_add_booking_code_to_transactions_table', 10),
('2016_09_29_085821_create_operator_booking_rules_table', 11),
('2016_09_30_101840_add_rule_column_to_operator_table', 12),
('2016_10_09_164542_create_seats_table', 13),
('2016_10_14_094256_create_bus_type_table', 14),
('2016_10_17_163550_add_parent_booking_id_bookings_table', 14),
('2016_10_19_171202_add_seat_no_to_bus_type', 15),
('2016_10_29_174729_add_operator_to_users', 16),
('2016_11_24_195858_add_fields_to_bus_types', 17),
('2016_11_24_235109_add_terms_and_booking_rules_to_operators', 17),
('2016_11_27_232234_add_trip_position_to_trips', 18),
('2016_11_29_231439_add_children_discounts_to_operators', 19),
('2016_11_30_004854_add_seats_and_age_group_to_passengers', 19),
('2016_11_30_012704_add_gender_to_bookings', 19),
('2016_11_30_014632_add_trip_id_to_passengers', 19),
('2016_12_06_010838_add_intl_fields_to_trips', 20),
('2016_12_06_011424_add_intl_fields_to_bookings', 20);

-- --------------------------------------------------------

--
-- Table structure for table `nysc_bookings`
--

CREATE TABLE `nysc_bookings` (
  `id` int(11) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `source_park_id` int(11) NOT NULL,
  `dest_camp_id` int(11) NOT NULL,
  `booking_code` varchar(45) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `total_fare` double(10,6) NOT NULL,
  `payment_mode` varchar(45) NOT NULL,
  `payment_reference` varchar(45) NOT NULL,
  `payment_status` varchar(45) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `travel_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nysc_bookings`
--

INSERT INTO `nysc_bookings` (`id`, `trip_id`, `source_park_id`, `dest_camp_id`, `booking_code`, `operator_id`, `total_fare`, `payment_mode`, `payment_reference`, `payment_status`, `passenger_id`, `travel_date`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, 'NYSC-OKY76941', 42, 0.000000, 'online', '4599042159807', 'On Hold', 313, '2017-06-20', '2017-06-20 13:33:43', '2017-06-20 13:33:43'),
(3, 1, 1, 1, 'NYSC-OKY33945', 42, 0.000000, 'offline', '7000316141123', 'On Hold', 314, '2017-06-20', '2017-06-20 13:53:22', '2017-06-20 13:53:22'),
(4, 1, 1, 1, 'NYSC-OKY85391', 42, 0.000000, 'offline', '9423981179477', 'On Hold', 315, '2017-06-20', '2017-06-20 13:53:39', '2017-06-20 13:53:39'),
(5, 1, 1, 1, 'NYSC-OKY47854', 42, 0.000000, 'offline', '7688918834259', 'On Hold', 316, '2017-06-20', '2017-06-20 13:54:01', '2017-06-20 13:54:01'),
(6, 1, 1, 1, 'NYSC-OKY69973', 42, 0.000000, 'offline', '4891765867703', 'On Hold', 317, '2017-06-20', '2017-06-20 13:55:20', '2017-06-20 13:55:20'),
(7, 1, 1, 1, 'NYSC-OKY89670', 42, 0.000000, 'offline', '3830144088237', 'On Hold', 318, '2017-06-20', '2017-06-20 13:56:39', '2017-06-20 13:56:39'),
(8, 1, 1, 1, 'NYSC-OKY69856', 42, 0.000000, 'offline', '4330916273260', 'On Hold', 319, '2017-06-20', '2017-06-20 13:57:04', '2017-06-20 13:57:04'),
(9, 1, 1, 1, 'NYSC-OKY77250', 42, 0.000000, 'offline', '130203972741', 'On Hold', 320, '2017-06-20', '2017-06-20 13:58:29', '2017-06-20 13:58:29'),
(10, 1, 1, 1, 'NYSC-OKY34970', 42, 0.000000, 'offline', '7013511928881', 'On Hold', 321, '2017-06-20', '2017-06-20 13:59:46', '2017-06-20 13:59:46'),
(11, 1, 1, 1, 'NYSC-OKY97502', 42, 0.000000, 'offline', '4841402454104', 'On Hold', 322, '2017-06-20', '2017-06-20 14:01:22', '2017-06-20 14:01:22'),
(12, 1, 1, 1, 'NYSC-OKY93254', 42, 0.000000, 'offline', '2107427528414', 'On Hold', 323, '2017-06-20', '2017-06-20 14:02:07', '2017-06-20 14:02:07'),
(13, 1, 1, 1, 'NYSC-OKY53975', 42, 0.000000, 'offline', '624660535066', 'On Hold', 324, '2017-06-20', '2017-06-20 14:03:31', '2017-06-20 14:03:31'),
(14, 1, 1, 1, 'NYSC-OKY86714', 42, 0.000000, 'offline', '5885102278421', 'On Hold', 325, '2017-06-20', '2017-06-20 14:03:53', '2017-06-20 14:03:53'),
(15, 1, 1, 1, 'NYSC-OKY83448', 42, 0.000000, 'offline', '1238591198549', 'On Hold', 326, '2017-06-20', '2017-06-20 14:04:08', '2017-06-20 14:04:08'),
(16, 1, 1, 1, 'NYSC-OKY74767', 42, 0.000000, 'offline', '310842885944', 'On Hold', 327, '2017-06-20', '2017-06-20 14:05:28', '2017-06-20 14:05:28'),
(17, 1, 1, 1, 'NYSC-OKY30109', 42, 0.000000, 'offline', '2487516342474', 'On Hold', 328, '2017-06-20', '2017-06-20 14:05:53', '2017-06-20 14:05:53'),
(18, 1, 1, 1, 'NYSC-OKY99965', 42, 0.000000, 'offline', '5081110937864', 'On Hold', 329, '2017-06-20', '2017-06-20 14:06:51', '2017-06-20 14:06:51'),
(19, 1, 1, 1, 'NYSC-OKY66252', 42, 0.000000, 'offline', '6950729449929', 'On Hold', 330, '2017-06-20', '2017-06-20 14:07:36', '2017-06-20 14:07:36'),
(20, 1, 1, 1, 'NYSC-OKY28376', 42, 0.000000, 'offline', '2061809644500', 'On Hold', 331, '2017-06-20', '2017-06-20 14:07:51', '2017-06-20 14:07:51'),
(21, 1, 1, 1, 'NYSC-OKY64727', 42, 0.000000, 'offline', '1215787587455', 'On Hold', 332, '2017-06-20', '2017-06-20 14:08:17', '2017-06-20 14:08:17'),
(22, 1, 1, 1, 'NYSC-OKY74614', 42, 0.000000, 'offline', '7855230661600', 'On Hold', 333, '2017-06-20', '2017-06-20 14:09:03', '2017-06-20 14:09:03'),
(23, 1, 1, 1, 'NYSC-OKY82771', 42, 0.000000, 'offline', '9339646297304', 'On Hold', 334, '2017-06-20', '2017-06-20 14:09:48', '2017-06-20 14:09:48'),
(24, 1, 1, 1, 'NYSC-OKY55863', 42, 0.000000, 'offline', '2253374314660', 'On Hold', 335, '2017-06-20', '2017-06-20 14:10:03', '2017-06-20 14:10:03'),
(25, 1, 1, 1, 'NYSC-OKY38644', 42, 0.000000, 'offline', '886841056176', 'On Hold', 336, '2017-06-20', '2017-06-20 14:10:30', '2017-06-20 14:10:30'),
(26, 2, 1, 1, 'NYSC-GIG28553', 33, 0.000000, 'offline', '9467071505514', 'On Hold', 337, '2017-06-22', '2017-06-20 14:13:23', '2017-06-20 14:13:23'),
(27, 2, 1, 1, 'NYSC-GIG85631', 33, 0.000000, 'offline', '6212929650343', 'On Hold', 338, '2017-06-22', '2017-06-20 14:14:36', '2017-06-20 14:14:36'),
(28, 2, 1, 1, 'NYSC-GIG79865', 33, 0.000000, 'offline', '6658218815788', 'On Hold', 339, '2017-06-22', '2017-06-20 14:15:06', '2017-06-20 14:15:06'),
(29, 2, 1, 1, 'NYSC-GIG44307', 33, 0.000000, 'offline', '3352591791654', 'On Hold', 340, '2017-06-22', '2017-06-20 14:15:25', '2017-06-20 14:15:25'),
(30, 2, 1, 1, 'NYSC-GIG77504', 33, 0.000000, 'offline', '2910320845139', 'On Hold', 341, '2017-06-22', '2017-06-20 14:15:36', '2017-06-20 14:15:36'),
(31, 2, 1, 1, 'NYSC-GIG70122', 33, 4600.000000, 'offline', '6420981029918', 'On Hold', 342, '2017-06-20', '2017-06-20 16:10:49', '2017-06-20 16:10:49'),
(32, 2, 1, 1, 'NYSC-GIG35718', 33, 4600.000000, 'online', '3983388', 'Successs: Approved by Financial Institution', 343, '2017-06-22', '2017-06-20 18:26:07', '2017-06-20 18:27:00'),
(33, 1, 1, 1, 'NYSC-OKY29954', 42, 5400.000000, 'online', '1414300225', 'Successs: Approved by Financial Institution', 344, '2017-06-22', '2017-06-21 12:57:45', '2017-06-21 12:59:41'),
(34, 2, 1, 1, 'NYSC-GIG42550', 33, 4600.000000, 'online', '839987321', 'On Hold', 345, '2017-06-22', '2017-06-21 13:08:05', '2017-06-21 13:08:08'),
(35, 2, 1, 1, 'NYSC-GIG95073', 33, 4600.000000, 'offline', '9687977629675', 'On Hold', 346, '2017-07-04', '2017-06-21 14:31:13', '2017-06-21 14:31:13'),
(36, 2, 1, 1, 'NYSC-GIG44627', 33, 4600.000000, 'online', '1995995107', 'Insufficient Funds', 347, '2017-06-28', '2017-06-26 21:15:38', '2017-06-26 21:17:47'),
(37, 1, 1, 1, 'NYSC-OKY28288', 42, 5400.000000, 'online', '364046772', 'On Hold', 348, '2017-06-30', '2017-06-28 12:28:30', '2017-06-28 12:28:33'),
(38, 1, 1, 1, 'NYSC-OKY30073', 42, 5400.000000, 'online', '2021475604', 'Insufficient Funds', 349, '2017-06-30', '2017-06-28 12:37:52', '2017-06-28 12:39:40'),
(39, 5, 53, 79, 'NYSC-OKY78628', 42, 4800.000000, 'online', '1780765053', 'On Hold', 350, '0000-00-00', '2017-07-05 09:34:52', '2017-07-05 09:34:57'),
(40, 3, 53, 1, 'NYSC-OKY20337', 42, 5500.000000, 'online', '1712221905', 'On Hold', 351, '2017-07-26', '2017-07-19 14:39:52', '2017-07-19 14:39:55'),
(41, 1, 1, 1, 'NYSC-OKY36120', 42, 5400.000000, 'offline', '2041056351510', 'On Hold', 352, '2017-07-14', '2017-07-19 14:58:25', '2017-07-19 14:58:25'),
(42, 3, 53, 1, 'NYSC-OKY34207', 42, 5500.000000, 'online', '1833234994', 'On Hold', 353, '2017-07-26', '2017-07-20 14:50:52', '2017-07-20 14:50:55'),
(43, 3, 53, 1, 'NYSC-OKY66400', 42, 5500.000000, 'offline', '187075411948', 'On Hold', 354, '2017-07-26', '2017-07-20 14:51:15', '2017-07-20 14:51:15'),
(44, 3, 53, 1, 'NYSC-OKY39984', 42, 5500.000000, 'online', '1667308350', 'On Hold', 355, '2017-07-22', '2017-07-21 09:50:22', '2017-07-21 09:50:24'),
(45, 1, 1, 1, 'NYSC-OKY29068', 42, 5400.000000, 'online', '442265983697', 'On Hold', 356, '0000-00-00', '2017-07-22 16:49:40', '2017-07-22 16:49:40'),
(46, 13, 53, 85, 'NYSC-CRS64710', 15, 6000.000000, 'offline', '7574380471712', 'On Hold', 357, '2017-07-25', '2017-07-23 01:06:12', '2017-07-23 01:06:12'),
(47, 42, 53, 105, 'NYSC-CRS26797', 15, 9999.999999, 'offline', '2823977223693', 'On Hold', 358, '2017-07-24', '2017-07-24 09:43:05', '2017-07-24 09:43:05'),
(48, 42, 53, 105, 'NYSC-CRS66896', 15, 9999.999999, 'offline', '3139579406368', 'On Hold', 359, '2017-07-26', '2017-07-24 09:44:20', '2017-07-24 09:44:20'),
(49, 7, 53, 80, 'NYSC-CRS85325', 15, 9999.999999, 'online', '1461767226', 'On Hold', 360, '2017-07-27', '2017-07-24 12:37:13', '2017-07-24 12:37:15'),
(50, 12, 37, 7, 'NYSC-KAN65131', 66, 7000.000000, 'online', '524336781', 'On Hold', 361, '2017-07-25', '2017-07-24 14:43:49', '2017-07-24 14:43:52'),
(51, 8, 10, 4, 'NYSC-OKY31559', 42, 4800.000000, 'online', '4192389104754', 'On Hold', 362, '2017-07-26', '2017-07-24 16:06:52', '2017-07-24 16:06:52'),
(52, 1, 1, 1, 'NYSC-OKY97459', 42, 5500.000000, 'offline', '7343100601401', 'On Hold', 363, '2017-07-27', '2017-07-24 17:24:02', '2017-07-24 17:24:02'),
(53, 32, 53, 16, 'NYSC-CRS38546', 15, 9999.999999, 'offline', '2004552683448', 'On Hold', 364, '2017-07-26', '2017-07-25 08:11:46', '2017-07-25 08:11:46'),
(54, 9, 53, 4, 'NYSC-CRS43289', 15, 7000.000000, 'online', '1456127387', 'On Hold', 365, '2017-07-28', '2017-07-26 16:14:39', '2017-07-26 16:14:42'),
(55, 9, 53, 4, 'NYSC-CRS36148', 15, 7000.000000, 'offline', '9823779698986', 'On Hold', 366, '2017-07-28', '2017-07-26 16:15:04', '2017-07-26 16:15:04'),
(56, 9, 53, 4, 'NYSC-CRS88046', 15, 7000.000000, 'online', '1333495729', 'On Hold', 367, '2017-07-28', '2017-07-26 16:18:03', '2017-07-26 16:18:06'),
(57, 9, 53, 4, 'NYSC-CRS65511', 15, 7000.000000, 'online', '905828552', 'On Hold', 368, '2017-07-28', '2017-07-26 18:21:26', '2017-07-26 18:21:29'),
(58, 22, 53, 11, 'NYSC-CRS51484', 15, 6000.000000, 'online', '587160408', 'On Hold', 369, '2017-09-12', '2017-08-18 09:11:42', '2017-08-18 09:11:44'),
(59, 1, 1, 1, 'NYSC-OKY26367', 42, 5500.000000, 'online', '503292609', 'On Hold', 370, '2017-12-31', '2017-09-13 10:22:30', '2017-09-13 10:22:33'),
(60, 2, 10, 1, 'NYSC-OKY96586', 42, 5500.000000, 'offline', '6836877607082', 'On Hold', 371, '2017-12-06', '2017-09-13 10:29:54', '2017-09-13 10:29:54'),
(61, 1, 1, 1, 'NYSC-OKY42633', 42, 5500.000000, 'offline', '2674095062324', 'On Hold', 372, '0000-00-00', '2017-10-24 01:43:38', '2017-10-24 01:43:38'),
(62, 7, 1, 4, 'NYSC-OKY46373', 42, 4800.000000, 'offline', '499210269614', 'On Hold', 373, '0000-00-00', '2017-10-24 06:17:02', '2017-10-24 06:17:02'),
(63, 7, 1, 4, 'NYSC-OKY26814', 42, 4800.000000, 'offline', '9164868723317', 'On Hold', 374, '0000-00-00', '2017-10-24 06:23:29', '2017-10-24 06:23:29'),
(64, 7, 1, 4, 'NYSC-OKY50281', 42, 4800.000000, 'offline', '4289870456172', 'On Hold', 375, '0000-00-00', '2017-10-24 06:49:45', '2017-10-24 06:49:45'),
(65, 7, 1, 4, 'NYSC-OKY46919', 42, 4800.000000, 'offline', '2212244642262', 'On Hold', 376, '0000-00-00', '2017-10-24 06:53:50', '2017-10-24 06:53:50'),
(66, 7, 1, 4, 'NYSC-OKY90866', 42, 4800.000000, 'offline', '4537508026111', 'On Hold', 377, '0000-00-00', '2017-10-24 08:35:20', '2017-10-24 08:35:20'),
(67, 1, 1, 1, 'NYSC-OKY94783', 42, 5500.000000, 'online', '1989973071', 'On Hold', 378, '0000-00-00', '2017-10-24 08:43:45', '2017-10-24 08:44:10'),
(68, 1, 1, 1, 'NYSC-OKY89872', 42, 5500.000000, 'online', '2051658518', 'On Hold', 379, '0000-00-00', '2017-10-24 08:44:16', '2017-10-24 08:44:27'),
(69, 1, 1, 1, 'NYSC-OKY88240', 42, 5500.000000, 'online', '7218477165534', 'On Hold', 380, '0000-00-00', '2017-10-24 09:32:20', '2017-10-24 09:32:20'),
(70, 1, 1, 1, 'NYSC-OKY54688', 42, 5500.000000, 'online', '7316285275361', 'On Hold', 381, '0000-00-00', '2017-10-24 09:44:29', '2017-10-24 09:44:29'),
(71, 1, 1, 1, 'NYSC-OKY72947', 42, 5500.000000, 'online', '949021986', 'On Hold', 382, '0000-00-00', '2017-10-24 09:46:02', '2017-10-24 09:46:12'),
(72, 1, 1, 1, 'NYSC-OKY53614', 42, 5500.000000, 'online', '486665089', 'On Hold', 383, '0000-00-00', '2017-10-26 11:07:34', '2017-10-26 11:07:34'),
(73, 1, 1, 1, 'NYSC-OKY28974', 42, 5500.000000, 'online', '404545460', 'Paid', 384, '0000-00-00', '2017-10-26 11:09:57', '2017-10-26 11:10:00'),
(74, 1, 1, 1, 'NYSC-OKY68410', 42, 5500.000000, 'online', '867587413', 'Paid', 385, '2017-10-18', '2017-10-26 15:43:50', '2017-10-26 15:43:51'),
(75, 16, 10, 8, 'NYSC-OKY93665', 42, 7000.000000, 'online', '1975717', 'Paid', 386, '2017-11-20', '2017-11-18 23:54:16', '2017-11-18 23:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `nysc_camps`
--

CREATE TABLE `nysc_camps` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nysc_camps`
--

INSERT INTO `nysc_camps` (`id`, `state_id`, `name`, `address`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 'Abia state Camp', NULL, 1, '2017-07-24 12:34:46', '2017-07-24 12:34:46'),
(2, 3, 'Adamawa state Camp', NULL, 1, '2017-07-24 12:34:46', '2017-07-24 12:34:46'),
(3, 4, 'Akwa Ibom state Camp', NULL, 1, '2017-07-24 12:34:46', '2017-07-24 12:34:46'),
(4, 5, 'Anambra state Camp', NULL, 1, '2017-07-24 12:34:46', '2017-07-24 12:34:46'),
(5, 6, 'Bauchi state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(6, 7, 'Bayelsa state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(7, 8, 'Benue state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(8, 10, 'Cross River state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(9, 11, 'Delta state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(10, 35, 'Ebonyi state Camp', NULL, 1, '2017-07-24 12:34:47', '2017-07-24 12:34:47'),
(11, 12, 'Edo state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(12, 36, 'Ekiti state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(13, 13, 'Enugu state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(14, 14, 'Gombe state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(15, 15, 'Imo state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(16, 16, 'Jigawa state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(17, 17, 'Kaduna state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(18, 18, 'Kano state Camp', NULL, 1, '2017-07-24 12:34:48', '2017-07-24 12:34:48'),
(19, 19, 'Katsina state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(20, 20, 'Kebbi state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(21, 21, 'Kogi state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(22, 22, 'Kwara state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(23, 23, 'Nasarawa state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(24, 24, 'Niger state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(25, 25, 'Ogun state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(26, 26, 'Osun state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(27, 27, 'Oyo state Camp', NULL, 1, '2017-07-24 12:34:49', '2017-07-24 12:34:49'),
(28, 28, 'Plateau state Camo', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50'),
(29, 29, 'Rivers state Camp', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50'),
(30, 30, 'Sokoto state Camp', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50'),
(31, 31, 'Taraba state Camp', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50'),
(32, 32, 'Bauchi state Camp', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50'),
(33, 33, 'Zamfara state Camp', NULL, 1, '2017-07-24 12:34:50', '2017-07-24 12:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `nysc_passengers`
--

CREATE TABLE `nysc_passengers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Adult',
  `next_of_kin_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nysc_passengers`
--

INSERT INTO `nysc_passengers` (`id`, `name`, `phone`, `email`, `gender`, `next_of_kin`, `next_of_kin_phone`, `created_at`, `updated_at`) VALUES
(313, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:33:43', '2017-06-20 13:33:43'),
(314, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:53:21', '2017-06-20 13:53:21'),
(315, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:53:39', '2017-06-20 13:53:39'),
(316, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:54:01', '2017-06-20 13:54:01'),
(317, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:55:19', '2017-06-20 13:55:19'),
(318, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:56:39', '2017-06-20 13:56:39'),
(319, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:57:04', '2017-06-20 13:57:04'),
(320, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:58:29', '2017-06-20 13:58:29'),
(321, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 13:59:46', '2017-06-20 13:59:46'),
(322, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:01:22', '2017-06-20 14:01:22'),
(323, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:02:07', '2017-06-20 14:02:07'),
(324, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:03:31', '2017-06-20 14:03:31'),
(325, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:03:53', '2017-06-20 14:03:53'),
(326, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:04:08', '2017-06-20 14:04:08'),
(327, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:05:28', '2017-06-20 14:05:28'),
(328, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:05:52', '2017-06-20 14:05:52'),
(329, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:06:51', '2017-06-20 14:06:51'),
(330, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:07:36', '2017-06-20 14:07:36'),
(331, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:07:51', '2017-06-20 14:07:51'),
(332, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:08:17', '2017-06-20 14:08:17'),
(333, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:09:03', '2017-06-20 14:09:03'),
(334, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:09:48', '2017-06-20 14:09:48'),
(335, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:10:03', '2017-06-20 14:10:03'),
(336, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Noibi Idris', '07039828381', '2017-06-20 14:10:30', '2017-06-20 14:10:30'),
(337, 'Boladale', '8080764159', 'boladale@yahoo.com', 'male', 'Bola', '08037121268', '2017-06-20 14:13:23', '2017-06-20 14:13:23'),
(338, 'Boladale', '8080764159', 'boladale@yahoo.com', 'male', 'Bola', '08037121268', '2017-06-20 14:14:36', '2017-06-20 14:14:36'),
(339, 'Boladale', '8080764159', 'boladale@yahoo.com', 'male', 'Bola', '08037121268', '2017-06-20 14:15:06', '2017-06-20 14:15:06'),
(340, 'Boladale', '8080764159', 'boladale@yahoo.com', 'male', 'Bola', '08037121268', '2017-06-20 14:15:25', '2017-06-20 14:15:25'),
(341, 'Boladale', '8080764159', 'boladale@yahoo.com', 'male', 'Bola', '08037121268', '2017-06-20 14:15:36', '2017-06-20 14:15:36'),
(342, 'Noibi Idris', '7039828381', 'bnoheeb@gmail.com', 'male', 'Mummy', '08037121268', '2017-06-20 16:10:49', '2017-06-20 16:10:49'),
(343, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08037121268', '2017-06-20 18:26:07', '2017-06-20 18:26:07'),
(344, 'Zad', '09088976754', 'Zad@gmail.com', 'male', 'Gad', '09088787656', '2017-06-21 12:57:45', '2017-06-21 12:57:45'),
(345, 'Ife', '08088776655', 'Ifeanyi@bus.com', 'male', 'King', '09088787656', '2017-06-21 13:08:05', '2017-06-21 13:08:05'),
(346, 'John', '08076543212', 'senkiesclown@yahoo.co.uk', 'female', 'Kay', '0908978675645', '2017-06-21 14:31:13', '2017-06-21 14:31:13'),
(347, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08080764159', '2017-06-26 21:15:38', '2017-06-26 21:15:38'),
(348, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08080764159', '2017-06-28 12:28:30', '2017-06-28 12:28:30'),
(349, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08080764159', '2017-06-28 12:37:52', '2017-06-28 12:37:52'),
(350, 'Weekly RoadMap', '8777799899', 'icalistbbus@gmail.com', 'male', 'bhhh', '900988777776', '2017-07-05 09:34:52', '2017-07-05 09:34:52'),
(351, 'Chinenye', '09063100238', 'doncassytha@gmail.com', 'female', 'Esther', '08160139706', '2017-07-19 14:39:52', '2017-07-19 14:39:52'),
(352, 'Noibi Adewale', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08080764159', '2017-07-19 14:58:25', '2017-07-19 14:58:25'),
(353, 'Sly', '9099020813', 'Sly@gmail.com', 'male', 'Han', '09088556644', '2017-07-20 14:50:52', '2017-07-20 14:50:52'),
(354, 'Sly', '9099020813', 'Sly@gmail.com', 'male', 'Han', '09088556644', '2017-07-20 14:51:15', '2017-07-20 14:51:15'),
(355, 'kauy', '090998776', 'kay@gmail.com', 'male', 'hay', '09099887766', '2017-07-21 09:50:22', '2017-07-21 09:50:22'),
(356, 'elias oluwaseyi blessing', '', '', 'male', '', '', '2017-07-22 16:49:40', '2017-07-22 16:49:40'),
(357, 'Michael Odusanwo', '08120868431', 'mowheez@gmail.com', 'male', 'Temitayo Odusanwo', '08085761436', '2017-07-23 01:06:12', '2017-07-23 01:06:12'),
(358, 'kola', '0908899877756', 'kola@gmail.com', 'male', 'bau', '0909988776576', '2017-07-24 09:43:05', '2017-07-24 09:43:05'),
(359, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08080764159', '2017-07-24 09:44:20', '2017-07-24 09:44:20'),
(360, 'gok', '9099020813', 'gok@gmail.com', 'male', 'aso', '07099884445', '2017-07-24 12:37:13', '2017-07-24 12:37:13'),
(361, 'Ifunanya Moneke', '08065511282', 'ifymoneke@gmail.com', 'female', 'Tokunbo', '07031051233', '2017-07-24 14:43:49', '2017-07-24 14:43:49'),
(362, 'iana', '9099020809', 'aiana', 'male', 'kol', '0909898886', '2017-07-24 16:06:52', '2017-07-24 16:06:52'),
(363, 'Noibi Kazeem', '8080764159', 'noibilism@gmail.com', 'male', 'Mummy', '08037121268', '2017-07-24 17:24:02', '2017-07-24 17:24:02'),
(364, 'Okoh Jordan Ehimen', '08065930989', 'mrjordanokoh@gmail.com', 'male', 'Dora okoh', '08062879319', '2017-07-25 08:11:46', '2017-07-25 08:11:46'),
(365, 'luk', '9088877972', 'luk@gmail.com', 'male', 'lik', '09088787656', '2017-07-26 16:14:39', '2017-07-26 16:14:39'),
(366, 'luk', '9088877972', 'luk@gmail.com', 'male', 'lik', '09088787656', '2017-07-26 16:15:04', '2017-07-26 16:15:04'),
(367, 'luk', '9088877972', 'luk@gmail.com', 'male', 'lik', '09088787656', '2017-07-26 16:18:03', '2017-07-26 16:18:03'),
(368, 'luk', '9088877972', 'luk@gmail.com', 'male', 'lik', '09088787656', '2017-07-26 18:21:26', '2017-07-26 18:21:26'),
(369, 'ejej', '990499', 'k@g.c', 'male', 'ndk', '09083948', '2017-08-18 09:11:42', '2017-08-18 09:11:42'),
(370, 'dflkdsk', '-3933', 'sd@gm.co', 'male', 'wei', '00000', '2017-09-13 10:22:30', '2017-09-13 10:22:30'),
(371, 'fhfhfhqq', '999', 'd@ff.c', 'male', 'ddd', '9999', '2017-09-13 10:29:54', '2017-09-13 10:29:54'),
(372, 'Dangote', '08847574773', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '09084747477', '2017-10-24 01:43:37', '2017-10-24 01:43:37'),
(373, 'Shedrack', '08183737474', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '39747372820', '2017-10-24 06:17:02', '2017-10-24 06:17:02'),
(374, 'Shedrack', '08183737474', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '39747372820', '2017-10-24 06:23:28', '2017-10-24 06:23:28'),
(375, 'Shedrack', '08183737474', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '39747372820', '2017-10-24 06:49:45', '2017-10-24 06:49:45'),
(376, 'Shedrack', '08183737474', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '39747372820', '2017-10-24 06:53:50', '2017-10-24 06:53:50'),
(377, 'Shedrack', '08183737474', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '39747372820', '2017-10-24 08:35:19', '2017-10-24 08:35:19'),
(378, 'Shedrack', '0938383822', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '094949848949', '2017-10-24 08:43:44', '2017-10-24 08:43:44'),
(379, 'Shedrack', '0938383822', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '094949848949', '2017-10-24 08:44:15', '2017-10-24 08:44:15'),
(380, 'Education', '08685884831', 'admin@bus.com', 'male', 'Bailly Wong', '09558584844', '2017-10-24 09:32:20', '2017-10-24 09:32:20'),
(381, 'Elizabeth', '08685884831', 'admin@bus.com', 'male', 'Bailly Wong', '09558584844', '2017-10-24 09:44:28', '2017-10-24 09:44:28'),
(382, 'Esther', '08685884831', 'admin@bus.com', 'male', 'Bailly Wong', '09558584844', '2017-10-24 09:46:02', '2017-10-24 09:46:02'),
(383, 'Ifeoluwa King', '10818377212', 'ifeoluwaking24@gmail.com', 'male', 'Bailly Wong', '09088382821', '2017-10-26 11:07:33', '2017-10-26 11:07:33'),
(384, 'Bill Clinton', '08187393185', 'ifeoluwaking24@gmail.com', 'male', 'Winifred', '08187393185', '2017-10-26 11:09:57', '2017-10-26 11:09:57'),
(385, 'Noibi Idris', '7039828381', 'bnoheeb@gmail.com', 'male', 'Kazeem', '08080764159', '2017-10-26 15:43:49', '2017-10-26 15:43:49'),
(386, 'Opeyemi ige', '08138239981', 'Yhemmie881@gmail.com', 'female', 'Omolola amole', '08182056985', '2017-11-18 23:54:16', '2017-11-18 23:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `nysc_trips`
--

CREATE TABLE `nysc_trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `trip_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `source_park_id` int(10) UNSIGNED NOT NULL,
  `dest_camp_id` int(10) UNSIGNED NOT NULL,
  `fare` double(8,2) NOT NULL,
  `discount` double DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nysc_trips`
--

INSERT INTO `nysc_trips` (`id`, `operator_id`, `trip_code`, `source_park_id`, `dest_camp_id`, `fare`, `discount`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 42, 'OKY-72256', 1, 1, 5500.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(2, 42, 'OKY-19799', 10, 1, 5500.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(3, 15, 'CRS-59668', 53, 1, 8000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(4, 66, 'KAN-32222', 37, 2, 12500.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(5, 15, 'CRS-11493', 53, 2, 15000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(6, 15, 'CRS-17566', 53, 3, 8000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(7, 42, 'OKY-61943', 1, 4, 4800.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(8, 42, 'OKY-88659', 10, 4, 4800.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(9, 15, 'CRS-21490', 53, 4, 7000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(10, 15, 'CRS-15950', 53, 5, 16000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(11, 15, 'CRS-76613', 53, 6, 8000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(12, 66, 'KAN-48957', 37, 7, 7000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(13, 15, 'CRS-70402', 53, 7, 10000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(14, 15, 'CRS-20199', 53, 8, 11000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(15, 42, 'OKY-62697', 1, 8, 7000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(16, 42, 'OKY-35068', 10, 8, 7000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(17, 15, 'CRS-89052', 53, 9, 6000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(18, 53, 'EFX-26224', 53, 9, 8000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(19, 15, 'CRS-72176', 53, 10, 9000.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(20, 42, 'OKY-97715', 10, 10, 5500.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(21, 42, 'OKY-26304', 1, 10, 5500.00, 0, NULL, '2017-07-24 12:49:56', '2017-07-24 12:49:56'),
(22, 15, 'CRS-91462', 53, 11, 6000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(23, 53, 'EFX-59266', 53, 11, 8000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(24, 15, 'CRS-77493', 53, 12, 7000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(25, 15, 'CRS-30750', 53, 13, 9000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(26, 42, 'OKY-49834', 1, 13, 5000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(27, 42, 'OKY-11336', 10, 13, 5000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(28, 15, 'CRS-50610', 53, 14, 15000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(29, 15, 'CRS-96495', 53, 15, 7000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(30, 42, 'OKY-11235', 10, 15, 4800.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(31, 42, 'OKY-11685', 1, 15, 4800.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(32, 15, 'CRS-54241', 53, 16, 15000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(33, 15, 'CRS-67296', 53, 17, 10000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(34, 15, 'CRS-16257', 53, 18, 13000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(35, 15, 'CRS-69242', 53, 19, 15000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(36, 15, 'CRS-83801', 53, 20, 15000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(37, 15, 'CRS-12494', 53, 21, 8000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(38, 15, 'CRS-75818', 53, 22, 8000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(39, 15, 'CRS-42991', 53, 23, 10000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(40, 15, 'CRS-57611', 53, 24, 12000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(41, 15, 'CRS-21400', 53, 25, 6000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(42, 15, 'CRS-58723', 53, 26, 6000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(43, 15, 'CRS-15479', 53, 27, 6000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(44, 15, 'CRS-53803', 53, 28, 12000.00, 0, NULL, '2017-07-24 12:49:57', '2017-07-24 12:49:57'),
(45, 15, 'CRS-16247', 53, 29, 8000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(46, 15, 'CRS-67128', 53, 30, 15000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(47, 66, 'KAN-81488', 37, 31, 13000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(48, 15, 'CRS-68109', 53, 31, 15000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(49, 15, 'CRS-65926', 53, 32, 15000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(50, 15, 'CRS-11140', 53, 33, 15000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(51, 71, 'ANK-6534890', 133, 3, 7500.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58'),
(52, 71, 'ANK-09872626', 133, 8, 9000.00, 0, NULL, '2017-07-24 12:49:58', '2017-07-24 12:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `online_users`
--

CREATE TABLE `online_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `online_users`
--

INSERT INTO `online_users` (`id`, `name`, `email`, `phone`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Wale', 'wale@gmail.com', '0803142353', 1, NULL, '2016-08-25 00:54:54', '2016-08-25 00:54:54'),
(2, 'deji lana', 'me@dejilana.com', '08025455574', 1, NULL, '2016-08-25 02:15:01', '2016-08-25 02:15:01'),
(3, 'Emmanuel', 'erekosimaosi@yahoo.com', '08099448310', 1, NULL, '2016-08-25 10:21:54', '2016-08-25 10:21:54'),
(4, 'Olawale', 'oawomolo@yahoo.com', '07030506262', 1, NULL, '2016-08-25 11:02:05', '2016-08-25 11:02:05'),
(5, 'Omuli Iwere', 'mullian99@gmail.com', '07013326756', 1, NULL, '2016-08-25 12:46:44', '2016-08-25 12:46:44'),
(6, 'Effi Gloria', 'gloriaeffi@gmail.com', '08034567305', 1, NULL, '2016-08-25 19:33:26', '2016-08-25 19:33:26'),
(7, 'Valentine Odili Akponor', 'valdelord@yahoo.com', '08176960693', 1, NULL, '2016-08-26 00:25:57', '2016-08-26 00:25:57'),
(8, 'Okechukwu Livinus', 'livi4success@gmail.com', '07038815655', 1, NULL, '2016-08-26 13:10:51', '2016-08-26 13:10:51'),
(9, 'moses olowa', 'mosope72@yahoo.com', '07084533189', 1, NULL, '2016-08-26 17:17:21', '2016-08-26 17:17:21'),
(10, 'Kosiso Okpalla', 'kosyokpalla@gmail.com', '08038554740', 1, NULL, '2016-08-27 14:25:22', '2016-08-27 14:25:22'),
(11, 'Uchechi hope echefu', 'talk2_uchal@yahoo.com', '07066619739', 1, NULL, '2016-08-27 22:59:18', '2016-08-27 22:59:18'),
(12, 'Kanayo Ejiofor', 'kanayo.ejiofor@gmail.com', '08030603010', 1, NULL, '2016-08-28 06:41:44', '2016-08-28 06:41:44'),
(13, 'Ladi Ojora', 'ladiojora@gmail.com', '08138332548', 1, NULL, '2016-08-28 23:45:55', '2016-08-28 23:45:55'),
(14, 'Yussuf ', 'yussuf3ng@yahoo.com', '08023979548', 1, NULL, '2016-08-29 02:16:26', '2016-08-29 02:16:26'),
(15, 'Abegunde Michael', 'impactcentric@yahoo.com', '08039422997', 1, NULL, '2016-08-29 03:34:10', '2016-08-29 03:34:10'),
(16, 'jjjsjjsjjsjsj', 'bbsbsbvsvsvsvvsvs', 'bsbbsbsbbsbshdbgd', 1, NULL, '2016-08-29 10:17:53', '2016-08-29 10:17:53'),
(17, 'Lord Rex', 'rex@rex41e.com', '07731352551', 1, NULL, '2016-08-29 10:46:27', '2016-08-29 10:46:27'),
(18, 'SHEILA ', 'sheilaashnelson@gmail.com', '07062250389', 1, NULL, '2016-08-29 10:46:51', '2016-08-29 10:46:51'),
(19, 'Anwana Elizabeth and Olanike Awoyinka ', 'Atoki eliz10@gmail.com', '08064654971', 1, NULL, '2016-08-29 13:26:04', '2016-08-29 13:26:04'),
(20, 'Charles', 'cosmasndu@gmail.com', '08024436791', 1, NULL, '2016-08-29 16:02:33', '2016-08-29 16:02:33'),
(21, 'JJJ', 'jojo@yahoo.com', '08087771111', 1, NULL, '2016-08-29 16:15:32', '2016-08-29 16:15:32'),
(22, 'rahman olatunji', 'rhazur@yandex.com', '08052000921', 1, NULL, '2016-08-29 16:18:10', '2016-08-29 16:18:10'),
(23, 'alex mark', 'alexmark@gmail.com', '08065897895', 1, NULL, '2016-08-29 16:20:58', '2016-08-29 16:20:58'),
(24, 'LovelyJohn', '08067109223', 'Hassanjohn4christ@gmail.com', 1, NULL, '2016-08-30 13:22:07', '2016-08-30 13:22:07'),
(25, 'EMeka', 'Henry', '07067806384', 1, NULL, '2016-08-30 16:56:28', '2016-08-30 16:56:28'),
(26, 'Aoise Minjiba', 'lovelybsno.1@hotmail.com', '+2347089168078', 1, NULL, '2016-08-31 02:56:07', '2016-08-31 02:56:07'),
(27, 'Chidiebere', 'chydy4love@yahoo.com', '08050448305', 1, NULL, '2016-08-31 06:07:32', '2016-08-31 06:07:32'),
(28, 'Osunsakin Abimbola ', 'labim15@yahoo.com', '07080066903', 1, NULL, '2016-08-31 12:35:28', '2016-08-31 12:35:28'),
(29, 'Lawal ', 'lawaldebssy@gmail.com', '08034152118', 1, NULL, '2016-08-31 15:56:48', '2016-08-31 15:56:48'),
(30, 'Oduola Timothy', 'timothy.olaolu@yahoo.com', '08111300030', 1, NULL, '2016-08-31 16:56:01', '2016-08-31 16:56:01'),
(31, 'Ayo Aribidara', 'ayosonade@gmail.com', '08061275002', 1, NULL, '2016-08-31 19:18:25', '2016-08-31 19:18:25'),
(32, 'Nnamdi ', 'nonsodguy@yahoo.com', '08090900856 ', 1, NULL, '2016-08-31 20:11:51', '2016-08-31 20:11:51'),
(33, 'obasuyi osas', 'trip4fineboy@yahoo.com', '08103244658', 1, NULL, '2016-08-31 20:12:35', '2016-08-31 20:12:35'),
(34, 'Kayode Adekanye', 'oluwanifesimi@yahoo.com', '08027076652', 1, NULL, '2016-08-31 23:28:04', '2016-08-31 23:28:04'),
(35, 'Precious Elliot', 'preciousezi@gmail.com', '07083918075', 1, NULL, '2016-09-01 01:17:08', '2016-09-01 01:17:08'),
(36, 'Paul', 'peeajayi@gmail.com', '08134407723', 1, NULL, '2016-09-01 05:17:26', '2016-09-01 05:17:26'),
(37, 'GIBSON', 'GIBSONRAY@ALIYUN.COM', '08037332474', 1, NULL, '2016-09-01 09:56:13', '2016-09-01 09:56:13'),
(38, 'zainab', 'zainab.iliyasu@aun.edu.ng', '08162224943', 1, NULL, '2016-09-01 11:27:26', '2016-09-01 11:27:26'),
(39, 'Chinevu', 'amadichinevu@yahoo.com', '07035085884', 1, NULL, '2016-09-01 22:51:08', '2016-09-01 22:51:08'),
(40, 'aba', 'abanotr@yahoo.fr', '8012405560', 1, NULL, '2016-09-01 23:18:06', '2016-09-01 23:18:06'),
(41, 'hfvbg', 'yhjh@yagg.com', '0814568907', 1, NULL, '2016-09-02 11:58:40', '2016-09-02 11:58:40'),
(42, 'joy', 'ikanindidi@gmail.com', '08045892389', 1, NULL, '2016-09-02 12:47:09', '2016-09-02 12:47:09'),
(43, 'Emmanuel', 'oylance@gmail.com', '08107191870', 1, NULL, '2016-09-02 19:33:13', '2016-09-02 19:33:13'),
(44, 'Tosin', 'lantos22006@gmail.com', '07032142807', 1, NULL, '2016-09-02 22:22:14', '2016-09-02 22:22:14'),
(45, 'Oduh Patricia c', 'oduhpatriciac@gmail.com', '08034343927', 1, NULL, '2016-09-03 00:18:34', '2016-09-03 00:18:34'),
(46, 'daniel', 'peter@mailina.com', '08723232222', 1, NULL, '2016-09-03 17:47:20', '2016-09-03 17:47:20'),
(47, 'robert gelbart', '6951 hinsdale dr  viera melbourne', '321-639-8258', 1, NULL, '2016-09-03 18:34:34', '2016-09-03 18:34:34'),
(48, 'oluchi', 'mba', '0xw08168444997', 1, NULL, '2016-09-03 21:22:24', '2016-09-03 21:22:24'),
(49, 'Temi', 'alus.beeme@gmail.com', '+2347054398124', 1, NULL, '2016-09-03 23:44:48', '2016-09-03 23:44:48'),
(50, 'Oa', 'olufunsoakinsanya@gmail.com', '08180469238', 1, NULL, '2016-09-04 03:12:19', '2016-09-04 03:12:19'),
(51, 'Ayo', 'me@ayolana.com', '08038953794', 1, NULL, '2016-09-04 19:45:09', '2016-09-04 19:45:09'),
(52, 'fasdfsadf', 'dssdfs@dasfasdf.com', '09055544433', 1, NULL, '2016-09-05 13:49:00', '2016-09-05 13:49:00'),
(53, 'abiodun daramola', 'biodarams@yahoo.co.uk', '08055576611', 1, NULL, '2016-09-05 17:02:30', '2016-09-05 17:02:30'),
(54, 'IFEANYI', 'NWANKWO_IFEANYI76@YAHOO.COM', '08098108991', 1, NULL, '2016-09-06 07:52:30', '2016-09-06 07:52:30'),
(55, 'fff', 'dennis@ff.com', '08087878787', 1, NULL, '2016-09-07 20:29:57', '2016-09-07 20:29:57'),
(56, 'okang sarah', 'okangsarahs@gmail.com', '08063665842', 1, NULL, '2016-10-26 09:00:42', '2016-10-26 09:00:42'),
(57, 'jones', 'jones.oseha@yahoo.com', '08037592878', 1, NULL, '2016-10-26 15:31:20', '2016-10-26 15:31:20'),
(58, 'victor oyelowo', 'folabioyelowo@gmail.com', '08060907855', 1, NULL, '2016-10-27 12:20:45', '2016-10-27 12:20:45'),
(59, 'NREWA', 'nrewa@hotmail.com', '08088452593', 1, NULL, '2016-10-28 02:27:41', '2016-10-28 02:27:41'),
(60, 'michael Adewale', 'Mychlewhale10@gmail.com', '+2348102786962', 1, NULL, '2016-10-28 09:49:45', '2016-10-28 09:49:45'),
(61, 'anita edematie', 'anita.edematie@unifiedpaymentsnigeria.com', '2348033991085', 1, NULL, '2016-10-28 13:18:35', '2016-10-28 13:18:35'),
(62, 'austin', 'princeaustin55@gmail.com', '08188506249', 1, NULL, '2016-10-28 15:12:10', '2016-10-28 15:12:10'),
(63, 'Mac Lawson', 'maclawson40@yahoo.co.uk', '+2348099777001', 1, NULL, '2016-10-28 18:06:24', '2016-10-28 18:06:24'),
(64, 'oto', 'teetones@live.co.uk', '+2348056089199', 1, NULL, '2016-10-28 19:47:29', '2016-10-28 19:47:29'),
(65, 'Emmanuel', 'Chinezcares@outlook.com', '08028828834', 1, NULL, '2016-10-28 21:27:27', '2016-10-28 21:27:27'),
(66, 'osemwengie', 'osemwengieizekor@gmail.com', '+2348039708080', 1, NULL, '2016-10-29 14:49:36', '2016-10-29 14:49:36'),
(67, 'ABDULWASIU DANJUMA SHITTU', 'shittu87@gmail.com', '+2348134513637', 1, NULL, '2016-10-31 01:16:05', '2016-10-31 01:16:05'),
(68, 'Onwuka Wilberforce', 'melie57@yahoo.com', '2348033304022', 1, NULL, '2016-10-31 13:59:18', '2016-10-31 13:59:18'),
(69, 'Tola Aderigbigbe', 't.aderigibigbe@snepco.com', '08097635421', 1, NULL, '2016-10-31 14:20:55', '2016-10-31 14:20:55'),
(70, 'ayo', 'ayorinde.oyebade@gmail.com', '+2347065558653', 1, NULL, '2016-11-01 03:26:06', '2016-11-01 03:26:06'),
(71, 'Vince', 'vinn4ever@yahoo.com', '08032305376', 1, NULL, '2016-11-01 10:10:24', '2016-11-01 10:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `operators`
--

CREATE TABLE `operators` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `commission` double(8,2) NOT NULL,
  `booking_rules` text COLLATE utf8_unicode_ci,
  `local_children_discount` double NOT NULL DEFAULT '0',
  `intl_children_discount` double NOT NULL DEFAULT '0',
  `local_terms` text COLLATE utf8_unicode_ci,
  `intl_terms` text COLLATE utf8_unicode_ci,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portal_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `should_sync` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rule` text COLLATE utf8_unicode_ci NOT NULL,
  `excel_template` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `operators`
--

INSERT INTO `operators` (`id`, `name`, `access_code`, `code`, `address`, `contact_person`, `phone`, `email`, `details`, `website`, `commission`, `booking_rules`, `local_children_discount`, `intl_children_discount`, `local_terms`, `intl_terms`, `img`, `portal_link`, `should_sync`, `active`, `deleted_at`, `created_at`, `updated_at`, `rule`, `excel_template`) VALUES
(1, 'ABC  TRANSPORT LTD', '1234567', 'ABC', 'Plot 79, Oba KayodeAkinyemi Way Amuwo-Odofin ', 'Customer Care', '07042226222', '', '', 'http://www.abctransport.com', 5.00, NULL, 0, 0, '', '', 'abc.png', 'http://localhost/bus-operator/public/', 1, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:49:44', '', NULL),
(2, 'ABIA LINE NETWORK', '', 'ABI', 'JIBOWU', 'Customer Care', '08030500600', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:50:24', '', NULL),
(3, 'AKWA IBOM TRANS', '', 'AKW', '7,ISHERI ROAD OJODU BERGER', 'HAPPINESS', '08060019400', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:50:52', '', NULL),
(4, 'ANAMBRA EXPRESS', '', 'ANB', '19A Ikorodu Road Jibowu Bus Lagos.', 'Ifeanyin Onwuachhusi', '08035876201', '', '', '', 5.00, NULL, 0, 0, '', '', 'anambra.jpg', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:51:12', '', NULL),
(5, 'ANIDS MASS TRANSIT', '', 'AMT', 'JIBOWU', 'juliet', '08062979716', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'anids.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:51:43', '', NULL),
(6, 'APARA LINE EXECUTIVE', '', 'APR', 'OSHODI-CHARITY', 'CHIMA', '08033577237', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:52:12', '', NULL),
(7, 'AUTO STAR EXECUTIVE', '', 'AUT', 'JIBOWU', 'Charity', '08037544998', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'autostar.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:52:27', '', NULL),
(8, 'BEST WAY', '', 'BST', 'Oshodi - Bolade', 'Mike', '08056070518', '', '', '', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:52:53', '', NULL),
(9, 'BONNY WAY', '', 'BON', 'Ijora 7up', 'Ifeanyi', '08186337911', '', '', '', 5.00, NULL, 0, 0, '', '', 'bonny_logo.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:53:15', '', NULL),
(10, 'C.U.O MOTORS', '', 'CUO', 'Oshodi - Bolade', 'MR GODWIN ', '08185983828', '', '', '', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:53:28', '', NULL),
(11, 'CHIDIEBERE TRANSPORT LIMITED', '', 'CHD', 'JIBOWU', 'Mr Tom', '08087980276', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:53:43', '', NULL),
(12, 'CHIDWA TRANSPORT', '', 'CHI', 'Ijesha', 'Obinna', '07033415866, 08026955865', '', '', '', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:53:59', '', NULL),
(13, 'CHISCO TRANSPORT NIG LTD', '', 'CHS', '104 Funsho Williams Ave Ipori, Costain Lagos', 'Alphonsus Iyobhebhe', '08029429926', '', '', 'http://www.chiscogroupng.com', 5.00, NULL, 0, 0, '', '', 'chisco.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:54:42', '', NULL),
(14, 'CONSTANT LINK VENTURES', '', 'CON', 'JIBOWU', '', '', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:55:03', '', NULL),
(15, 'CROSS COUNTRY LTD', '', 'CRS', '345, Murtala muhammed way Yaba Lagos', 'Jolomi, Deborah, Femi,Tofunmi', '08055555595, 08028430304, 08028443411, 07088084271', 'crosscountryltd@yahoo.ca , jolomi_okorodudu@hotmail.co.uk', '', 'http://www.crosscountry.com.ng', 5.00, NULL, 10, 10, '<table width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"53\">S/N</td>\r\n<td width=\"183\">Conditions</td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td width=\"183\">All buses have AC, DVD,Head Rest, Seat belt and nourishing meal</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Seat capacity is 45 and 49</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td width=\"183\">Departure time is 10am from Yaba Park</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td width=\"183\">Boarding time is 8:30am</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td width=\"183\">Passengers should be at the terminal at most one hour before boarding time</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td width=\"183\">Reports on ticket sold sent 24 hours before departure date</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td width=\"183\">Children between 4 and 12 years 10% discount</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'cross.png', '', 0, 1, NULL, '2013-07-29 12:43:42', '2016-12-19 16:11:19', '', 'cross_country.xlsx'),
(16, 'DB EXPRESS', '', 'DBE', 'IDDO ALTRA MODERN SHOPPING COMPLEX IDDO LAGOS', 'james Terfa Ayia', '08024325579', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:55:19', '', NULL),
(17, 'DE MODERN', '', 'DEM', 'Ojota Road', 'Mr. Samuel Osunkwor', '08055668894, 08077032645, 0807703288', '', '', '', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:55:37', '', NULL),
(18, 'DOMINION EXPRESS', '', 'DOM', 'Ojota bus-stop, ojota road', 'bro wale', '08033027901/07072691821', '', '', 'http://www.Dominionexpress.com', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:55:57', '', NULL),
(19, 'E.EKESON BROS NIG LTD', '', 'EKE', '31, Road, Jibowu, Ikorodu, Lagos', 'Unknown', '01-8611200, 08035680056', '', '', 'http://www.Unknown.com', 5.00, NULL, 0, 0, '<table width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 51px;\">S/N</td>\r\n<td style=\"width: 247px;\">Conditions</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">1</td>\r\n<td style=\"width: 247px;\">All buses have AC, DVD,Head Rest, Seat belt and nourishing meal</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">2</td>\r\n<td style=\"width: 247px;\">Seat capacity is 15 Toyota Haice and 26 Mini Lux, Luxury 59</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">3</td>\r\n<td style=\"width: 247px;\">Departure time is 6:20am from Jibowu Park and night 5:30pm</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">4</td>\r\n<td style=\"width: 247px;\">Boarding time is 6:00am</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">5</td>\r\n<td style=\"width: 247px;\">Passengers should be at the terminal at most thirty minute before boarding time</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">6</td>\r\n<td style=\"width: 247px;\">Reports on ticket sold sent 24 hours before departure date</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">7</td>\r\n<td style=\"width: 247px;\">Children between 10 year below pay 50% off ticket price and 11 years upward 100% which is the actual fare</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 51px;\">8</td>\r\n<td style=\"width: 247px;\">Ticket booked and didn\'t travel on the surposed date is only valid for on month</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'ekeson.png', '', 0, 1, NULL, '2013-07-29 12:43:42', '2016-12-19 16:11:59', '', 'ekesons.xlsx'),
(20, 'EDDYSON MOTORS', '', 'EDD', 'MAZA-MAZA', 'Eddyson', '08033350539', '', '', '', 5.00, NULL, 0, 0, '', '', 'eddy.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:56:21', '', NULL),
(21, 'EDEGBE LINE', '', 'EDE', 'Plot 199, Ugbowo Lagos Road Benin City', 'CUSTOMER SERVICE', '+234 703 465 4165', '', '', 'http://edegbemotorsltd.com', 5.00, NULL, 0, 0, '', '', 'edegbe_line_name.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:57:29', '', NULL),
(22, 'EDOBOR LINE ', '', 'EDO', 'OGBA', 'Manager', '08030525222', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '', '', 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 16:57:08', '', NULL),
(23, 'EFEX-EXECUTIVE', '', 'EFX', ' 77/79, Murtala Muhammed Way, Yaba Lagos', 'Mr Tunde', '08077790250,08023134121', '', '', 'http://www.efex-executive.com.ng', 5.00, NULL, 0, 0, '', '', 'efex.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2017-01-25 17:00:00', 'a:2:{s:6:\"infant\";a:2:{s:7:\"min_age\";i:0;s:7:\"max_age\";i:9;}s:8:\"discount\";a:3:{i:0;a:3:{s:7:\"min_age\";i:0;s:7:\"max_age\";i:3;s:19:\"discount_percentage\";i:100;}i:1;a:3:{s:7:\"min_age\";i:4;s:7:\"max_age\";i:6;s:19:\"discount_percentage\";i:10;}i:2;a:3:{s:7:\"min_age\";i:7;s:7:\"max_age\";i:9;s:19:\"discount_percentage\";i:5;}}}', NULL),
(24, 'EHISBEN MOTORS', '', 'EHI', 'Ikota, Lekki', 'Bankole Oladokun', '08129459270', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'ehisben.jpg', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-08-27 15:19:11', '', NULL),
(25, 'EKENE DILI CHUKWU TRANSPORT', '', 'EKE', 'NO 1 JIBOWU OFF IKORODU ROAD.LAGOS', 'Unknown', '08037465055', '', '', 'http://www.Unknown.com', 5.00, NULL, 0, 0, NULL, NULL, 'ekene-dili-chukwu.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-09-30 17:09:55', '', NULL),
(26, 'EVERGREEN MOTORS', '', 'EVE', 'Bolade Bus-stop, Oshodi', 'Chibuzor', '08060517714', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-09-30 16:06:07', '', NULL),
(27, 'EZENWATA', '', 'EZE', 'Agege ', 'Mr Ibrahim', '08034128049', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-10-02 11:01:00', '', NULL),
(28, 'FAIR PLUS MOTORS', '', 'FRP', 'OTTO LAGOS', '', '', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'fairplus.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(29, 'FREEMAN MOTORS', '', 'FRE', 'Ojota Road', 'Mr. Kehinde', '08084444918, 08064603960', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'freeman.jpg', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(30, 'G.AGOFURE MOTORS', '', 'GAG', '154 P.T.I. Road Effurun . Deco Road . Warri', 'Unknown', '08063922418, 08078176536, 07082116715', '', '', 'http://www.Unknown.com', 5.00, NULL, 0, 0, NULL, NULL, 'aguo.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(31, 'G.U.O TRANSPORT', '', 'GUO', 'KM 12 Onitsha/Enugu Expressway Onitsha Anambr', 'Chukwuemezeim Makuo', '08075090613', '', '', 'http://guoandsons.com', 5.00, NULL, 0, 0, NULL, NULL, 'guo.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-08-20 14:06:31', '', NULL),
(32, 'GOBISON MOTORS', '', 'GOB', 'OTTO LAGOS', '', '', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(33, 'GOD IS GOOD MOTORS', '', 'GIG', '202B Uselu Lagos Road, Near Ediaken Mkt Benin', 'Unknown', '07051117010 08056898615', '', '', 'http://www.goodisgoodmotors.com', 5.00, NULL, 0, 0, NULL, NULL, 'gig.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-04-07 09:27:59', '', NULL),
(34, 'GODDY EDOSA MOTORS', '', 'GDY', 'Badagry Expressway', 'Customer Care', '01 7746164', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(35, 'HIS GRACE EXPRESS', '', 'HIS', '103,isheri road ojodu', 'Benjamin', '08034597789/08073630386', '', '', 'http://www.hisgraceexpress.com', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-08-27 15:35:32', '', NULL),
(36, 'IFEAYINCHUKWU MOTORS', '', 'IFE', 'OTTO CAUSE WAY', 'MR Nnamdi (Manager)', '08036591479', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-10-02 12:15:58', '', NULL),
(37, 'IMO TRANSPORT', '', 'IMT', 'OSHODI-CHARITY ROAD', 'FRANKLINE', '', '', '', 'http://www.imotransport.com', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(38, 'IZUCHUKWU TRANSPORT', '', 'IZU', 'JIBOWU', '', '', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(39, 'JOCHY MOTORS', '', 'JCY', '34 Ikorodu Rd, Jibowu, Lagos ', 'Stephen Sunday', '08082561236', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'jochy.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-08-27 15:56:02', '', NULL),
(40, 'KINGS MOTORS', '', 'KNG', 'VOLKS', 'MANAGER', '08033514278', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(41, 'LIBRA MOTORS', '', 'LBR', 'MILE 2', 'EGHOSA ODOHUYI', '08034721709', '', '', '', 5.00, NULL, 0, 0, '', '', 'Libra.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2016-12-19 16:11:45', '', NULL),
(42, 'OKEYSON INVESTMENT', '', 'OKY', 'JIBOWU', 'Udeji Charles ,Osondu Grace,Ifeoma', '08033216982,08029645691,', 'okeyson_group@yahoo.com,amakaokeyson@gmail.com,ifeomaadaobi47@yahoo.com', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, '<table width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"55\">S/N</td>\r\n<td width=\"302\">Conditions</td>\r\n<td width=\"183\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td width=\"302\">All buses have AC, Head Rest and Seat belt</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Seat capacity is 15 to all destination</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td width=\"302\">Departure time is 6:00am across all parks 1st bus</td>\r\n<td>2nd bus departure time 7: 00am</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td width=\"302\">Boarding time is 5:30am</td>\r\n<td>Second bus boarding time 6:30am</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td width=\"302\">Passengers should be at the terminal at most thirty minute before boarding time</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td width=\"302\">Reports on ticket sold sent 24 hours before departure date</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td width=\"302\">Children between 0-4 is fee and 5 years and above pay 100%</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>8</td>\r\n<td width=\"302\">Ticket booked and didn&rsquo;t travel is only valid for 1 month</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'okeyson_logo.png', '', 0, 1, NULL, '2013-07-29 12:43:42', '2016-12-08 13:17:25', '', 'okeson.xlsx'),
(43, 'OSAREMEN MOTORS', '', 'OSA', 'OGBA', 'MANAGER', '08023405030', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(44, 'OYEMEKUN MOTORS', '', 'OYE', 'Ojota Road', 'Ademola', '08165087149', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'oyemekun.jpg', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-08-27 15:40:59', '', NULL),
(45, 'PEACE MASS TRANSIT', '', 'PMT', '233 Layout Emene Enugu', 'Unknown', '08055091827', '', '', 'http://peacemasstransit.com', 5.00, NULL, 0, 0, NULL, NULL, 'peace.jpg', '', 0, 0, NULL, '2013-07-29 12:43:42', '2015-01-07 16:35:36', '', NULL),
(46, 'SUNQUICK MOTORS', '', 'SUN', 'Mazamaza bustop.', '', '', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(47, 'SURE VICTORY', '', 'SUR', 'Oshodi - Charity', 'Justin', '0803332307', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-09-30 16:49:28', '', NULL),
(48, 'UYI MOTORS', '', 'UYI', 'VOLKSWAGEN BUS/STOP.BADAGRY EXPRESSWAY LAGOS', '', '08029991755', '', '', 'http://www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(49, 'q', '', 'YLM', '158 lagos Abeokuta express road, Iyana Ipaja', 'Dipo', '08037169013, 07059216452', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-07-29 12:43:42', '', NULL),
(50, 'YOUNG SHALL GROW', '', 'YNG', 'Oshodi - Bolade', 'Chikwendu', '08022446559', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'ysg.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2013-10-03 10:20:27', '', NULL),
(51, 'YOUNG SHALL GROW MOTORS LTD', '', 'YON', 'Alakija  Old Ojo Road Maza-Maza Lagos', 'Unknown', '08037169013, 07059216452', '', '', 'http://www.ysgtransport.com', 5.00, NULL, 0, 0, NULL, NULL, 'ysg.png', '', 0, 0, NULL, '2013-07-29 12:43:42', '2014-09-03 10:18:47', '', NULL),
(52, 'EDOBOR LINE', '', 'EDO', 'EDOBOR LINE', 'bus', '0800000000', '', 'EDOBOR LINE', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-05 14:49:52', '2013-08-05 14:49:52', '', NULL),
(53, 'EFEX EXECUTIVE', '', 'EFX', 'EFEX-EXECUTIVE', 'EFEX-EXECUTIVE', '0800000000', '', 'dfsdfsd', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'efex.png', '', 0, 0, NULL, '2013-08-05 14:57:39', '2013-10-03 15:45:06', '', NULL),
(54, 'AGBONIFO LINE', '', 'AGB', 'Yaba Lagos', 'Manager', '08023709614', '', '', 'www.bus,com,ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-19 10:45:06', '2013-10-02 14:37:39', '', NULL),
(55, 'IYARE MOTORS', '', 'IYR', '181 Abeokuta Express way Iyana ipaja.', 'Mr Monday Ihenyen', '07036168899, 08111116794', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-21 08:34:46', '2013-08-21 08:34:46', '', NULL),
(56, 'FIRST TARZAN', '', 'FIR', 'Iddo Terminus by railway Station, Opposite Iddo Brt Bus-Stop', 'Mr Onyekwere (Maanger)', '08034252978', '', '', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-26 13:22:06', '2013-08-26 13:22:06', '', NULL),
(57, 'BENUE LINK', '', 'BEN', 'Cause way road, opposite Iddo Plaza.', 'Mr Ajimy', '08036926178', '', '', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-26 13:24:19', '2014-09-10 15:57:33', '', NULL),
(58, 'SILVER TRAVEL', '', 'SIL', 'Cause way, Iddo, Opposite Iddo Plaza', 'Mr Joseph', '08060720705', '', '', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-26 13:25:52', '2013-08-26 13:25:52', '', NULL),
(59, 'NOUNCIL LINE', '', 'NOU', '19 Cause way, otto', 'Mr Gbenga', '08084502203', '', '', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-26 13:28:19', '2013-08-26 13:28:19', '', NULL),
(60, 'AU MOTORS', '', 'AUM', 'Cele Bus-stop', 'Osas (Manager)', '08023842598', '', '', 'www.bus.com.ng', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-28 08:43:32', '2013-08-28 08:45:02', '', NULL),
(62, 'ORIZU MOTORS', '', 'ORZ', 'Alafia orile', 'Orizu Ogamma', '08184562201', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-09-25 15:17:25', '2013-09-25 15:17:25', '', NULL),
(63, 'I.T.C', '', 'ITC', 'Charity bus stop oshodi expressway', 'Nonso', '08037903636', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-09-30 16:42:45', '2013-10-03 10:11:30', '', NULL),
(64, 'AKWA IBOM TRANS', '', 'AIT', 'opposite chery\'s place by Ikots bus  stop', 'kenneth', '07036630903', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-10-02 11:52:11', '2013-10-02 13:40:38', '', NULL),
(65, 'IFESINACHI TRANSPORT', '', 'IFS', 'IKORODU ROAD  JIBOWU.', 'MR NELSON', '08037201173', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'ifesinachi.jpg', '', 0, 0, NULL, '2013-10-02 14:58:19', '2013-11-14 10:01:38', '', NULL),
(66, 'KANTA CRUISE', '', 'KAN', 'Iddo Plaza,Iddo', 'Mr Hosana', '08135244407', '', '', '', 5.00, NULL, 0, 0, '<table width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"53\">S/N</td>\r\n<td width=\"261\">Conditions</td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td width=\"261\">All buses have AC, Seat belt</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Seat capacity is 15 Toyota Haice</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td width=\"261\">Departure time is 6:30am</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td width=\"261\">Boarding time is 6:20am</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td width=\"261\">Passengers should be at the terminal at most thirty minute before boarding time</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td width=\"261\">Children between 5year upward pay 100% on tickets which is the actual fare</td>\r\n</tr>\r\n<tr>\r\n<td>8</td>\r\n<td width=\"261\">Ticket booked and didn\'t travel on the surposed date is still valid for month</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'kanta.png', '', 0, 1, NULL, '2014-01-31 00:42:21', '2017-01-25 16:59:30', '', 'kanta_cruise.xlsx'),
(67, 'GREENER LINE', '', 'GRE', 'Plot 12, Airport Road, P.O Box 2639, Warri, Delta State', 'Manager', '07030628640, 08039322665', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2014-03-04 17:20:23', '2014-03-04 17:27:12', '', NULL),
(68, 'Chioni Transport', '', 'CHO', 'Maryland', 'k', '08035157004', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2014-10-08 12:13:43', '2014-10-08 12:13:43', '', NULL),
(69, 'D&D Resources', '', 'DDR', 'Anthony', 'Oloko Isreal Kalu ', '0809987665444', '', '', '', 5.00, NULL, 0, 0, NULL, NULL, 'dd.jpg', '', 0, 0, NULL, '2014-10-08 12:16:30', '2014-10-08 12:16:30', '', NULL),
(70, 'Unknown', '', 'UKN', 'Unknown', '', '', '', NULL, NULL, 5.00, NULL, 0, 0, NULL, NULL, 'operator.png', '', 0, 0, NULL, '2013-08-28 08:43:32', '2013-08-28 08:43:32', '', NULL),
(71, 'Aniekan Abasi Transport Company', '', 'ANI', 'Mr Godwin', 'Mr Godwin', ' 080 227 341 36', '', '', '', 0.00, NULL, 0, 0, '<table width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"55\">S/N</td>\r\n<td width=\"302\">Conditions</td>\r\n<td width=\"183\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td width=\"302\">All buses have AC, Head Rest and Seat belt</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Seat capacity is 15 to all destination</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td width=\"302\">Departure time is 6:30am 1st bus</td>\r\n<td>2nd bus departure time 7: 30am</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td width=\"302\">Boarding time is 6:00am</td>\r\n<td>Second bus boarding time 6:30am</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td width=\"302\">Passengers should be at the terminal at most thirty minute before boarding time</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td width=\"302\">Reports on ticket sold sent 24 hours before departure date</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td width=\"302\">Children between 0-7 is fee and 8 years and above pay same price as the tcket fare 100%</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>8</td>\r\n<td width=\"302\">Ticket booked and didn&rsquo;t travel is only valid for 3weeks</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'operator.png', '', 0, 1, NULL, '2013-07-29 12:43:42', '2017-01-25 16:58:30', '', 'aniekan_abasi.xlsx'),
(72, '', '', NULL, '', '', '', '', '', '', 0.00, NULL, 0, 0, '', '', '', '', 0, 0, NULL, '2016-12-15 09:59:03', '2016-12-15 09:59:03', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `operator_rules`
--

CREATE TABLE `operator_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(11) NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `operator_rules`
--

INSERT INTO `operator_rules` (`id`, `operator_id`, `rules`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'a:2:{s:6:\"infant\";a:2:{s:7:\"min_age\";i:0;s:7:\"max_age\";i:9;}s:8:\"discount\";a:3:{i:0;a:3:{s:7:\"min_age\";i:0;s:7:\"max_age\";i:3;s:19:\"discount_percentage\";i:100;}i:1;a:3:{s:7:\"min_age\";i:4;s:7:\"max_age\";i:6;s:19:\"discount_percentage\";i:10;}i:2;a:3:{s:7:\"min_age\";i:7;s:7:\"max_age\";i:9;s:19:\"discount_percentage\";i:5;}}}', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parks`
--

CREATE TABLE `parks` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nysc` int(11) DEFAULT '0',
  `chisco_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parks`
--

INSERT INTO `parks` (`id`, `state_id`, `name`, `address`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`, `nysc`, `chisco_id`) VALUES
(1, 1, 'Jibowu [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 1, '9a5d2d2d-42e6-4323-5395-08d525073f66'),
(2, 1, 'Maza-Maza [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, '8731adf0-6291-4407-b048-08d52508835f'),
(3, 1, 'Cele [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(4, 1, 'Ajah [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(5, 1, 'Ijesha [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(6, 1, 'Ogba [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(7, 1, 'Ojodu [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(8, 1, 'Alaba International [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(9, 1, 'Ikorodu [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(10, 1, 'Iyana Ipaja [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 1, NULL),
(11, 15, 'Owerri [in Imo] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(12, 15, 'Orlu [in Imo] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(13, 10, 'Ugep [in Cross River] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(14, 10, 'Ikom [in Cross River] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(15, 10, 'Ogoja [in Cross River] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(16, 13, 'Nsukka [in Enugu] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(17, 13, 'Holy ghost [in Enugu] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(18, 13, 'Oji [in Enugu] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(19, 13, 'Awgu [in Enugu] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(20, 35, 'Abakaliki [in Ebonyi] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(21, 35, 'Afikpo [in Ebonyi] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(22, 35, 'Okposi [in Ebonyi] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(23, 5, 'Ekwulobia [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(24, 5, 'Asaba [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(25, 5, 'Onitsha [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(26, 5, 'Nnewi [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(27, 5, 'Oboloafor [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(28, 5, 'Awka [in Anambra] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(29, 2, 'Umuahia [in Abia] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(30, 2, 'Aba [in Abia] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, '2e574dd8-f9a0-4a2b-5394-08d525073f66'),
(31, 29, 'Port Harcourt [in Rivers] ', '', 1, 1, NULL, '2016-11-01 12:40:20', '2016-11-01 12:40:20', 0, NULL),
(32, 1, 'Empire [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:40:37', '2016-11-01 12:40:37', 0, NULL),
(33, 4, 'Ikot Ekpene [in Akwa Ibom] ', '', 1, 1, NULL, '2016-11-01 12:40:37', '2016-11-01 12:40:37', 0, NULL),
(34, 4, 'Uyo [in Akwa Ibom] ', '', 1, 1, NULL, '2016-11-01 12:40:37', '2016-11-01 12:40:37', 0, NULL),
(35, 4, 'Oron [in Akwa Ibom] ', '', 1, 1, NULL, '2016-11-01 12:40:37', '2016-11-01 12:40:37', 0, NULL),
(36, 4, 'Eket [in Akwa Ibom] ', '', 1, 1, NULL, '2016-11-01 12:40:37', '2016-11-01 12:40:37', 0, NULL),
(37, 1, 'Iddo [in Lagos] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 1, NULL),
(38, 8, 'Makurdi [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(39, 8, 'Ugbokolo [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(40, 8, 'Zaki-Biam [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(41, 8, 'Katsina-Ala [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(42, 8, 'Otukpa [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(43, 8, 'Gboko [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(44, 8, 'Otukpo [in Benue] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(45, 31, 'Jalingo [in Taraba] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(46, 31, 'Wukari [in Taraba] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(47, 18, 'Angwa uku [in Kano] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(48, 23, 'Lafia [in Nasarawa] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(49, 3, 'Yola [in Adamawa] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(50, 18, 'Sabongeri [in Kano] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(51, 21, 'Ayingba [in Kogi] ', '', 1, 1, NULL, '2016-11-01 12:42:13', '2016-11-01 12:42:13', 0, NULL),
(52, 1, 'Ikotun [in Lagos] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(53, 1, 'Yaba [in Lagos] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 1, NULL),
(54, 6, 'Bauchi [in Bauchi] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(55, 10, 'Calabar [in Cross River] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(56, 27, 'Ibadan [in Oyo] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(57, 28, 'Jos [in Plateau] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(58, 17, 'Kaduna [in Kaduna] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(59, 18, 'Kano [in Kano] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(60, 25, 'Ota [in Ogun] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(61, 9, 'Maiduguri [in Borno] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(62, 30, 'Sokoto [in Sokoto] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(63, 8, 'Akwanga [in Benue] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(64, 11, 'Asaba [in Delta] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(65, 34, 'Utako [in Abuja] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(66, 39, 'Accra [in Accra] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, '1b093cf1-18f1-4b01-b047-08d52508835f'),
(67, 38, 'Cotonou [in Cotonou] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(68, 40, 'Lome [in Lome] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(69, 12, 'Benin [in Edo] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(70, 17, 'Zaria [in Kaduna] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(71, 11, 'Warri [in Delta] ', '', 1, 1, NULL, '2016-11-28 00:11:34', '2016-11-28 00:11:34', 0, NULL),
(72, 1, 'Iba [in Lagos] ', '', 1, 1, NULL, '2016-11-28 00:11:58', '2016-11-28 00:11:58', 0, NULL),
(73, 34, 'Kubwa [in Abuja] ', '', 1, 1, NULL, '2016-11-28 00:11:58', '2016-11-28 00:11:58', 0, NULL),
(74, 34, 'Jabi [in Abuja] ', '', 1, 1, NULL, '2016-11-28 00:11:58', '2016-11-28 00:11:58', 0, NULL),
(75, 13, 'Enugu [in Enugu] ', '', 1, 1, NULL, '2016-11-28 00:11:58', '2016-11-28 00:11:58', 0, NULL),
(76, 37, 'Ore [in Ondo] ', '', 1, 1, NULL, '2016-11-28 00:11:58', '2016-11-28 00:11:58', 0, NULL),
(79, 1, 'Okota [in Lagos]', '', 1, 1, NULL, '2017-06-14 11:16:16', '2017-06-14 11:19:27', 0, NULL),
(81, 11, 'Igbariam [in Delta]', '', 1, 1, NULL, '2017-07-21 08:07:26', '2017-07-21 08:07:26', 0, NULL),
(82, 13, '4 Corner [in Enugu]', '', 1, 1, NULL, '2017-07-21 08:08:22', '2017-07-21 08:08:22', 0, NULL),
(83, 13, '9th Mile [in Enugu]', '', 1, 1, NULL, '2017-07-21 08:08:54', '2017-07-21 08:08:54', 0, NULL),
(84, 2, 'Okigwe', '', 1, 1, NULL, '2017-07-21 08:09:14', '2017-07-21 08:09:14', 0, NULL),
(85, 15, 'Awo Junction [in Imo]', '', 1, 1, NULL, '2017-07-21 08:09:43', '2017-07-21 08:09:43', 0, NULL),
(86, 15, 'Mbano [in Imo]', '', 1, 1, NULL, '2017-07-21 08:10:08', '2017-07-21 08:10:08', 0, NULL),
(87, 15, 'Nkwere [in Imo]', '', 1, 1, NULL, '2017-07-21 08:10:36', '2017-07-21 08:10:36', 0, NULL),
(88, 15, 'Mbise [in Imo]', '', 1, 1, NULL, '2017-07-21 08:11:28', '2017-07-21 08:11:28', 0, NULL),
(89, 15, 'Ogbaku [in Imo]', '', 1, 1, NULL, '2017-07-21 08:11:56', '2017-07-21 08:11:56', 0, NULL),
(90, 21, 'Okene [in Kogi]', '', 1, 1, NULL, '2017-07-21 08:12:33', '2017-07-21 08:12:33', 0, NULL),
(91, 11, 'Agbor [in Delta]', '', 1, 1, NULL, '2017-07-21 08:12:59', '2017-07-21 08:12:59', 0, NULL),
(92, 5, 'Awkuzu [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:13:47', '2017-07-21 08:13:47', 0, NULL),
(93, 11, 'Sapele [in Delta]', '', 1, 1, NULL, '2017-07-21 08:14:14', '2017-07-21 08:14:14', 0, NULL),
(94, 7, 'Bayelsa [in Bayelsa]', '', 1, 1, NULL, '2017-07-21 08:14:43', '2017-07-21 08:14:43', 0, NULL),
(95, 5, 'Uga [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:15:17', '2017-07-21 08:15:17', 0, NULL),
(96, 5, 'Okija [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:16:11', '2017-07-21 08:16:11', 0, NULL),
(97, 15, 'Mbaise [in Imo]', '', 1, 1, NULL, '2017-07-21 08:16:44', '2017-07-21 08:16:44', 0, NULL),
(98, 12, 'Benin Bypass [in Edo]', '', 1, 1, NULL, '2017-07-21 08:17:15', '2017-07-21 08:17:15', 0, NULL),
(99, 5, 'Umuoji [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:17:44', '2017-07-21 08:17:44', 0, NULL),
(100, 5, 'Nkpor [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:18:10', '2017-07-21 08:18:10', 0, NULL),
(101, 5, 'Akokwa [in Anambra] ', '', 1, 1, NULL, '2017-07-21 08:18:49', '2017-07-21 08:18:49', 0, NULL),
(102, 41, 'Utako [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:19:58', '2017-07-21 08:19:58', 0, '8ef1d8f0-ac08-4515-b046-08d52508835f'),
(103, 41, 'Zuba [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:20:22', '2017-07-21 08:20:22', 0, NULL),
(104, 41, 'Rukpa [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:20:45', '2017-07-21 08:20:45', 0, NULL),
(105, 41, 'Nyanya [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:21:11', '2017-07-21 08:21:11', 0, NULL),
(106, 41, 'Mararaba [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:21:33', '2017-07-21 08:21:33', 0, NULL),
(107, 41, 'Gwarimpa [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:21:57', '2017-07-21 08:21:57', 0, NULL),
(108, 41, 'Gwagwalada', '', 1, 1, NULL, '2017-07-21 08:22:20', '2017-07-21 08:22:20', 0, NULL),
(109, 41, 'Deidei [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:22:45', '2017-07-21 08:22:45', 0, NULL),
(110, 41, 'Kubua [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:23:08', '2017-07-21 08:23:08', 0, NULL),
(111, 41, 'Area 3 [in Abuja]', '', 1, 1, NULL, '2017-07-21 08:23:31', '2017-07-21 08:23:31', 0, NULL),
(112, 21, 'Lokoja [in Kogi]', '', 1, 1, NULL, '2017-07-21 08:24:32', '2017-07-21 08:24:32', 0, NULL),
(113, 12, 'Ekpoma [in Edo]', '', 1, 1, NULL, '2017-07-21 08:25:00', '2017-07-21 08:25:00', 0, NULL),
(114, 12, 'Auchi [in Edo]', '', 1, 1, NULL, '2017-07-21 08:25:23', '2017-07-21 08:25:23', 0, NULL),
(115, 15, 'Mgbidi [in Imo]', '', 1, 1, NULL, '2017-07-21 08:25:56', '2017-07-21 08:25:56', 0, NULL),
(116, 5, 'Ihiala [in Anambra]', '', 1, 1, NULL, '2017-07-21 08:26:25', '2017-07-21 08:26:25', 0, NULL),
(117, 5, 'Oba Junction [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:08:25', '2017-07-21 09:08:25', 0, NULL),
(118, 5, 'Uli [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:09:33', '2017-07-21 09:09:33', 0, NULL),
(119, 5, 'Nnobi [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:10:34', '2017-07-21 09:10:34', 0, NULL),
(120, 5, 'Igboukwu [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:12:13', '2017-07-21 09:12:13', 0, NULL),
(121, 5, 'Awka-Etiti [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:13:45', '2017-07-21 09:13:45', 0, NULL),
(122, 5, 'Anara [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:14:22', '2017-07-21 09:14:22', 0, NULL),
(123, 5, 'Adazi [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:14:57', '2017-07-21 09:15:58', 0, NULL),
(124, 5, 'Agulu [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:17:59', '2017-07-21 09:17:59', 0, NULL),
(125, 5, 'Agulu [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:18:28', '2017-07-21 09:18:28', 0, NULL),
(126, 11, 'Ise Ile-uku [in Delta]', '', 1, 1, NULL, '2017-07-21 09:19:39', '2017-07-21 09:19:39', 0, NULL),
(127, 15, 'Umuaka [in Imo]', '', 1, 1, NULL, '2017-07-21 09:20:28', '2017-07-21 09:20:28', 0, NULL),
(128, 29, 'Elele [Rivers]', '', 1, 1, NULL, '2017-07-21 09:21:10', '2017-07-21 09:21:10', 0, NULL),
(129, 5, 'Oko [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:21:39', '2017-07-21 09:21:39', 0, NULL),
(130, 5, 'Umunze [in Anambra]', '', 1, 1, NULL, '2017-07-21 09:22:28', '2017-07-21 09:22:28', 0, NULL),
(133, 1, 'Ojuelegba [in Lagos]', '', 1, 1, NULL, '2017-07-24 12:00:02', '2017-07-24 12:00:02', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE `passengers` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seat` int(11) DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) DEFAULT NULL,
  `age_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Adult',
  `trip_id` int(11) DEFAULT NULL,
  `passport_no` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_type` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_date_of_issue` date DEFAULT NULL,
  `passport_expiry_date` date DEFAULT NULL,
  `passport_place_of_issue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sub_trip_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`id`, `booking_id`, `name`, `seat`, `gender`, `age`, `age_group`, `trip_id`, `passport_no`, `passport_type`, `passport_date_of_issue`, `passport_expiry_date`, `passport_place_of_issue`, `deleted_at`, `created_at`, `updated_at`, `sub_trip_id`) VALUES
(835, 798, 'Noibi Kazeem', 0, 'Male', NULL, 'Adult', 1408, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-24 06:24:15', NULL, 0),
(836, 799, 'Donkells Emmanuel ', 0, 'Male', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-24 16:34:24', NULL, 0),
(837, 800, 'Susan Ohaekelem', 0, 'Female', NULL, 'Adult', 1367, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25 02:39:44', NULL, 0),
(838, 801, 'Susan Ohaekelem', 0, 'Female', NULL, 'Adult', 1367, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25 02:41:20', NULL, 0),
(839, 802, 'Susan Ohaekelem', 0, 'Female', NULL, 'Adult', 1415, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25 03:16:23', NULL, 0),
(840, 803, 'chidoziem joseph Ekeocha', 0, 'Male', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-25 15:34:45', NULL, 0),
(841, 804, 'METU', 0, 'Female', NULL, 'Adult', 1433, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 15:05:17', NULL, 0),
(842, 805, 'Metu Anthonia chioma', 0, 'Female', NULL, 'Adult', 1433, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 15:08:01', NULL, 0),
(843, 806, 'Metu Anthonia chioma', 0, 'Female', NULL, 'Adult', 1433, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 15:39:02', NULL, 0),
(844, 807, 'Metu Anthonia chioma', 0, 'Female', NULL, 'Adult', 1433, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 15:50:08', NULL, 0),
(845, 808, 'Metu Anthonia chioma', 0, 'Female', NULL, 'Adult', 1433, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 16:08:02', NULL, 0),
(846, 809, 'Nmadu Sharon ', 0, 'Female', NULL, 'Adult', 1465, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 20:01:13', NULL, 0),
(847, 810, 'simonl loveth', 0, 'Female', NULL, 'Adult', 1408, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 22:12:17', NULL, 0),
(848, 811, 'simonl loveth', 0, 'Female', NULL, 'Adult', 1408, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-26 22:13:33', NULL, 0),
(849, 812, 'Onyejiaka Michael', 0, 'Male', NULL, 'Adult', 1467, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27 19:39:06', NULL, 0),
(850, 813, 'Onyejiaka Michael ', 0, 'Male', NULL, 'Adult', 1467, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27 19:49:53', NULL, 0),
(851, 814, 'Onyejiaka Michael ', 0, 'Male', NULL, 'Adult', 1467, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-27 19:51:12', NULL, 0),
(852, 815, 'Oko Chiedozie', 0, 'Male', NULL, 'Adult', 1434, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-28 23:36:58', NULL, 0),
(853, 816, 'JACOB IKECHUKWU', 0, 'Male', NULL, 'Adult', 1410, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-29 07:26:13', NULL, 0),
(854, 817, 'uchechukwu nuela ', 0, 'Female', NULL, 'Adult', 1431, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-29 17:19:30', NULL, 0),
(855, 818, 'Christiana eze', 0, 'Female', NULL, 'Adult', 1404, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-31 22:06:42', NULL, 0),
(856, 819, 'Christiana eze', 0, 'Female', NULL, 'Adult', 1404, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-31 22:22:20', NULL, 0),
(857, 820, 'Edward Jumbo ', 0, 'Male', NULL, 'Adult', 1368, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-01 15:42:30', NULL, 0),
(858, 821, 'Edward Jumbo ', 0, 'Male', NULL, 'Adult', 1368, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-01 15:43:08', NULL, 0),
(859, 822, 'vivian', 0, 'Female', NULL, 'Adult', 1432, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-03 09:25:15', NULL, 0),
(860, 823, 'chiamaka vivian omereonye', 0, 'Female', NULL, 'Adult', 1400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-03 09:29:03', NULL, 0),
(861, 824, 'chiamaka vivian omereonye', 0, 'Female', NULL, 'Adult', 1400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-03 09:29:25', NULL, 0),
(862, 825, 'Mgbakor George', 0, 'Male', NULL, 'Adult', 1434, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-04 02:16:55', NULL, 0),
(863, 826, 'jon', 0, 'Male', NULL, 'Adult', 1436, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-08 11:26:48', NULL, 0),
(864, 827, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 17:40:04', NULL, 0),
(865, 828, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 17:40:20', NULL, 0),
(866, 829, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 17:41:23', NULL, 0),
(867, 830, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 17:45:19', NULL, 0),
(868, 831, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 17:46:34', NULL, 0),
(869, 832, 'Segun', 0, 'Female', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 18:25:25', NULL, 0),
(870, 833, 'Segun', 0, '', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 23:01:21', NULL, 0),
(871, 834, 'Segun', 0, '', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 23:02:11', NULL, 0),
(872, 835, 'Segun', 0, '', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 23:08:24', NULL, 0),
(873, 836, 'Segun', 0, '', NULL, 'Adult', 1399, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 23:13:50', NULL, 0),
(874, 855, 'Segun olawhite', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 02:01:41', NULL, 0),
(875, 855, 'Segun olawhite', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 02:04:26', NULL, 0),
(876, 855, 'Segun olawhite', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 02:05:38', NULL, 0),
(877, 855, 'Segun', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 10:51:55', NULL, 0),
(878, 855, 'Segun', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 10:53:05', NULL, 0),
(879, 855, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 10:56:04', NULL, 0),
(880, 855, 'Segun', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:11:08', NULL, 0),
(881, 855, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:12:47', NULL, 0),
(882, 855, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:13:35', NULL, 0),
(883, 855, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:14:22', NULL, 0),
(884, 855, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:15:23', NULL, 0),
(885, 855, 'Olatunji', 0, '', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:16:34', NULL, 0),
(886, 855, 'Olatunji', 0, '', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:28:36', NULL, 0),
(887, 855, 'Olatunji', 0, '', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:37:13', NULL, 0),
(888, 855, 'Olatunji', 0, '', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:51:02', NULL, 0),
(889, 855, 'Olatunji', 0, '', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:52:51', NULL, 0),
(890, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:59:16', NULL, 0),
(891, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:12:52', NULL, 0),
(892, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:15:26', NULL, 0),
(893, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:20:36', NULL, 0),
(894, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:27:10', NULL, 0),
(895, 855, 'Segun', 0, 'Female', NULL, 'Adult', 1402, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:31:49', NULL, 0),
(896, 890, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 1404, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:36:07', NULL, 0),
(897, 891, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 1404, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 12:36:58', NULL, 0),
(898, 919, 'Olatunji', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 16:09:22', NULL, 0),
(899, 920, 'Olatunji', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 16:10:27', NULL, 0),
(900, 921, 'Olatunji', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 16:15:14', NULL, 0),
(901, 922, 'Segun owalata', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 16:20:28', NULL, 0),
(902, 923, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:27:37', NULL, 0),
(903, 924, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:28:35', NULL, 0),
(904, 925, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:29:52', NULL, 0),
(905, 926, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:30:12', NULL, 0),
(906, 927, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:32:39', NULL, 0),
(907, 928, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:34:40', NULL, 0),
(908, 929, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 00:36:26', NULL, 0),
(909, 930, 'Segun olawhite', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 05:56:05', NULL, 0),
(910, 931, 'Olatunji', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 06:03:28', NULL, 0),
(911, 932, 'Olatunji', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 06:04:08', NULL, 0),
(912, 933, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:18:36', NULL, 0),
(913, 934, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:19:18', NULL, 0),
(914, 935, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:25:37', NULL, 0),
(915, 936, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:26:38', NULL, 0),
(916, 937, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:27:31', NULL, 0),
(917, 938, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:29:11', NULL, 0),
(918, 939, 'Kazeem', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 07:31:14', NULL, 0),
(919, 940, 'Olatunji', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 09:44:49', NULL, 0),
(920, 941, 'Olatunji', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 09:53:35', NULL, 0),
(921, 942, 'Olatunji', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 09:55:54', NULL, 0),
(922, 943, 'Segun', 0, 'Female', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 09:57:14', NULL, 0),
(923, 944, 'Segun', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:21:02', NULL, 0),
(924, 945, 'Segun', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:21:28', NULL, 0),
(925, 946, 'Segun', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:22:00', NULL, 0),
(926, 947, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:36:46', NULL, 0),
(927, 948, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:37:36', NULL, 0),
(928, 949, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:40:29', NULL, 0),
(929, 950, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:41:09', NULL, 0),
(930, 951, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 10:47:36', NULL, 0),
(931, 952, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 12:22:05', NULL, 0),
(932, 953, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 12:58:22', NULL, 0),
(933, 954, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 12:59:39', NULL, 0),
(934, 955, 'Olatunji', 0, 'Female', NULL, 'Adult', 1405, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 13:03:24', NULL, 0),
(935, 956, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 13:12:53', NULL, 0),
(936, 957, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:10:06', NULL, 0),
(937, 958, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:33:02', NULL, 0),
(938, 959, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:33:30', NULL, 0),
(939, 960, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:34:55', NULL, 0),
(940, 961, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:35:12', NULL, 0),
(941, 962, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:38:34', NULL, 0),
(942, 963, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:45:23', NULL, 0),
(943, 964, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:45:56', NULL, 0),
(944, 965, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:46:43', NULL, 0),
(945, 966, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:47:27', NULL, 0),
(946, 967, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:48:33', NULL, 0),
(947, 968, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:48:55', NULL, 0),
(948, 969, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:49:14', NULL, 0),
(949, 970, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:50:24', NULL, 0),
(950, 971, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:51:32', NULL, 0),
(951, 972, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:51:44', NULL, 0),
(952, 973, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:52:19', NULL, 0),
(953, 974, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:52:43', NULL, 0),
(954, 975, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:54:44', NULL, 0),
(955, 976, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:55:03', NULL, 0),
(956, 977, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:56:17', NULL, 0),
(957, 978, 'Ajayi', 0, 'Male', NULL, 'Adult', 400, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 14:56:55', NULL, 0),
(958, 979, 'Segun', 0, 'Female', NULL, 'Adult', 22, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 16:13:53', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `description`, `active`, `bank_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Bank', 'Bank Deposit', 1, NULL, NULL, NULL, NULL),
(2, 'PayStack', 'Online Payment', 1, NULL, NULL, NULL, NULL),
(3, 'Zenith Bank', 'Payment through Zenith Mobile Banking', 1, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `queue_jobs`
--

CREATE TABLE `queue_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Super User', '', '2016-02-09 08:00:00', NULL, NULL),
(2, 'Operator', '', NULL, '2016-11-25 13:17:13', '2016-11-25 13:17:13');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `id` int(10) UNSIGNED NOT NULL,
  `seat_no` int(11) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `departure_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sub_trip_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`id`, `seat_no`, `trip_id`, `booking_id`, `departure_date`, `created_at`, `updated_at`, `sub_trip_id`) VALUES
(1, 11, 3147, 1, '2016-11-01 00:00:00', '2016-11-01 14:47:32', '2016-11-01 14:47:32', 0),
(2, 15, 3147, 1, '2016-11-01 00:00:00', '2016-11-01 14:47:32', '2016-11-01 14:47:32', 0),
(3, 1, 3292, 2, '2016-11-03 00:00:00', '2016-11-01 15:45:29', '2016-11-01 15:45:29', 0),
(4, 1, 3169, 3, '2016-11-03 00:00:00', '2016-11-01 15:47:35', '2016-11-01 15:47:35', 0),
(5, 8, 3210, 4, '2016-11-02 00:00:00', '2016-11-01 15:54:43', '2016-11-01 15:54:43', 0),
(6, 7, 3210, 4, '2016-11-02 00:00:00', '2016-11-01 15:54:43', '2016-11-01 15:54:43', 0),
(7, 11, 3147, 5, '2016-11-02 00:00:00', '2016-11-01 15:57:45', '2016-11-01 15:57:45', 0),
(8, 3, 3252, 6, '2016-11-02 00:00:00', '2016-11-01 22:29:11', '2016-11-01 22:29:11', 0),
(9, 4, 3252, 6, '2016-11-02 00:00:00', '2016-11-01 22:29:11', '2016-11-01 22:29:11', 0),
(10, 13, 3372, 7, '2016-11-02 00:00:00', '2016-11-02 13:58:58', '2016-11-02 13:58:58', 0),
(11, 8, 3238, 8, '2016-11-04 00:00:00', '2016-11-03 09:23:26', '2016-11-03 09:23:26', 0),
(12, 5, 3238, 8, '2016-11-04 00:00:00', '2016-11-03 09:23:26', '2016-11-03 09:23:26', 0),
(13, 5, 3252, 11, '2016-11-05 00:00:00', '2016-11-04 16:03:48', '2016-11-04 16:03:48', 0),
(14, 3, 3203, 13, '2016-11-05 00:00:00', '2016-11-04 23:03:32', '2016-11-04 23:03:32', 0),
(15, 6, 3147, 14, '2016-11-05 00:00:00', '2016-11-05 18:32:51', '2016-11-05 18:32:51', 0),
(16, 2, 3388, 15, '0000-00-00 00:00:00', '2016-11-06 06:04:48', '2016-11-06 06:04:48', 0),
(17, 5, 3244, 17, '2016-11-07 00:00:00', '2016-11-07 12:22:35', '2016-11-07 12:22:35', 0),
(18, 1, 3368, 18, '0000-00-00 00:00:00', '2016-11-08 09:28:42', '2016-11-08 09:28:42', 0),
(19, 11, 3168, 19, '2016-11-10 00:00:00', '2016-11-09 10:35:28', '2016-11-09 10:35:28', 0),
(20, 5, 3296, 20, '2016-11-10 00:00:00', '2016-11-09 13:32:52', '2016-11-09 13:32:52', 0),
(21, 5, 3296, 21, '2016-11-10 00:00:00', '2016-11-09 13:34:15', '2016-11-09 13:34:15', 0),
(22, 2, 3273, 22, '2016-11-12 00:00:00', '2016-11-09 16:52:26', '2016-11-09 16:52:26', 0),
(23, 2, 3238, 26, '2016-11-15 00:00:00', '2016-11-14 15:40:06', '2016-11-14 15:40:06', 0),
(24, 5, 3238, 27, '2016-11-15 00:00:00', '2016-11-14 15:51:56', '2016-11-14 15:51:56', 0),
(25, 10, 3302, 28, '2016-11-14 00:00:00', '2016-11-14 22:40:21', '2016-11-14 22:40:21', 0),
(26, 3, 3228, 12, '2016-11-25 12:18:57', '2016-11-19 09:33:31', '2016-11-19 09:33:31', 0),
(27, 7, 3238, 31, '2016-11-21 00:00:00', '2016-11-20 11:44:10', '2016-11-20 11:44:10', 0),
(28, 11, 3238, 31, '2016-11-21 00:00:00', '2016-11-20 11:44:10', '2016-11-20 11:44:10', 0),
(29, 11, 3147, 33, '2016-11-25 12:18:55', '2016-11-24 18:06:59', '2016-11-24 18:06:59', 0),
(30, 15, 499, 5, '2016-11-28 00:00:00', '2016-11-28 07:54:40', '2016-11-28 07:54:40', 0),
(31, 14, 499, 7, '2016-11-28 00:00:00', '2016-11-28 07:56:19', '2016-11-28 07:56:19', 0),
(32, 12, 616, 8, '2016-11-28 00:00:00', '2016-11-28 11:56:40', '2016-11-28 11:56:40', 0),
(33, 3, 922, 10, '2016-11-30 00:00:00', '2016-11-30 12:38:55', '2016-11-30 12:38:55', 0),
(34, 8, 371, 15, '2016-12-02 00:00:00', '2016-12-02 13:26:18', '2016-12-02 13:26:18', 0),
(35, 7, 371, 15, '2016-12-02 00:00:00', '2016-12-02 13:26:18', '2016-12-02 13:26:18', 0),
(36, 5, 371, 16, '2016-12-02 00:00:00', '2016-12-02 13:28:54', '2016-12-02 13:28:54', 0),
(37, 4, 371, 16, '2016-12-02 00:00:00', '2016-12-02 13:28:55', '2016-12-02 13:28:55', 0),
(38, 5, 924, 17, '2016-12-02 00:00:00', '2016-12-02 13:51:29', '2016-12-02 13:51:29', 0),
(39, 4, 924, 17, '2016-12-02 00:00:00', '2016-12-02 13:51:29', '2016-12-02 13:51:29', 0),
(40, 3, 924, 17, '2016-12-02 00:00:00', '2016-12-02 13:51:29', '2016-12-02 13:51:29', 0),
(41, 2, 889, 18, '2016-12-03 00:00:00', '2016-12-02 14:12:52', '2016-12-02 14:12:52', 0),
(42, 9, 922, 19, '2016-12-03 00:00:00', '2016-12-02 14:23:35', '2016-12-02 14:23:35', 0),
(43, 10, 922, 19, '2016-12-03 00:00:00', '2016-12-02 14:23:35', '2016-12-02 14:23:35', 0),
(44, 3, 457, 21, '2016-10-12 00:00:00', '2016-12-03 23:25:24', '2016-12-03 23:25:24', 0),
(45, 8, 425, 22, '2016-12-05 00:00:00', '2016-12-04 15:39:01', '2016-12-04 15:39:01', 0),
(46, 5, 446, 24, '2016-12-08 00:00:00', '2016-12-05 13:59:23', '2016-12-05 13:59:23', 0),
(47, 9, 1310, 25, '2016-12-07 00:00:00', '2016-12-05 18:22:35', '2016-12-05 18:22:35', 0),
(48, 8, 1030, 27, '2016-12-07 00:00:00', '2016-12-06 11:04:00', '2016-12-06 11:04:00', 0),
(49, 3, 85, 28, '2016-12-07 00:00:00', '2016-12-06 13:28:09', '2016-12-06 13:28:09', 0),
(50, 4, 85, 28, '2016-12-07 00:00:00', '2016-12-06 13:28:09', '2016-12-06 13:28:09', 0),
(51, 5, 85, 28, '2016-12-07 00:00:00', '2016-12-06 13:28:09', '2016-12-06 13:28:09', 0),
(52, 1, 984, 29, '2016-12-06 00:00:00', '2016-12-06 13:37:08', '2016-12-06 13:37:08', 0),
(53, 2, 407, 30, '2016-12-06 00:00:00', '2016-12-06 15:21:20', '2016-12-06 15:21:20', 0),
(54, 6, 26, 31, '2016-12-08 00:00:00', '2016-12-07 07:14:19', '2016-12-07 07:14:19', 0),
(55, 5, 311, 32, '2016-12-11 00:00:00', '2016-12-07 09:04:00', '2016-12-07 09:04:00', 0),
(56, 7, 862, 35, '2016-12-22 00:00:00', '2016-12-08 12:22:33', '2016-12-08 12:22:33', 0),
(57, 12, 421, 44, '2016-12-11 00:00:00', '2016-12-09 12:20:06', '2016-12-09 12:20:06', 0),
(58, 1, 984, 48, '2016-12-09 00:00:00', '2016-12-09 15:16:41', '2016-12-09 15:16:41', 0),
(59, 10, 419, 50, '2016-12-10 00:00:00', '2016-12-10 11:56:18', '2016-12-10 11:56:18', 0),
(60, 12, 419, 50, '2016-12-10 00:00:00', '2016-12-10 11:56:18', '2016-12-10 11:56:18', 0),
(61, 5, 911, 51, '2016-12-12 00:00:00', '2016-12-10 21:27:28', '2016-12-10 21:27:28', 0),
(62, 15, 894, 52, '2016-12-11 00:00:00', '2016-12-11 13:18:35', '2016-12-11 13:18:35', 0),
(63, 3, 184, 54, '2016-12-23 00:00:00', '2016-12-12 13:22:34', '2016-12-12 13:22:34', 0),
(64, 11, 799, 71, '2016-12-15 00:00:00', '2016-12-14 20:40:44', '2016-12-14 20:40:44', 0),
(65, 11, 799, 72, '2016-12-15 00:00:00', '2016-12-14 20:52:15', '2016-12-14 20:52:15', 0),
(66, 11, 799, 73, '2016-12-15 00:00:00', '2016-12-14 20:52:40', '2016-12-14 20:52:40', 0),
(67, 11, 799, 74, '2016-12-15 00:00:00', '2016-12-14 20:52:48', '2016-12-14 20:52:48', 0),
(68, 11, 799, 75, '2016-12-15 00:00:00', '2016-12-14 20:56:43', '2016-12-14 20:56:43', 0),
(69, 6, 1054, 77, '2016-12-15 00:00:00', '2016-12-15 01:22:54', '2016-12-15 01:22:54', 0),
(70, 7, 1054, 77, '2016-12-15 00:00:00', '2016-12-15 01:22:54', '2016-12-15 01:22:54', 0),
(71, 5, 984, 78, '2016-12-16 00:00:00', '2016-12-15 11:03:09', '2016-12-15 11:03:09', 0),
(72, 13, 127, 79, '2016-12-16 00:00:00', '2016-12-15 12:11:41', '2016-12-15 12:11:41', 0),
(73, 11, 293, 80, '2016-12-16 00:00:00', '2016-12-16 10:38:45', '2016-12-16 10:38:45', 0),
(74, 8, 443, 81, '0000-00-00 00:00:00', '2016-12-16 11:38:14', '2016-12-16 11:38:14', 0),
(75, 1, 57, 82, '2016-12-17 00:00:00', '2016-12-16 12:47:02', '2016-12-16 12:47:02', 0),
(76, 3, 198, 86, '2016-12-19 00:00:00', '2016-12-16 19:17:40', '2016-12-16 19:17:40', 0),
(77, 6, 513, 90, '2016-12-19 00:00:00', '2016-12-17 16:24:45', '2016-12-17 16:24:45', 0),
(78, 3, 668, 102, '2016-12-23 00:00:00', '2016-12-20 17:53:30', '2016-12-20 17:53:30', 0),
(79, 6, 457, 103, '2016-12-21 00:00:00', '2016-12-20 20:35:46', '2016-12-20 20:35:46', 0),
(80, 6, 457, 104, '2016-12-21 00:00:00', '2016-12-20 20:41:37', '2016-12-20 20:41:37', 0),
(81, 9, 457, 105, '2016-12-23 00:00:00', '2016-12-21 14:26:15', '2016-12-21 14:26:15', 0),
(82, 10, 457, 105, '2016-12-23 00:00:00', '2016-12-21 14:26:15', '2016-12-21 14:26:15', 0),
(83, 6, 457, 107, '2016-12-23 00:00:00', '2016-12-21 14:29:02', '2016-12-21 14:29:02', 0),
(84, 6, 1040, 108, '2016-12-23 00:00:00', '2016-12-21 15:01:24', '2016-12-21 15:01:24', 0),
(85, 9, 113, 114, '2016-12-22 00:00:00', '2016-12-21 18:03:07', '2016-12-21 18:03:07', 0),
(86, 12, 113, 115, '2016-12-22 00:00:00', '2016-12-21 18:17:29', '2016-12-21 18:17:29', 0),
(87, 9, 85, 117, '2016-12-21 00:00:00', '2016-12-21 18:36:46', '2016-12-21 18:36:46', 0),
(88, 9, 1606, 119, '2016-12-23 00:00:00', '2016-12-22 18:15:51', '2016-12-22 18:15:51', 0),
(89, 12, 1606, 120, '2016-12-23 00:00:00', '2016-12-22 18:33:51', '2016-12-22 18:33:51', 0),
(90, 13, 1606, 121, '2016-12-23 00:00:00', '2016-12-22 18:46:42', '2016-12-22 18:46:42', 0),
(91, 2, 499, 123, '2016-12-24 00:00:00', '2016-12-22 23:23:44', '2016-12-22 23:23:44', 0),
(92, 5, 239, 124, '2016-12-23 00:00:00', '2016-12-23 02:57:57', '2016-12-23 02:57:57', 0),
(93, 5, 239, 125, '2016-12-23 00:00:00', '2016-12-23 02:59:23', '2016-12-23 02:59:23', 0),
(94, 14, 58, 127, '2016-12-23 00:00:00', '2016-12-23 13:47:06', '2016-12-23 13:47:06', 0),
(95, 6, 63, 128, '2016-12-24 00:00:00', '2016-12-23 17:54:12', '2016-12-23 17:54:12', 0),
(96, 5, 201, 129, '2016-12-25 00:00:00', '2016-12-24 06:24:51', '2016-12-24 06:24:51', 0),
(97, 2, 47, 130, '2016-12-29 00:00:00', '2016-12-24 18:12:30', '2016-12-24 18:12:30', 0),
(98, 2, 289, 132, '2016-12-24 00:00:00', '2016-12-24 19:41:29', '2016-12-24 19:41:29', 0),
(99, 2, 289, 133, '2016-12-24 00:00:00', '2016-12-24 19:41:30', '2016-12-24 19:41:30', 0),
(100, 2, 729, 136, '2016-12-28 00:00:00', '2016-12-27 08:17:32', '2016-12-27 08:17:32', 0),
(101, 11, 143, 140, '2016-12-29 00:00:00', '2016-12-29 00:10:38', '2016-12-29 00:10:38', 0),
(102, 10, 17, 141, '2016-01-17 00:00:00', '2016-12-30 14:24:38', '2016-12-30 14:24:38', 0),
(103, 9, 513, 142, '2016-01-13 00:00:00', '2016-12-30 20:53:47', '2016-12-30 20:53:47', 0),
(104, 14, 706, 143, '2017-01-04 00:00:00', '2017-01-04 12:49:30', '2017-01-04 12:49:30', 0),
(105, 5, 113, 144, '0000-00-00 00:00:00', '2017-01-04 13:10:09', '2017-01-04 13:10:09', 0),
(106, 15, 297, 145, '2017-01-05 00:00:00', '2017-01-04 13:22:23', '2017-01-04 13:22:23', 0),
(107, 14, 297, 145, '2017-01-05 00:00:00', '2017-01-04 13:22:23', '2017-01-04 13:22:23', 0),
(108, 15, 139, 146, '2017-01-04 00:00:00', '2017-01-04 16:12:35', '2017-01-04 16:12:35', 0),
(109, 7, 251, 147, '2017-01-04 00:00:00', '2017-01-04 16:18:27', '2017-01-04 16:18:27', 0),
(110, 6, 620, 148, '2017-01-05 00:00:00', '2017-01-04 16:46:45', '2017-01-04 16:46:45', 0),
(111, 15, 169, 149, '2017-01-04 00:00:00', '2017-01-04 17:52:32', '2017-01-04 17:52:32', 0),
(112, 5, 169, 151, '2017-01-05 00:00:00', '2017-01-05 10:08:42', '2017-01-05 10:08:42', 0),
(113, 8, 57, 152, '2017-01-06 00:00:00', '2017-01-05 10:29:56', '2017-01-05 10:29:56', 0),
(114, 12, 57, 153, '2017-01-05 00:00:00', '2017-01-05 14:25:53', '2017-01-05 14:25:53', 0),
(115, 11, 57, 165, '2017-01-05 00:00:00', '2017-01-05 15:13:52', '2017-01-05 15:13:52', 0),
(116, 3, 936, 166, '2017-01-07 00:00:00', '2017-01-06 12:45:31', '2017-01-06 12:45:31', 0),
(117, 1, 113, 167, '2017-01-08 00:00:00', '2017-01-06 13:59:26', '2017-01-06 13:59:26', 0),
(118, 1, 173, 168, '2017-01-07 00:00:00', '2017-01-06 14:06:09', '2017-01-06 14:06:09', 0),
(119, 1, 451, 169, '2017-01-06 00:00:00', '2017-01-06 14:11:04', '2017-01-06 14:11:04', 0),
(120, 6, 489, 170, '2017-01-07 00:00:00', '2017-01-06 14:19:31', '2017-01-06 14:19:31', 0),
(121, 3, 455, 171, '2017-01-08 00:00:00', '2017-01-06 20:33:18', '2017-01-06 20:33:18', 0),
(122, 6, 548, 172, '2017-01-07 00:00:00', '2017-01-06 22:28:07', '2017-01-06 22:28:07', 0),
(123, 7, 548, 172, '2017-01-07 00:00:00', '2017-01-06 22:28:07', '2017-01-06 22:28:07', 0),
(124, 8, 548, 172, '2017-01-07 00:00:00', '2017-01-06 22:28:07', '2017-01-06 22:28:07', 0),
(125, 6, 620, 173, '2017-01-07 00:00:00', '2017-01-06 22:45:37', '2017-01-06 22:45:37', 0),
(126, 7, 620, 173, '2017-01-07 00:00:00', '2017-01-06 22:45:37', '2017-01-06 22:45:37', 0),
(127, 8, 620, 173, '2017-01-07 00:00:00', '2017-01-06 22:45:37', '2017-01-06 22:45:37', 0),
(128, 1, 57, 174, '2017-01-07 00:00:00', '2017-01-07 00:14:02', '2017-01-07 00:14:02', 0),
(129, 12, 984, 177, '2017-01-09 00:00:00', '2017-01-09 13:28:13', '2017-01-09 13:28:13', 0),
(130, 3, 429, 180, '2017-01-11 00:00:00', '2017-01-10 11:59:01', '2017-01-10 11:59:01', 0),
(131, 6, 429, 182, '2017-01-11 00:00:00', '2017-01-10 15:08:15', '2017-01-10 15:08:15', 0),
(132, 2, 652, 183, '2017-01-13 00:00:00', '2017-01-11 11:30:57', '2017-01-11 11:30:57', 0),
(133, 5, 910, 184, '2017-01-13 00:00:00', '2017-01-12 15:16:06', '2017-01-12 15:16:06', 0),
(134, 15, 113, 186, '0000-00-00 00:00:00', '2017-01-16 16:05:15', '2017-01-16 16:05:15', 0),
(135, 15, 169, 187, '0000-00-00 00:00:00', '2017-01-16 16:14:10', '2017-01-16 16:14:10', 0),
(136, 15, 211, 188, '0000-00-00 00:00:00', '2017-01-16 16:17:58', '2017-01-16 16:17:58', 0),
(137, 15, 57, 190, '0000-00-00 00:00:00', '2017-01-16 16:36:41', '2017-01-16 16:36:41', 0),
(138, 15, 43, 191, '2017-01-18 00:00:00', '2017-01-17 14:33:27', '2017-01-17 14:33:27', 0),
(139, 9, 696, 192, '2017-01-21 00:00:00', '2017-01-20 09:29:52', '2017-01-20 09:29:52', 0),
(140, 10, 826, 193, '2017-01-22 00:00:00', '2017-01-21 14:14:09', '2017-01-21 14:14:09', 0),
(141, 3, 5, 196, '2017-01-24 00:00:00', '2017-01-23 11:36:00', '2017-01-23 11:36:00', 0),
(142, 6, 915, 197, '2017-01-24 00:00:00', '2017-01-23 11:45:15', '2017-01-23 11:45:15', 0),
(143, 7, 915, 198, '2017-01-24 00:00:00', '2017-01-23 11:50:49', '2017-01-23 11:50:49', 0),
(144, 8, 915, 199, '2017-01-24 00:00:00', '2017-01-23 11:52:33', '2017-01-23 11:52:33', 0),
(145, 11, 915, 200, '2017-01-24 00:00:00', '2017-01-23 12:01:00', '2017-01-23 12:01:00', 0),
(146, 10, 915, 201, '2017-01-24 00:00:00', '2017-01-23 12:07:31', '2017-01-23 12:07:31', 0),
(147, 9, 915, 202, '2017-01-24 00:00:00', '2017-01-23 13:17:17', '2017-01-23 13:17:17', 0),
(148, 12, 915, 203, '2017-01-24 00:00:00', '2017-01-23 13:24:06', '2017-01-23 13:24:06', 0),
(149, 13, 915, 204, '2017-01-24 00:00:00', '2017-01-23 13:25:09', '2017-01-23 13:25:09', 0),
(150, 13, 915, 205, '2017-01-24 00:00:00', '2017-01-23 13:25:55', '2017-01-23 13:25:55', 0),
(151, 14, 915, 206, '2017-01-24 00:00:00', '2017-01-23 13:33:37', '2017-01-23 13:33:37', 0),
(152, 15, 915, 208, '2017-01-24 00:00:00', '2017-01-23 13:40:18', '2017-01-23 13:40:18', 0),
(153, 6, 341, 209, '2017-01-24 00:00:00', '2017-01-23 18:25:47', '2017-01-23 18:25:47', 0),
(154, 7, 433, 210, '2017-01-26 00:00:00', '2017-01-25 13:31:42', '2017-01-25 13:31:42', 0),
(155, 8, 433, 211, '2017-01-26 00:00:00', '2017-01-25 13:38:41', '2017-01-25 13:38:41', 0),
(156, 6, 433, 212, '2017-01-26 00:00:00', '2017-01-25 13:39:26', '2017-01-25 13:39:26', 0),
(157, 11, 433, 213, '2017-01-26 00:00:00', '2017-01-25 13:54:35', '2017-01-25 13:54:35', 0),
(158, 12, 915, 214, '0000-00-00 00:00:00', '2017-01-25 16:21:27', '2017-01-25 16:21:27', 0),
(159, 5, 353, 215, '2017-01-26 00:00:00', '2017-01-25 16:54:56', '2017-01-25 16:54:56', 0),
(160, 4, 353, 215, '2017-01-26 00:00:00', '2017-01-25 16:54:56', '2017-01-25 16:54:56', 0),
(161, 9, 915, 216, '2017-01-27 00:00:00', '2017-01-26 11:54:32', '2017-01-26 11:54:32', 0),
(162, 11, 915, 217, '2017-01-27 00:00:00', '2017-01-26 12:12:50', '2017-01-26 12:12:50', 0),
(163, 10, 915, 217, '2017-01-27 00:00:00', '2017-01-26 12:12:50', '2017-01-26 12:12:50', 0),
(164, 7, 915, 217, '2017-01-27 00:00:00', '2017-01-26 12:12:50', '2017-01-26 12:12:50', 0),
(165, 14, 915, 218, '2017-01-27 00:00:00', '2017-01-26 12:21:32', '2017-01-26 12:21:32', 0),
(166, 15, 915, 218, '2017-01-27 00:00:00', '2017-01-26 12:21:32', '2017-01-26 12:21:32', 0),
(167, 13, 915, 219, '2017-01-27 00:00:00', '2017-01-26 12:56:12', '2017-01-26 12:56:12', 0),
(168, 12, 915, 219, '2017-01-27 00:00:00', '2017-01-26 12:56:12', '2017-01-26 12:56:12', 0),
(169, 6, 915, 226, '2017-01-27 00:00:00', '2017-01-26 13:24:51', '2017-01-26 13:24:51', 0),
(170, 6, 915, 227, '2017-01-27 00:00:00', '2017-01-26 13:26:11', '2017-01-26 13:26:11', 0),
(171, 6, 915, 228, '2017-01-27 00:00:00', '2017-01-26 13:31:37', '2017-01-26 13:31:37', 0),
(172, 8, 915, 229, '2017-01-27 00:00:00', '2017-01-26 13:34:11', '2017-01-26 13:34:11', 0),
(173, 3, 915, 229, '2017-01-27 00:00:00', '2017-01-26 13:34:11', '2017-01-26 13:34:11', 0),
(174, 5, 915, 230, '2017-01-27 00:00:00', '2017-01-26 13:35:28', '2017-01-26 13:35:28', 0),
(175, 2, 154, 231, '2017-01-27 00:00:00', '2017-01-26 15:42:28', '2017-01-26 15:42:28', 0),
(176, 6, 3, 232, '2017-01-31 00:00:00', '2017-01-30 11:36:10', '2017-01-30 11:36:10', 0),
(177, 6, 782, 233, '2017-01-31 00:00:00', '2017-01-30 13:06:55', '2017-01-30 13:06:55', 0),
(178, 14, 164, 234, '2017-02-03 00:00:00', '2017-02-02 11:53:50', '2017-02-02 11:53:50', 0),
(179, 15, 164, 234, '2017-02-03 00:00:00', '2017-02-02 11:53:50', '2017-02-02 11:53:50', 0),
(180, 9, 269, 235, '2017-02-03 00:00:00', '2017-02-02 12:55:45', '2017-02-02 12:55:45', 0),
(181, 9, 269, 236, '2017-02-03 00:00:00', '2017-02-02 13:58:41', '2017-02-02 13:58:41', 0),
(182, 10, 269, 237, '2017-02-03 00:00:00', '2017-02-02 14:03:22', '2017-02-02 14:03:22', 0),
(183, 5, 144, 238, '2017-02-03 00:00:00', '2017-02-02 14:25:12', '2017-02-02 14:25:12', 0),
(184, 13, 269, 239, '2017-02-03 00:00:00', '2017-02-02 14:40:28', '2017-02-02 14:40:28', 0),
(185, 11, 269, 240, '2017-02-03 00:00:00', '2017-02-02 14:59:03', '2017-02-02 14:59:03', 0),
(186, 14, 269, 241, '2017-02-03 00:00:00', '2017-02-02 15:04:19', '2017-02-02 15:04:19', 0),
(187, 1, 616, 242, '2017-02-04 00:00:00', '2017-02-03 07:21:57', '2017-02-03 07:21:57', 0),
(188, 2, 616, 242, '2017-02-04 00:00:00', '2017-02-03 07:21:57', '2017-02-03 07:21:57', 0),
(189, 5, 154, 243, '2017-02-04 00:00:00', '2017-02-03 12:05:30', '2017-02-03 12:05:30', 0),
(190, 8, 28, 244, '2017-02-10 00:00:00', '2017-02-09 09:19:58', '2017-02-09 09:19:58', 0),
(191, 11, 154, 245, '2017-02-10 00:00:00', '2017-02-09 13:56:53', '2017-02-09 13:56:53', 0),
(192, 8, 269, 246, '2017-02-10 00:00:00', '2017-02-09 15:12:24', '2017-02-09 15:12:24', 0),
(193, 5, 328, 247, '2017-02-12 00:00:00', '2017-02-11 12:51:29', '2017-02-11 12:51:29', 0),
(194, 15, 269, 249, '2017-02-17 00:00:00', '2017-02-16 14:26:52', '2017-02-16 14:26:52', 0),
(195, 1, 221, 250, '2017-02-19 00:00:00', '2017-02-18 20:49:17', '2017-02-18 20:49:17', 0),
(196, 15, 648, 251, '2017-02-20 00:00:00', '2017-02-19 14:49:33', '2017-02-19 14:49:33', 0),
(197, 15, 13, 252, '2017-02-21 00:00:00', '2017-02-20 15:02:56', '2017-02-20 15:02:56', 0),
(198, 12, 1, 195, '2016-11-01 00:00:00', '2017-07-04 17:29:34', NULL, 0),
(199, 12, 1, 195, '2016-11-01 00:00:00', '2017-07-05 18:33:28', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `exposed` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`id`, `customer_id`, `provider_user_id`, `provider`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '10210531500622451', 'facebook', NULL, '2016-09-21 10:09:23', '2016-09-21 10:09:23'),
(2, 5, '114429332373312139821', 'google', NULL, '2016-10-26 10:02:00', '2016-10-26 10:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `region`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Lagos', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-06-12 16:53:03'),
(2, 'Abia', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(3, 'Adamawa', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(4, 'Akwa Ibom', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(5, 'Anambra', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(6, 'Bauchi', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(7, 'Bayelsa', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(8, 'Benue', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(9, 'Borno', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(10, 'Cross River', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(11, 'Delta', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(12, 'Edo', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(13, 'Enugu', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(14, 'Gombe', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(15, 'Imo', 'Nigeria', 1, 0, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(16, 'Jigawa', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(17, 'Kaduna', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(18, 'Kano', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(19, 'Katsina', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(20, 'Kebbi', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(21, 'Kogi', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(22, 'Kwara', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(23, 'Nasarawa', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(24, 'Niger', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(25, 'Ogun', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(26, 'Osun', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(27, 'Oyo', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(28, 'Plateau', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(29, 'Rivers', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(30, 'Sokoto', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(31, 'Taraba', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(32, 'Yobe', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(33, 'Zamfara', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(34, 'Abuja', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(35, 'Ebonyi', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(36, 'Ekiti', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(37, 'Ondo', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(38, 'Cotonou', 'Republic of Benin', 0, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(39, 'Accra', 'Ghana', 1, 0, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(40, 'Lome', 'Togo', 1, 0, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03'),
(41, 'Abuja', 'Nigeria', 1, 1, NULL, '2016-03-02 16:53:03', '2016-03-02 16:53:03');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `booking_code` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `booking_id`, `booking_code`, `status`, `response`, `created_at`, `updated_at`) VALUES
(962, 798, 'OKY765815', 'Transaction Not Found', 'Failed', '2017-12-24 07:24:20', NULL),
(963, 798, 'OKY765815', 'Transaction cancelled by user', 'Cancelled', '2017-12-24 07:31:03', NULL),
(964, 798, 'OKY765815', 'Transaction Not Found', 'Failed', '2017-12-24 07:31:55', NULL),
(965, 798, 'OKY765815', 'Transaction cancelled by user', 'Cancelled', '2017-12-24 07:34:46', NULL),
(966, 806, 'OKY964678', 'Transaction Not Found', 'Failed', '2017-12-26 16:41:36', NULL),
(967, 806, 'OKY964678', 'Transaction Not Found', 'Failed', '2017-12-26 16:41:42', NULL),
(968, 807, 'OKY691496', 'Transaction Not Found', 'Failed', '2017-12-26 16:50:46', NULL),
(969, 808, 'OKY826707', 'Transaction Not Found', 'Failed', '2017-12-26 17:08:34', NULL),
(970, 808, 'OKY826707', 'Transaction Not Found', 'Failed', '2017-12-26 17:08:37', NULL),
(971, 815, 'OKY122530', 'Transaction Not Found', 'Failed', '2017-12-29 00:37:40', NULL),
(972, 816, 'OKY310597', 'Transaction Not Found', 'Failed', '2017-12-29 08:34:36', NULL),
(973, 825, 'OKY205772', 'Transaction Not Found', 'Failed', '2018-01-04 03:19:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_logs`
--

CREATE TABLE `transaction_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(11) NOT NULL,
  `transaction_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `operator_id` int(10) DEFAULT NULL,
  `trip_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `source_park_id` int(10) UNSIGNED NOT NULL,
  `dest_park_id` int(10) UNSIGNED NOT NULL,
  `departure_time` time DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fare` double(8,2) NOT NULL,
  `new_fare` double DEFAULT '0',
  `bus_type_id` int(10) UNSIGNED DEFAULT NULL,
  `no_of_seats` int(11) DEFAULT NULL,
  `trip_position` varchar(255) CHARACTER SET utf8 DEFAULT 'First Bus',
  `parent_trip_id` int(10) UNSIGNED DEFAULT NULL,
  `round_trip_fare` double(8,2) DEFAULT NULL,
  `ac` tinyint(1) DEFAULT '0',
  `security` tinyint(1) DEFAULT '0',
  `tv` tinyint(1) DEFAULT '0',
  `insurance` tinyint(1) DEFAULT '0',
  `passport` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `is_intl_trip` tinyint(1) DEFAULT '0',
  `virgin_passport_fare` double DEFAULT '0',
  `no_passport_fare` double DEFAULT '0',
  `round_trip_virgin_passport_fare` double DEFAULT '0',
  `round_trip_no_passport_fare` double DEFAULT '0',
  `operator_trip_id` int(11) DEFAULT NULL,
  `virgin_passport` double DEFAULT '0',
  `regular_passport` double DEFAULT '0',
  `no_passport` double DEFAULT '0',
  `international` int(11) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fare_expiry_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_trip_name` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_trip_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `name`, `operator_id`, `trip_code`, `source_park_id`, `dest_park_id`, `departure_time`, `duration`, `fare`, `new_fare`, `bus_type_id`, `no_of_seats`, `trip_position`, `parent_trip_id`, `round_trip_fare`, `ac`, `security`, `tv`, `insurance`, `passport`, `active`, `is_intl_trip`, `virgin_passport_fare`, `no_passport_fare`, `round_trip_virgin_passport_fare`, `round_trip_no_passport_fare`, `operator_trip_id`, `virgin_passport`, `regular_passport`, `no_passport`, `international`, `deleted_at`, `fare_expiry_date`, `created_at`, `updated_at`, `parent_trip_name`, `parent_trip_code`) VALUES
(1364, '_Iyana Ipaja [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177224223', 10, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1365, '_Iyana Ipaja [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177219754', 10, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1366, '_Iyana Ipaja [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177221450', 10, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1367, '_Iyana Ipaja [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177229833', 10, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1368, '_Iyana Ipaja [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177227560', 10, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1369, '_Iyana Ipaja [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177219581', 10, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1370, '_Iyana Ipaja [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177273440', 10, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1371, '_Iyana Ipaja [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177287728', 10, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1372, '_Iyana Ipaja [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177260237', 10, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1373, '_Iyana Ipaja [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177286443', 10, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1374, '_Iyana Ipaja [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177277696', 10, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1375, '_Iyana Ipaja [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177282381', 10, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1376, '_Iyana Ipaja [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177225198', 10, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1377, '_Iyana Ipaja [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177235191', 10, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1378, '_Iyana Ipaja [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177281874', 10, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1379, '_Iyana Ipaja [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177271380', 10, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1380, '_Okota [in Lagos]_Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177277322', 79, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1381, '_Okota [in Lagos]_Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177247901', 79, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1382, '_Okota [in Lagos]_Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177226254', 79, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1383, '_Okota [in Lagos]_Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177268234', 79, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1384, '_Okota [in Lagos]_Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177227392', 79, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1385, '_Okota [in Lagos]_Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177232331', 79, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1386, '_Okota [in Lagos]_Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177234208', 79, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1387, '_Okota [in Lagos]_Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177244730', 79, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1388, '_Okota [in Lagos]_Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177270567', 79, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1389, '_Okota [in Lagos]_Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177294857', 79, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1390, '_Okota [in Lagos]_Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177296508', 79, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1391, '_Okota [in Lagos]_Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177221322', 79, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1392, '_Okota [in Lagos]_Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177250747', 79, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1393, '_Okota [in Lagos]_Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177290500', 79, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1394, '_Okota [in Lagos]_Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177276316', 79, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1395, '_Okota [in Lagos]_Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177270569', 79, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1396, '_Jibowu [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177275347', 1, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1397, '_Jibowu [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177274632', 1, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1398, '_Jibowu [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177238664', 1, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1399, '_Jibowu [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177226523', 1, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:52', '2017-12-23 21:42:52', NULL, NULL),
(1400, '_Jibowu [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177381866', 1, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1401, '_Jibowu [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177336613', 1, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1402, '_Jibowu [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177344805', 1, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1403, '_Jibowu [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177374669', 1, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1404, '_Jibowu [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177370794', 1, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1405, '_Jibowu [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177395315', 1, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1406, '_Jibowu [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177312530', 1, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1407, '_Jibowu [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177351162', 1, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1408, '_Jibowu [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177347315', 1, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1409, '_Jibowu [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177386091', 1, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1410, '_Jibowu [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177315675', 1, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1411, '_Jibowu [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177367217', 1, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1412, '_Ogba [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177336389', 6, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1413, '_Ogba [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177364697', 6, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1414, '_Ogba [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177343034', 6, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1415, '_Ogba [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177347074', 6, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1416, '_Ogba [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177315239', 6, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1417, '_Ogba [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177327592', 6, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1418, '_Ogba [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177397673', 6, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1419, '_Ogba [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177363414', 6, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1420, '_Ogba [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177366762', 6, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1421, '_Ogba [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177372988', 6, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1422, '_Ogba [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177393442', 6, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1423, '_Ogba [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177316747', 6, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1424, '_Ogba [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177363016', 6, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1425, '_Ogba [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177370911', 6, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1426, '_Ogba [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177338627', 6, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1427, '_Ogba [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177334722', 6, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1428, '_Ajah [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177319705', 4, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1429, '_Ajah [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177384645', 4, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1430, '_Ajah [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177383727', 4, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1431, '_Ajah [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177325156', 4, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1432, '_Ajah [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177395645', 4, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1433, '_Ajah [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177316245', 4, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1434, '_Ajah [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177338843', 4, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1435, '_Ajah [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177363997', 4, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1436, '_Ajah [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177316459', 4, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1437, '_Ajah [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177345782', 4, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1438, '_Ajah [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177368150', 4, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1439, '_Ajah [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177335158', 4, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:53', '2017-12-23 21:42:53', NULL, NULL),
(1440, '_Ajah [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177486388', 4, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1441, '_Ajah [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177462474', 4, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1442, '_Ajah [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177469705', 4, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1443, '_Ajah [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177454645', 4, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1444, '_Ikorodu [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177471589', 9, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1445, '_Ikorodu [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177421896', 9, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1446, '_Ikorodu [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177423381', 9, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1447, '_Ikorodu [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177417286', 9, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1448, '_Ikorodu [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177470059', 9, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1449, '_Ikorodu [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177445656', 9, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1450, '_Ikorodu [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177438208', 9, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1451, '_Ikorodu [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177477405', 9, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1452, '_Ikorodu [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177433987', 9, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1453, '_Ikorodu [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177423543', 9, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1454, '_Ikorodu [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177412374', 9, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1455, '_Ikorodu [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177411985', 9, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1456, '_Ikorodu [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177498787', 9, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1457, '_Ikorodu [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177482200', 9, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1458, '_Ikorodu [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177410421', 9, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1459, '_Ikorodu [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177496201', 9, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1460, '_Maza-Maza [in Lagos] _Enugu [in Enugu] _11000_06:00:00', 42, 'OKY-151406177415044', 2, 75, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1461, '_Maza-Maza [in Lagos] _Nsukka [in Enugu] _10500_06:00:00', 42, 'OKY-151406177486120', 2, 16, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1462, '_Maza-Maza [in Lagos] _Umuahia [in Abia] _11000_06:00:00', 42, 'OKY-151406177497713', 2, 29, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1463, '_Maza-Maza [in Lagos] _Owerri [in Imo] _11000_06:00:00', 42, 'OKY-151406177418687', 2, 11, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1464, '_Maza-Maza [in Lagos] _Port Harcourt [in Rivers] _11000_06:00:00', 42, 'OKY-151406177443669', 2, 31, '06:00:00', '', 11000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1465, '_Maza-Maza [in Lagos] _Onitsha [in Anambra] _8500_06:00:00', 42, 'OKY-151406177487452', 2, 25, '06:00:00', '', 8500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1466, '_Maza-Maza [in Lagos] _Awka [in Anambra] _9500_06:00:00', 42, 'OKY-151406177491377', 2, 28, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1467, '_Maza-Maza [in Lagos] _Ugep [in Cross River] _12000_06:00:00', 42, 'OKY-151406177473696', 2, 13, '06:00:00', '', 12000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1468, '_Maza-Maza [in Lagos] _Afikpo [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177431027', 2, 21, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1469, '_Maza-Maza [in Lagos] _Abakaliki [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177451530', 2, 20, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1470, '_Maza-Maza [in Lagos] _Ekwulobia [in Anambra] _9500_06:00:00', 42, 'OKY-151406177462980', 2, 23, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1471, '_Maza-Maza [in Lagos] _Oji [in Enugu] _10500_06:00:00', 42, 'OKY-151406177412834', 2, 18, '06:00:00', '', 10500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1472, '_Maza-Maza [in Lagos] _Awgu [in Enugu] _10000_06:00:00', 42, 'OKY-151406177490674', 2, 19, '06:00:00', '', 10000.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1473, '_Maza-Maza [in Lagos] _Nnewi [in Anambra] _9500_06:00:00', 42, 'OKY-151406177436903', 2, 26, '06:00:00', '', 9500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1474, '_Maza-Maza [in Lagos] _Okposi [in Ebonyi] _11500_06:00:00', 42, 'OKY-151406177460142', 2, 22, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL),
(1475, '_Maza-Maza [in Lagos] _Ikom [in Cross River] _11500_06:00:00', 42, 'OKY-151406177440727', 2, 14, '06:00:00', '', 11500.00, 0, 1, NULL, 'First Bus', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, '2017-12-23 21:42:54', '2017-12-23 21:42:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `active`, `role_id`, `operator_id`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 'Ayula', 'Bosede', 'me@ayolana.com', '$2y$10$0XqVj6YGiRVezmMr3DDwLuOpfLf5gy/ZJujl0SG7XblosMe6/yF96', '08133772946', '', 1, 1, NULL, 'y8cChupAkTRchuTVnhfP9lgJpqOOko7nxI10RKH8tplGG5ph7xrdaG00pTSN', NULL, '2016-03-13 02:58:29', '2016-03-13 03:11:49'),
(8, 'mussicElWZ', 'mussicElWZ', 'supermp355@gmail.com', '$2y$10$lHql9MjvNUWachiKCg7tA.OH5UbbNniTV0g.0v5m06EH154BwSBMq', '123456', 'http://www.mp3dj.eu', 1, 1, NULL, NULL, NULL, '2016-10-27 07:34:23', '2016-10-27 07:34:23'),
(9, 'Bus.com.ng', 'admin', 'admin@bus.com', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '08025455574', '', 1, 1, 1, 'Q6tDorAqbipIwN21P8oSU3DObLHwk9kldSDtbLZRmwNzFGZHCytnQG1IQTuv', NULL, '2016-11-03 13:54:33', '2017-12-22 11:49:46'),
(10, 'Admin', 'Kanta', 'kanta@bustickets.ng', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '0999999', '', 1, 2, 66, '9K9p32cEEnFqAbu6g31aj7bGK1w3FvrAfq9VxObz1yp8qQa92Df97qJBmlmK', NULL, '2016-11-25 13:17:47', '2016-12-06 16:11:01'),
(11, 'Admin', 'Okeyson', 'okeyson@bustickets.ng', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '0134242', '', 1, 2, 42, 'm3KrpRkrad6F0MchDpPxbXghnjMK1AEV3w59YQxlIviGSxBFcYhew8f8AfK6', NULL, '2016-11-28 09:33:32', '2017-02-03 12:16:00'),
(12, 'Deborah', 'Izevbokun', 'crosscountryltd@bustickets.ng', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '08055555595, 08028430304, 08028443411, 07088084271', '', 1, 2, 15, 'oTmowRDyQQxVe3lbZoxZ4l4t7gYgROYp2r8BzJ54oPTDYz4MMIXnFKSfiywk', NULL, '2016-12-14 13:07:42', '2017-01-26 14:46:22'),
(13, 'Solomon', 'Yisa', 'eekeson@bustickets.ng', '', '08034567305', '', 1, 2, 19, NULL, NULL, '2017-01-31 10:15:36', '2017-01-31 10:15:36'),
(15, 'Tonna', 'Oziligbo', 'tonna.oziligbo@bus.com.ng', '', '08065368743', '', 1, 1, 70, NULL, NULL, '2017-07-27 13:16:04', '2017-07-27 13:16:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `log_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agent_transactions`
--
ALTER TABLE `agent_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agent_purchases_agent_id_index` (`agent_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_trip_id_index` (`trip_id`),
  ADD KEY `bookings_payment_method_id_index` (`payment_method_id`),
  ADD KEY `bookings_bank_id_index` (`bank_id`),
  ADD KEY `bookings_parent_booking_id_index` (`parent_booking_id`),
  ADD KEY `bookings_operator_booking_id_index` (`operator_booking_id`);

--
-- Indexes for table `booking_notes`
--
ALTER TABLE `booking_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_notes_booking_id_index` (`booking_id`),
  ADD KEY `booking_notes_user_id_index` (`user_id`);

--
-- Indexes for table `booking_refunds`
--
ALTER TABLE `booking_refunds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_refunds_booking_id_index` (`booking_id`),
  ADD KEY `booking_refunds_user_id_index` (`user_id`),
  ADD KEY `booking_refunds_approved_user_id_index` (`approved_user_id`);

--
-- Indexes for table `bus_types`
--
ALTER TABLE `bus_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_logs`
--
ALTER TABLE `call_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `call_logs_booking_id_index` (`booking_id`),
  ADD KEY `call_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cash_offices`
--
ALTER TABLE `cash_offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chartered_bookings`
--
ALTER TABLE `chartered_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chartered_bookings_payment_method_id_foreign` (`payment_method_id`),
  ADD KEY `chartered_bookings_operator_id_index` (`operator_id`),
  ADD KEY `chartered_bookings_trip_id_index` (`trip_id`),
  ADD KEY `chartered_bookings_park_id_index` (`park_id`),
  ADD KEY `chartered_bookings_bus_type_id_index` (`bus_type_id`);

--
-- Indexes for table `chisco_trips`
--
ALTER TABLE `chisco_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `customers_feedback`
--
ALTER TABLE `customers_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devices_assigned_to_index` (`assigned_to`),
  ADD KEY `devices_operator_id_index` (`operator_id`);

--
-- Indexes for table `failed_queue_jobs`
--
ALTER TABLE `failed_queue_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_sender_id_index` (`sender_id`),
  ADD KEY `messages_recipient_id_index` (`recipient_id`);

--
-- Indexes for table `nysc_bookings`
--
ALTER TABLE `nysc_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nysc_camps`
--
ALTER TABLE `nysc_camps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `camps_state_id_index` (`state_id`);

--
-- Indexes for table `nysc_passengers`
--
ALTER TABLE `nysc_passengers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nysc_trips`
--
ALTER TABLE `nysc_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_users`
--
ALTER TABLE `online_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operator_rules`
--
ALTER TABLE `operator_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parks`
--
ALTER TABLE `parks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parks_state_id_index` (`state_id`);

--
-- Indexes for table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `passengers_booking_id_index` (`booking_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_methods_bank_id_index` (`bank_id`);

--
-- Indexes for table `queue_jobs`
--
ALTER TABLE `queue_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_booking_id_index` (`booking_id`);

--
-- Indexes for table `transaction_logs`
--
ALTER TABLE `transaction_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trips_operator_id_index` (`operator_id`),
  ADD KEY `trips_source_park_id_index` (`source_park_id`),
  ADD KEY `trips_dest_park_id_index` (`dest_park_id`),
  ADD KEY `trips_bus_type_id_index` (`bus_type_id`),
  ADD KEY `trips_parent_trip_id_index` (`parent_trip_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`),
  ADD KEY `users_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_logs_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `agent_transactions`
--
ALTER TABLE `agent_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=980;

--
-- AUTO_INCREMENT for table `booking_notes`
--
ALTER TABLE `booking_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_refunds`
--
ALTER TABLE `booking_refunds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bus_types`
--
ALTER TABLE `bus_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `call_logs`
--
ALTER TABLE `call_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cash_offices`
--
ALTER TABLE `cash_offices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chartered_bookings`
--
ALTER TABLE `chartered_bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=360;

--
-- AUTO_INCREMENT for table `chisco_trips`
--
ALTER TABLE `chisco_trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;

--
-- AUTO_INCREMENT for table `customers_feedback`
--
ALTER TABLE `customers_feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_queue_jobs`
--
ALTER TABLE `failed_queue_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nysc_bookings`
--
ALTER TABLE `nysc_bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `nysc_camps`
--
ALTER TABLE `nysc_camps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `nysc_passengers`
--
ALTER TABLE `nysc_passengers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `nysc_trips`
--
ALTER TABLE `nysc_trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `online_users`
--
ALTER TABLE `online_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `operators`
--
ALTER TABLE `operators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `operator_rules`
--
ALTER TABLE `operator_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parks`
--
ALTER TABLE `parks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=959;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `queue_jobs`
--
ALTER TABLE `queue_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seats`
--
ALTER TABLE `seats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=974;

--
-- AUTO_INCREMENT for table `transaction_logs`
--
ALTER TABLE `transaction_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1476;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `agent_transactions`
--
ALTER TABLE `agent_transactions`
  ADD CONSTRAINT `agent_purchases_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_parent_booking_id_foreign` FOREIGN KEY (`parent_booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_trip_id_foreign` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `booking_notes`
--
ALTER TABLE `booking_notes`
  ADD CONSTRAINT `booking_notes_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `booking_notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `booking_refunds`
--
ALTER TABLE `booking_refunds`
  ADD CONSTRAINT `booking_refunds_approved_user_id_foreign` FOREIGN KEY (`approved_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `booking_refunds_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `booking_refunds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `call_logs`
--
ALTER TABLE `call_logs`
  ADD CONSTRAINT `call_logs_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `call_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chartered_bookings`
--
ALTER TABLE `chartered_bookings`
  ADD CONSTRAINT `chartered_bookings_bus_type_id_foreign` FOREIGN KEY (`bus_type_id`) REFERENCES `bus_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chartered_bookings_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chartered_bookings_park_id_foreign` FOREIGN KEY (`park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chartered_bookings_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chartered_bookings_trip_id_foreign` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `devices_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `devices_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_recipient_id_foreign` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `nysc_camps`
--
ALTER TABLE `nysc_camps`
  ADD CONSTRAINT `camps_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parks`
--
ALTER TABLE `parks`
  ADD CONSTRAINT `parks_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `passengers`
--
ALTER TABLE `passengers`
  ADD CONSTRAINT `passengers_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD CONSTRAINT `payment_methods_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trips`
--
ALTER TABLE `trips`
  ADD CONSTRAINT `trips_bus_type_id_foreign` FOREIGN KEY (`bus_type_id`) REFERENCES `bus_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trips_dest_park_id_foreign` FOREIGN KEY (`dest_park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trips_parent_trip_id_foreign` FOREIGN KEY (`parent_trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `trips_source_park_id_foreign` FOREIGN KEY (`source_park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD CONSTRAINT `user_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
